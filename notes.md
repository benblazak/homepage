## notes

messy research notes, roughly labeled by what i was working on at the time. parts of these might make it into a more organized note.

### setup

- centering things
  - https://stackoverflow.com/questions/31217268/center-div-on-the-middle-of-screen
    - https://www.w3.org/Style/Examples/007/center.en.html
- global styles in sveltekit
  - discussion
    - https://github.com/sveltejs/kit/issues/1948
    - https://github.com/sveltejs/kit/issues/714#issuecomment-808600046
    - https://github.com/sveltejs/kit/pull/726
  - example https://github.com/sveltejs/kit/tree/master/examples/hn.svelte.dev
    - global css in `/src/app.css`
    - `import '../app.css';` in `/src/routes/__layout.svelte`
- dark mode based on system settings https://css-tricks.com/dark-modes-with-css/
- asciidoc global vars (just use `include`) https://github.com/asciidoctor/asciidoctor/issues/2130
- dark mode favicons https://css-tricks.com/dark-mode-favicons/
- svelte, app.html, what are `%svelte.body%` and `%svelte.head%`?
  - https://github.com/sveltejs/kit/issues/2035
    - https://github.com/sveltejs/kit/issues/2221
  - app.html is a template, and these tags have to do with server side rendering and client side hydration

### favicon

- nice looking sveltekit tutorial https://www.sitepoint.com/a-beginners-guide-to-sveltekit/
  - the official docs are also quite nice, but i think there's some extra stuff in here that's worth looking at
  - was looking for where favicon stuff should go, and whether it was safe (and good practice) to modify app.html directly. this mentioned putting meta tags in a `<svelte:head>` element in `__layout.svelte`, which seems to me like a good place for favicon stuff too.
- optimizing svg https://css-tricks.com/tools-for-optimizing-svg/
  - used inkscape, since that's what i used to create the icon
- for measuring site performance later https://developers.google.com/web/tools/lighthouse
- mdn on site structure (though it's different for svelte apps) https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/Dealing_with_files#what_structure_should_your_website_have
- using git hooks https://support.gitkraken.com/working-with-repositories/githooksexample/
- `<meta name="theme-color" ...`
  - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta/name/theme-color
  - https://css-tricks.com/meta-theme-color-and-trickery/
  - testing
    - ios 15.0 safari
      - if you leave this tag out, safari uses the background color (at least for my "under construction" page) which i think makes more sense than setting it to an absolute value

### pwa

- service worker notes
  - https://developers.google.com/web/fundamentals/primers/service-workers
    - scope is determined by the location of the service worker file
  - https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers
    - ended up using some of this code
  - you can only have 1 per scope https://medium.com/tengio-ltd/merging-multiple-service-worker-scripts-in-the-same-scope-83213da915ad
  - browser support https://jakearchibald.github.io/isserviceworkerready/
  - https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent/waitUntil
  - https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope/skipWaiting
  - was getting a few errors in lighthouse about pwa things not working, till i added `"scope": "/",` to the web manifest
- typescript triple-slash directives https://www.typescriptlang.org/docs/handbook/triple-slash-directives.html

### nav bar (2021-10-08)

- mac keyboard shortcuts
  - web inspector (safari, chrome): cmd + option + i
  - toggle full screen: ctrl + cmd + f
  - emoji picker (for some inputs): ctrl + cmd + space
  - vscode asciidoc preview: cmd + k, v
- if you git merge with `--no-ff`, the branch will remain visible in the history even if you delete it https://stackoverflow.com/questions/3392392/when-is-the-right-time-to-delete-a-git-feature-branch
- navbar examples
  - https://kit.svelte.dev
  - https://svelte.dev
  - https://github.com just has a bar at the top
  - https://stripe.com just has links at the top too, but it's beautiful
    - https://stripe.com/docs their docs page has a persistent nav bar at the top. also pretty. though i want mine to show/hide on scroll.
- sveltekit site https://github.com/sveltejs/sites
  - sveltekit `__layout.svelte` https://github.com/sveltejs/sites/blob/master/sites/kit.svelte.dev/src/routes/__layout.svelte
  - uses things from https://github.com/sveltejs/sites/tree/master/packages/site-kit/components
  - the svelte nav bar isn't perfect though, it has some weird behaviors
    - e.g. on https://svelte.dev it disappears, except on the home page, when the window gets narrow
  - the code does mention some things i hadn't thought about
    - e.g. not showing or hiding when scroll happens in the sidebar, on the docs page
- someone duplicating the apple style navbar https://linguinecode.com/post/create-a-navbar-in-svelte
  - i actually don't like the https://www.apple.com nav bar that much lol
- tailwind screen size breakpoints https://tailwindcss.com/docs/breakpoints
- can't use css vars in media queries https://stackoverflow.com/questions/40722882/css-native-variables-not-working-in-media-queries
- a svelte tutorial that mentions a navbar https://dev.to/karkranikhil/build-responsive-website-using-svelte-in-30-minutes-l9
- about custom elements in html https://www.html5rocks.com/en/tutorials/webcomponents/customelements/
- css size units https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Values_and_units
- https://fontawesome.com don't know if i'll use this yet (e.g. for the github link)
- svg isn't showing in `img` tag
  - old answer https://stackoverflow.com/questions/4476526/do-i-use-img-object-or-embed-for-svg-files
    - https://www.w3.org/Graphics/SVG/IG/resources/svgprimer.html#SVG_in_HTML
      - `img` doesn't support scripting (?) (and my svg has a polyfil script)
  - all about using svg https://css-tricks.com/using-svg/
    - using `object` like they mention works :)
- svg still doesn't render in chrome
  - `Uncaught TypeError: Failed to execute 'createImageData' on 'CanvasRenderingContext2D': Value is not of type 'long'.`
  - other svg (github icon from fontawesome) works
  - i think it's the mesh gradient polyfill (javascript) code that's failing. but it sounds (from google) like browsers can't natively display mesh gradients (? in any case, inkscape apparently includes a polyfill by default). so maybe i'll need to change my icon. alternately, i could use a raster image.
- redoing my butterfly svg
  - i really liked it, but i may as well adjust it now
  - most importantly, it turns out to be crucial to test with browsers as you draw something... since inkscape can do more than they can render correctly
  - renaming `butterfly.optimized.svg` -> `butterfly.svg`
  - renaming `butterfly.gradient-mask.optimized.svg` -> `butterfly-color.svg`
  - copying `butterfly-color.svg` -> `favicon.svg`
- trying to make the svg clickable in a link
  - https://css-tricks.com/links-inline-svg-staying-target-events/
  - clickable regions in svg https://css-tricks.com/the-many-ways-to-link-up-shapes-and-images-with-html-and-css/ though that's not what i'm trying to do
  - http://sanjeevkpandit.com.np/posts/avoid-using-object-tag-inside-anchor-tag/ so... back to `img`? -> yup!
- you can put `aria-label` on anything https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-label_attribute
- stripe blog on one of their page designs https://stripe.com/blog/connect-front-end-experience
- several fontawesome libraries for svelte
  - https://www.npmjs.com/package/svelte-fa
  - https://www.npmjs.com/package/fontawesome-svelte
  - https://www.npmjs.com/package/fa-svelte
    - intended to be small and fast, at the expense of some features
  - https://www.npmjs.com/package/svelte-awesome
  - using fontawesoma via the official npm packages
    - javascript api https://fontawesome.com/v5.15/how-to-use/javascript-api/setup/library
    - typical use? https://fontawesome.com/v5.15/how-to-use/on-the-web/setup/using-package-managers
- `<script>` `async` and `defer` attributes https://javascript.info/script-async-defer
- css selectors https://www.w3schools.com/cssref/css_selectors.asp
- css variable performance https://lisilinhart.info/posts/css-variables-performance/
- fonts
  - https://websitesetup.org/web-safe-fonts-html-css/
    - https://fonts.google.com
    - https://fonts.adobe.com/?ref=tk.com need creative cloud subscription
  - what does `display=swap` mean? -> https://stackoverflow.com/questions/56537360/google-font-display-swap-strange-behaviour
  - interesting: [CSS Typography: Techniques and Best Practices](https://www.webfx.com/blog/web-design/css-typography-02/)
  - better to let google host its fonts (rather than self hosting) https://wp-rocket.me/blog/self-hosting-google-fonts/
- css centering https://css-tricks.com/centering-css-complete-guide/
  - to align an `img` with text, it's the image you want centered https://stackoverflow.com/questions/489340/vertically-align-text-next-to-an-image
- pausing here
  - fixing up links https://www.w3schools.com/css/css_link.asp
  - aligning navbar https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Alignment/Box_Alignment_in_Flexbox
  - line under navbar https://stackoverflow.com/questions/17030664/round-cap-underline-in-css
- also, i need to learn more about css layouts https://developer.mozilla.org/en-US/docs/Learn/CSS
- also, html has a `nav` tag! oops https://developer.mozilla.org/en-US/docs/Web/HTML/Element/nav
- about padding
  - my safari seems to have a 6px margin around `body` by default; my chrome seems to have 8px
  - https://medium.com/coding-blocks/css-padding-a-magic-wand-2b8a66b84ebe
  - https://css-tricks.com/margin-0-padding-0-no-longer-cool/
  - https://www.thoughtco.com/css-zero-out-margins-3464247
- okay to split css into multiple files https://stackoverflow.com/questions/2336302/single-huge-css-file-vs-multiple-smaller-specific-css-files
- https://css-tricks.com/six-tips-for-better-web-typography/
- there currently is on blog, so i'm adding an error page here
  - the sveltekit site error page https://github.com/sveltejs/sites/blob/master/sites/kit.svelte.dev/src/routes/__error.svelte
  - sveltekit docs https://kit.svelte.dev/docs#layouts-error-pages

### nav bar (2021-10-13)

- merged, then realized i had bunches left to do lol
- fontawesome
  - install with npm https://fontawesome.com/v5.15/how-to-use/on-the-web/setup/using-package-managers
    - installing as a dev dependency since i'm building static
    - ooo -- sounds like this is the correct thing to do with svelte anyway https://www.reddit.com/r/sveltejs/comments/jwdhlf/svelte_npm_library/ -- though... still not sure if this would be true, if the target was node
    - they say to import in `<head>` but i had to `import ...` in `<script>`
  - basic use https://fontawesome.com/v5.15/how-to-use/on-the-web/referencing-icons/basic-use
    - fontawesome elements are replaced with `<svg>` elements by default
    - they recommend using `<i>` or `<span>` elements, but since you have to target `<svg>` elements in your css, my environment seems happier using `<svg>` elements to begin with
- `width` on `img` is only in `px`... https://www.w3schools.com/tags/att_img_width.asp
  - can use `width` in css instead for other units
- lint errors
  - might need to ignore style in eslint config https://stackoverflow.com/questions/64644571/how-to-configure-sass-and-eslint-in-svelte-project
  - sass styles are not formatted by prettier, but scss styles are
- typescript errors on the `load` function in `__error.svelte`
  - `Missing return type on function`
  - `Object pattern argument should be typed`
  - https://stackoverflow.com/questions/12769636/how-to-make-a-class-implement-a-call-signature-in-typescript
  - found an example https://mvolkmann.github.io/blog/svelte/sveltekit/?v=1.0.18
  - also if you look [in the svelte docs](https://kit.svelte.dev/docs#loading) they have an example for a regular (non-error) load function that references the type `Load` -- if you find that type in the source (using vscode), there's also a type called `ErrorLoad` -- `ErrorLoad` defines a [call signature](https://www.typescriptlang.org/docs/handbook/2/functions.html#call-signatures) `(ErrorLoadInput): Promise<LoadOutput>`, with extra stuff inside, but following this signature makes the compiler happy :)
- you can, in a round about way, use js values in css with svelte https://gotofritz.net/blog/setting-css-values-dynamically-in-svelte/
  - but you cannot use css variables inside css selectors https://stackoverflow.com/questions/17951877/is-it-possible-to-use-css-vars-in-css3-selectors
- how to remove unused css selector warnings https://stackoverflow.com/questions/60677782/how-to-disable-svelte-warning-unused-css-selector -- i don't want to do this, but for reference
- "unused css" gets removed, but you can keep it https://stackoverflow.com/questions/68499862/can-i-keep-unused-css-selector
  - the docs even mention this https://svelte.dev/docs#style but i'd forgotten
- you can also pretend like text is svg :) https://css-tricks.com/adding-stroke-to-web-text/
- can't modify color vars in css https://stackoverflow.com/questions/40010597/how-do-i-apply-opacity-to-a-css-color-variable
- css fill remaining space https://stackoverflow.com/questions/90178/make-a-div-fill-the-height-of-the-remaining-screen-space
  - https://stackoverflow.com/a/24979148/2360353
- default css values https://www.w3schools.com/cssref/css_default_values.asp -- not sure what browser this is for
- fontawesome was making my bundle super big
  - uninstalled the recommended js packages, and switched to this method https://fontawesome.com/v5.15/how-to-use/on-the-web/advanced/svg-javascript-core
- don't really want js watching every dom change though
  - `dom.watch()` options https://fontawesome.com/v5.15/how-to-use/javascript-api/methods/dom-watch
  - can use `dom.i2svg()` to only do this once, but need to call it after things are loaded
    - https://fontawesome.com/v5.15/how-to-use/javascript-api/methods/dom-i2svg
    - https://svelte.dev/tutorial/onmount
- fontawesome flash of unstyled icons
  - this also happens when `npm run preview`
  - tried this, but it didn't work (in dev) https://dev.to/kaylasween/how-to-fix-font-awesome-icons-flashing-wrong-size-in-gatsby-3igm (or in preview)
  - switched to using `span` instead of `svg` tags for them, and now the icons will flash not there, but they don't flash big -- :)
    - maybe safari default size for an svg is big? -- maybe it has to do with the butterfly svg i have?
    - i was originally not doing this because the `svg` selector in css would be "unused" and get pruned -- but `:global(svg)` fixes that
- fontawesome use svg instead of js?
  - https://stackoverflow.com/questions/66069647/best-way-to-import-svg-icons-into-a-svelte-app
  - https://joshuatz.com/posts/2021/using-svg-files-in-svelte/
    - but sveltekit doesn't use rollup, it uses vite
      - but vite can use rollup plugins?
  - not as simple, for whatever reason, as just referencing the svg in an `<img>` (it is solid black, and the dimensions of the box are weird...)
    - this is okay though, since i think i found a way i like
- fontawesome javascript api method pages
  - https://fontawesome.com/v5.15/how-to-use/javascript-api/methods/dom-i2svg
    - this appears to be the first one -- had to click through them to see them all
    - `dom.i2svg` -- for converting `<span class="fa fa-home" />` to svg via js once
  - https://fontawesome.com/v5.15/how-to-use/javascript-api/methods/dom-watch
    - `dom.watch` -- for doing the same conversion, except automatically (in case the dom changes)
  - https://fontawesome.com/v5.15/how-to-use/javascript-api/methods/icon
    - `icon` -- has a `html` property, so you can say `<span>{@html icon(faHome).html}</span>` after `import {faHome} from ...` and avoid the `library.add(...)` and such from the other methods
    - this is also probably ideal in terms of when and how the icons are rendered and delivered, but... i don't understand all the moving pieces well enough yet to articulate it properly :)
- fontawesome icons flashing
  - someone else trying to solve this https://medium.com/@fabianterh/fixing-flashing-huge-font-awesome-icons-on-a-gatsby-static-site-787e1cfb3a18
    - yay! it was the `import "... .css"` that i had been missing before

### other (2021-10-17)

- custom error (404) page doesn't work on cloudflare pages
  - need `404.html` in root https://developers.cloudflare.com/pages/platform/serving-pages
  - can do it this way? https://github.com/sveltejs/kit/issues/754
  - but that way disables SSR by default, and isn't quite what i want? https://github.com/sveltejs/kit/issues/1209
    - especially https://github.com/sveltejs/kit/issues/1209#issuecomment-893179110
- css `@import url("...")` and `@import "..."` are equivelant https://stackoverflow.com/questions/24579417/in-css-when-does-should-one-use-the-url-function-versus-a-string
- for fontawesome, doing `config.autoAddCss = false;` in `__layout.svelte` seems to set the property globally
- butterfly icon rendering pixelated in safari (not in chrome)
  - making it bigger and scaling with css helps, but makes the box bigger too, and isn't really what i want
  - can add icons to fontawesome 5 https://stackoverflow.com/questions/11426172/add-custom-icons-to-font-awesome
    - but it doesn't look like this will work for me (my icon has much more than a single path going on)
  - the non-color icon renders clearly if it's substituted
  - renders clearly if the `<svg>...</svg>` code is pasted into the document -- so it's just safari (webkit) and the `<img>` tag?
  - here's a library that takes svg included via `<img>` tags and substitutes inline svg https://github.com/jonnyhaynes/inline-svg/blob/master/src/inlineSVG.js
  - can rename the `.svg` to `.svelte` (as someone suggested somewhere... didn't save the link, it was a while ago) and use it like any other svelte component -- requires a small modification to the file contents (removing some stuff that's only necessary for standalone svg) -- works :)
    - running `npm run build`, it looks like the fontawesome icons included via js, and the butterfly icon included as a svelte component, are all inlined into `index.html` -- the color butterfly is a little large for an icon, but i think it's small enough that that's okay
    - maybe i did save the link -- it might have been here https://joshuatz.com/posts/2021/using-svg-files-in-svelte/#svite--vite -- they mention it anyway :) -- along with lots of other good ideas -- probably the best link about this
  - https://github.com/vitejs/vite/issues/1204#issuecomment-892956809
    - `import butterfly from "$lib/icons/butterfly-color.svg?raw"` and `{@html butterfly}` work
      - but why?
      - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import
        - nope, not what i was looking for
      - ahh, `?raw` is a vite feature! https://vitejs.dev/guide/assets.html#importing-asset-as-url
  - it looks like importing with javascript like this puts the contents exactly into the resulting page, while making it a .svelte file causes the contents to be minified (a little; comments and line endings are removed; not sure how much difference it makes in this case)
    - it actually looks like the non-minified version is ever so slightly smaller. not going to look too closely into why right now. in any case, they're very close.
  - so, gonna modify the optimized svgs
    - i had to remove the xml declaration from the optimized files
      - this is an option i think, when you generate the optimized svg, but i did it by hand
    - i also removed `height` and `width` attributes from the top level `<svg>` tag
      - when resizing with css, they were causing the containing box to be bigger than it needed to be, i think
    - and added `aria-hidden="true"`
  - so i'm going to import `?raw` and include that way, partly because it's consistent with how i'm using the fontawesome icons :)
- after testing this, it seems that the `<span>` around the fontawesome icons is no longer necessary -- probably because of including the css manually
- svg was shrinking when the viewport got too small
  - set `min-width` https://stackoverflow.com/questions/64502869/how-can-i-prevent-svg-elements-from-resizing-in-the-browser/64503135

### cloudflare pages (2021-10-18)

- trying a `.node-version` file instead of setting the `NODE_VERSION` environment variable
  - seems to work :)
- tryping `pipfile` for python version
  - seems to work :)
  - `Pipfile` needs to be capitalized
  - default python 3 seems to 3.7
  - i need >=3.8, due to some changes to `pathlib`
  - tried 3.9, doesn't seem available
  - tried 3.8, doesn't seem available
  - so this won't work (the way i want it to at least)
  - maybe i don't need python 3.8...
  - yay!
- custom 404 https://developers.cloudflare.com/pages/platform/serving-pages#not-found-behavior
  - https://github.com/sveltejs/kit/issues/1209#issuecomment-893179110
  - about npm scripts https://www.twilio.com/blog/npm-scripts
    - `postbuild` is automatically called after `build`
  - works!
  - but `path` was `/404.html` lol -- so that's not helpful
    - i don't see anything in the svelte docs that might help...
    - ooo `window.location.pathname` https://css-tricks.com/snippets/javascript/get-url-and-url-parts-in-javascript/
    - need to guard references to `window` with `if (browser) ...` https://kit.svelte.dev/docs#modules-$app-env
  - also tried `$page.path`, importing `page` from `$app/stores` -- but that didn't work either
    - looking at the build output, it's prerendered (at build time) so it never has a chance to have the actual path lol
  - was trying to return a status of 404
    - adapter-static would error, so the build would fail
    - it looks like it might not be necessary though? -- in safari -> inspect -> network -> headers, it shows a 404 for the page that doesn't exist (from the service worker though) -- then the 404.html...js generated by svelte is loaded with status 200 (also by the service worker 🤔) (with initiator = the route that failed)
    - i should probably look into this more in the future, to understand it better :)
- gonna try a little, rewriting the above (simple) python script in typescript, to remove the extra dependency
  - running typescript code from the command line
    - https://stackoverflow.com/questions/33535879/how-to-run-typescript-files-from-command-line
      - https://github.com/TypeStrong/ts-node
    - for some reason this seems hard to do here
  - spent just a little time trying to use javascript too, and maybe it's not worth it? -- js really isn't meant for this use case anyway, and this code doesn't have to run on a server (except on build, where other languages are available)
  - it'd be fun to write it in ruby, but i'm learning other things right now :) and this works

### nav bar (2021-10-19)

- working on splitting out the nav bar code from `__layout.svelte`
- also taking this opportunity to add `rel` and `target` attributes to my outbound links
  - https://developers.google.com/search/docs/advanced/guidelines/qualify-outbound-links
  - https://www.freecodecamp.org/news/how-to-use-html-to-open-link-in-new-tab/

### index (2021-10-19)

- working on index.html
- html5 semantic elemtns https://www.w3schools.com/html/html5_semantic_elements.asp
- gravatar image requests https://en.gravatar.com/site/implement/images/
  - url to mine https://www.gravatar.com/avatar/4ee9f0f98a7595fc51e0e9363805d09e.jpg
  - for sizes, use e.g. `?size=80` -- up to 2048px -- default is 80px
- circular image with css https://www.w3schools.com/howto/howto_css_rounded_images.asp
- hosting for images
  - uses dropbox, s3 https://brandur.org/fragments/static-site-asset-management
  - there used to be a way to direct download from google drive via url, but i'm not sure it's supported anymore https://www.howtogeek.com/747810/how-to-make-a-direct-download-link-for-google-drive-files/
  - i think i could also just use a separate repo on github (or even gitlab?) and add it as a submodule
    - this is not the "right way" to do it, but it would probably be easiest 😅 and i can always change it later
    - it also keeps the images separate from the code, so that it's not so hard to change later -- and also so that as the image repo grows, it doesn't baloon the code repo -- and if it baloons itself with old images, i can always delete and recreate it to clean it out
    - this does mean that old versions of the code won't work, because their references will be broken, but i think that's okay
    - also... maybe git lfs would be a good idea anyway... because cloning downloads the entire history, unless lfs is used... -- though i think there's a way to not download the entire thing, so i wonder if cloudflare pages does that -- in any case, i don't want unused stuff making my build take longer and such
  - github has a 1GB per account lfs limit
  - gitlab has a 10GB per repo limit (including lfs) -- and unlimited repositories? -- but cf pages doesn't support gitlab directly yet -- but i think i could use it for a submodule
  - or i could just use github lfs -- it'll probably be fine -- and cf pages will hopefully support gitlab (or github will raise its limits) before it isn't
    - might be much easier than working with submodules unnecessarily https://git-scm.com/book/en/v2/Git-Tools-Submodules
  - so i guess the question is, do i want to have versioned images, or just a place for them (whether versioned or not)
    - if versioned, they should be lfs in the current repo
    - if just a place for them, ideally i could put them in google drive or a gcp bucket or something, but that looks difficult -- for now i could put them lfs in gitlab, and include them as a submodule (maybe with path `/static/lfs`)
    - i suppose it's a future question, since treating them as versioned will make the repo bigger over time (and it looks like it's kinda hard to clean up) -- wheras treating them as unversioned will break old versions of the site
      - cleaning up
        - github https://docs.github.com/en/repositories/working-with-files/managing-large-files/removing-files-from-git-large-file-storage
        - gitlab https://docs.gitlab.com/ee/topics/git/lfs/#removing-objects-from-lfs
    - really though, will i ever need old versions of the site?
    - but also, will this site ever be so big that it matters? lol
    - oh, also, if the repo does get too big, and i don't want to clean it, i can always start a fresh one with the current code, and go from there -- deleting or archiving the old one, depending on how i feel then
  - at the moment, i think i'll go with the hopefully easiest option, and use github lfs -- we'll see if my feelings change before i implement it -- :)
- service worker caching for external things
  - google font; gravatar; possibly future other things
  - global list of external things to cache?
    - what is a `.d.ts` file?
      - https://basarat.gitbook.io/typescript/project/modules/globals
      - https://stackoverflow.com/questions/21247278/about-d-ts-in-typescript
        - `.d.ts` files contain type definitions for regular javascript code -- so `d` is for "definitions" maybe?
  - https://gist.github.com/Rich-Harris/fd6c3c73e6e707e312d7c5d7d0f3b2f9
  - imports in service workers https://web.dev/es-modules-in-sw/
  - made a global module for the url strings :)
- service worker is erroring (in both chrome and safari)
  - https://stackoverflow.com/questions/66529102/uncaught-in-promise-typeerror-failed-to-execute-cache-on-addall-request
  - checked which file was 404, and it was `.DS_Store`, probably the one from `/static`... -\_-
  - deleted all `.DS_Store` files and now my service worker appears to work :)
  - can disable ds_store files per account https://www.techrepublic.com/article/how-to-disable-the-creation-of-dsstore-files-for-mac-users-folders/
  - added a "prebuild" script to delete the files -- kinda silly since they'll only exist on macs, but... simple and it should solve this problem
  - service workers don't run in dev mode, so no need to worry about it then
- making a submodule with lfs
  - different than what i settled on the other day, but this provides the most flexibility (and it's easily reversible) so i'm gonna try it
  - created https://gitlab.com/benblazak/homepage-lfs
  - add submodule https://git-scm.com/book/en/v2/Git-Tools-Submodules
  - set up lfs https://docs.gitlab.com/ee/topics/git/lfs/
  - mmm, maybe i don't want to deal with submodules after all -- deleting -- happy with this little experiment -- will come back to it later
- for github lfs, the only way to clear things out of the server lfs storage is to delete the repo https://docs.github.com/en/repositories/working-with-files/managing-large-files/removing-files-from-git-large-file-storage#git-lfs-objects-in-your-repository
- adaptive layout
  - browsers seem to only want to show size in px
  - specifically css px -- this is old, but all i could find https://stackoverflow.com/questions/26528421/ems-instead-of-px-in-chrome-developer-tools
  - https://developer.apple.com/safari/tools/
    - incidentally, this page switches the nav bar format at 768px
  - tailwind screen breakpoints https://unpkg.com/browse/tailwindcss@2.2.17/stubs/defaultConfig.stub.js
  - https://pixelsconverter.com/pixels-to-inches
  - ahh, 1px = 1/96 in https://www.w3schools.com/cssref/css_units.asp
  - global scss vars
    - https://www.reddit.com/r/sveltejs/comments/pmham1/sveltekit_how_to_set_up_global_scss_accessible_to/
    - but it doesn't look like it should be necessary https://github.com/sveltejs/svelte-preprocess#global-style
    - a bug about hot realoading in global styles https://github.com/sveltejs/svelte-hmr/issues/22 but this was before the switch to vite
    - the vite folks seem to think that first solution is the right one? lol (as far as vite is concerned) https://github.com/vitejs/vite/issues/832
- alternatives to git submodule
  - https://www.atlassian.com/git/tutorials/git-subtree
    - ahh, but this might not be what i want https://stackoverflow.com/questions/31769820/differences-between-git-submodule-and-subtree
  - https://stackoverflow.com/questions/6500524/alternatives-to-git-submodules
- thinking more about git stuff
  - could plan to import to gitlab later https://docs.gitlab.com/ee/user/project/import/github.html
  - can host lfs objects elsewhere https://docs.github.com/en/enterprise-server@3.2/admin/user-management/managing-repositories-in-your-enterprise/configuring-git-large-file-storage-for-your-enterprise#configuring-git-large-file-storage-to-use-a-third-party-server
  - could host lfs objects on gitlab?

### nav (2021-10-23)

- working on styling for smaller screens
  - want to make one of those hamburger mobile navigation menus
  - w3 tutorial https://www.w3schools.com/howto/howto_js_mobile_navbar.asp
  - apple navbar in svelte https://linguinecode.com/post/create-a-navbar-in-svelte
  - looks like you can do it without javascript https://1stwebdesigner.com/how-to-create-a-responsive-dropdown-navigation-using-only-css/ -- but i'm already using javascript, so i may as well use it for this too
    - another tutorial along the same lines https://medialoot.com/blog/how-to-create-a-responsive-navigation-menu-using-only-css/
- also, search bar
  - https://support.google.com/programmable-search/answer/4513882?hl=en
    - https://programmablesearchengine.google.com
      - from testing, seems like the code they give you after you create one has to be directly in `<body>` -- hopefully there's a way around that
    - https://developers.google.com/custom-search/docs/element
  - this wouldn't work offline
    - how to not display it in that case?
- styling
  - reordering with css https://stackoverflow.com/questions/7425665/switching-the-order-of-block-elements-with-css
  - sass can manipulate colors https://sass-lang.com/documentation/modules/color
    - in css this is more complicated https://stackoverflow.com/questions/40010597/how-do-i-apply-opacity-to-a-css-color-variable though support for actual color manipulation may come eventually https://github.com/Fyrd/caniuse/issues/5405
  - putting one div on top of another https://stackoverflow.com/questions/2941189/how-to-overlay-one-div-over-another-div
  - mobile vs desktop first design https://ishadeed.com/article/the-state-of-mobile-first-and-desktop-first/ -- he advocates neither -- lots of interesting stuff in here
  - when to use different css units (skimmed) https://chiamakaikeanyi.dev/sizing-in-css-px-vs-em-vs-rem/
  - css `:not` selectors can be chained https://css-tricks.com/css-not-with-multiple-classes/
  - `display: none` vs `visibility: hidden` https://www.w3schools.com/css/css_display_visibility.asp
- transitions
  - can transition when adding a class https://stackoverflow.com/questions/16176648/trying-to-do-a-css-transition-on-a-class-change/16176902
- switching back to sass for global css
  - mostly so i can make the color variables sass variables
  - https://sass-lang.com/documentation/breaking-changes/css-vars
  - `prependData: '@import "src/vars.scss";'` specified in `svelte.config.js` doesn't appear to be added to the global `app.scss`
- removing hover for touch devices https://developer.mozilla.org/en-US/docs/Web/CSS/@media/hover
  - also, `@media` queries seem to have to be top level in css -- sass allows them at any level
  - another reference https://stackoverflow.com/questions/23885255/how-to-remove-ignore-hover-css-style-on-touch-devices
- can blur behind elements
  - https://stackoverflow.com/questions/27583937/how-can-i-make-a-css-glass-blur-effect-work-for-an-overlay
  - safari requires prefix https://caniuse.com/css-backdrop-filter
  - decided not to for now -- but for reference, i had this in `app.scss`
    - light `--color-background-90: #{color.change($sol-base3, $alpha: 0.9)}`
    - dark `--color-background-90: #{color.change($sol-base03, $alpha: 0.9)}`
  - and in `Nav.svelte`'s style i had
    - `background-color: var(--color-background-90);`
    - `-webkit-backdrop-filter: blur(5px);`
  - and i had `nav` set to `position: sticky;`
  - now that i think about it, it might have been better to have `--color-background-rgb` and use sass to get the rgb values, and then use those in regular css to generate the color with whatever alpha we needed
    - can't use sass functions, of course, since the color scheme preference could change at runtime
- styling without javascript
  - decided to quickly look at this
  - if the global style is imported in js in `__layout.svelte`, it doesn't get imported with js disabled
  - if the global style is imported in a `<style global ...>`, it does work without js :)
  - google fonts don't seem to work without js, at least the way they are, included in the scss file (though, i can't think of a way more likely to work either)
  - changes to `app.scss` are hot reloaded automatically, just like they are if i included it via js
    - which i thought was the reason including global styles via js was recommended... maybe they fixed that, or maybe it's a side effect of sass?
  - also, if i included it in a global `style` tag, i don't need to manually import `vars.scss` in `app.scss`, since it seems to get processed by the vite preprocessor
  - when the screen is small, there's currently no way to see the nav icons without js, since apparently they're rendered hidden -- not sure how to change that though... don't want flicker or anything for folks who have js enabled, and don't want to make the code more complicated to handle that case at the moment -- maybe later
- hide and show nav on scroll
  - example https://www.w3schools.com/howto/howto_js_navbar_hide_scroll.asp
  - position https://developer.mozilla.org/en-US/docs/Web/CSS/position
    - looks like `sticky` is what i want
    - actually, if i'm doing this with js, i probably just want to leave position normal
  - slightly related, for later -- side navigation -- possibly for blog posts -- https://css-tricks.com/sticky-smooth-active-nav/
  - https://stackoverflow.com/questions/22675126/what-is-offsetheight-clientheight-scrollheight
  - https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement
  - https://www.javascripttutorial.net/dom/css/add-styles-to-an-element/
  - https://javascript.info/onscroll
  - https://developer.mozilla.org/en-US/docs/Web/API/Document/height
    - https://developer.mozilla.org/en-US/docs/Web/API/Element/clientHeight
  - testing in safari, it looks like i've scrolled past the height of `nav` when `window.scrollY > nav.offsetHeight`, and i've scrolled to the end of the page when `window.scrollY < document.body.offsetHeight - window.innerHeight`
  - https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing effects what css `height` means, as well as the values of (some?) js height properties
  - debounce function for js https://davidwalsh.name/javascript-debounce-function
  - js and css animations and transitions https://css-tricks.com/controlling-css-animations-transitions-javascript/
  - dynamically add css https://stackoverflow.com/questions/1720320/how-to-dynamically-create-css-class-in-javascript-and-apply
  - js toggle css class https://www.w3schools.com/howto/howto_js_toggle_class.asp
  - how to css transitions trigger?
    - i don't see a way besides adding a class
    - https://stackoverflow.com/questions/11450552/how-to-set-and-trigger-an-css-transition-from-javascript/11450663
      - "The way you trigger transitions is to make an element match the CSS selector. The easiest way to do that is to assign your transition to a class, then add that class using Javascript." hmmm
    - https://zellwk.com/blog/css-transitions/
      - looks like a good article
      - only mentions triggering via js via setting a class
  - specificity https://stackoverflow.com/questions/54654672/css-specificity-when-javascript-modifies-css
    - https://developer.mozilla.org/en-US/docs/Web/API/CSS_Object_Model/Using_dynamic_styling_information
  - transitions on inline style changes?
    - https://stackoverflow.com/questions/40784938/is-it-possible-to-apply-css-transitions-inline
  - so, transitions happen when one value for a property applies, and then a different one takes precedence -- i think
    - NOTE: this isn't quite correct, see below
    - adding or removing a class is indeed one of the easier ways to cause this
    - having something styled, and then overriding one of those values via js (via `element.style.`, which is equivalent to spefifying that value as an inline style, which has maximum (?) precedence), is another way
    - having a value set via `element.style.`, and then changing that value, doesn't work
      - oh wait... maybe it does, but there just wasn't time for the transition between the two values i was setting
  - yup -- so nevermind that above theory -- there just needs to be time for the transition to actually occur
  - took me a second to realize -- sticky is perfect for my use case -- no need to switch between sticky and static for nav bar position -- if an element woulnd't be moved down by the sticky setting, it stays put, and behaves like static -- only if it needs to move down will the `top` value matter -- also, negative `top` values are allowed, so you can hide the nav bar that way
- for maybe later -- browser vs server side js https://dev.to/deekshithrajbasa/angular-server-side-rendering-ssr-the-browser-is-not-the-server-30a5
- media queries
  - logic https://css-tricks.com/logic-in-media-queries/
  - there is a query for javascript! https://www.w3schools.com/cssref/css3_pr_mediaquery.asp
    - but it's not supported yet https://caniuse.com/?search=scripting
- working on transition for the hamburger and times (menu button)
  - i had this working before, but i think the styling was different, and i can't find notes on it (even with all these notes! lol)
  - found this, it's a super good explanation of how to overlay divs https://stackoverflow.com/questions/2941189/how-to-overlay-one-div-over-another-div
  - but my padding isn't right
  - explanation of why https://stackoverflow.com/questions/17115344/absolute-positioning-ignoring-padding-of-parent
  - still can't get this to work, but gonna stop for now -- at least this time i took notes :)
  - hmm, could use svelte to tie the transitions together, so the icons weren't both visible at the same time
- working on how things are with js disabled
  - https://css-tricks.com/how-to-load-fonts-in-a-way-that-fights-fout-and-makes-lighthouse-happy/
  - for some reason, my google font imported in `app.scss` via `@import` doesn't load without js, but if i include it in a `<link>` in `<svelte:head>`, it does load
  - https://stackoverflow.com/questions/10036977/best-way-to-include-css-why-use-import#
    - apparently there are other reasons why one might want to use `<link>` over `@include`, especially that `<link>` allows the style to be loaded in parallel
    - though i'm not sure if that applies in my case, since the css might be included inline? i haven't looked at what's generated
  - https://developers.google.com/fonts/docs/getting_started
  - about `<link>` https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link
    - `async` and `defer` look like `<script>` attributes https://discourse.mozilla.org/t/async-v-s-defer/53819
  - https://navillus.dev/blog/progressive-enhancement/ -- even mentions hamburger menus specifically
    - uses button method
  - the `<noscript>` tag appears to work https://www.w3schools.com/tags/tag_noscript.asp
  - loading css asynchronously https://css-tricks.com/the-simplest-way-to-load-css-asynchronously/
  - https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types/preload
  - all the sites seem to say to do it this way https://web.dev/defer-non-critical-css/ but it's not working for me
  - this way works! https://stackoverflow.com/a/67716609/2360353
    - they say to use `rel="preload stylesheet"`
    - this seems to work in safari, and make lighthouse happy in chrome
  - oooooh, to make the other way work, you do need two lines -- the [mozilla page](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types/preload) (i think i linked above) does have this in the example, i just missed it
    - didn't test this, since using `rel="preload stylesheet"` doesn't duplicate the url like this would
- using checkbox instead of js action for opening nav
  - looking a lot at this https://svelte.dev/repl/267acb68b79647849f0532774d62c594?version=3.38.2
    - which was linked [here](https://navillus.dev/blog/progressive-enhancement/) (i think i linked above)
  - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/checkbox
  - looks like the label and the checkbox can be in separate divs :)
- accessibility for hidden things
  - https://a11y-guidelines.orange.com/en/web/components-examples/accessible-hiding/
  - hiding from view but not from screen readers https://a11y-guidelines.orange.com/en/web/components-examples/accessible-hiding/
  - for mobile nav http://web-accessibility.carnegiemuseums.org/code/navigation/
    - recommends to make the button to open and close navigation accessible -- wich seems easier than hiding things from view without hiding from screen readers
- to use svelte transitions, you have to add/remove from the dom https://stackoverflow.com/questions/59062025/is-there-a-way-to-perform-svelte-transition-without-a-if-block
  - so, it would have to be in the dom initially to be visible without js -- but that creates a quick hide on reload that i don't like -- so it looks like i have to use js triggered css transitions for hiding and showing the menu on mobile as well
  - actually -- can use e.g. `{#if !browser || navOpen}` :)
- documenting js (and ts) files https://jsdoc.app/tags-file.html
- got it to work :)
  - [this example](https://svelte.dev/docs#use_action) is really very good (also linked above) though i did a few things differently
  - rather than set `var navOpen = true;` initially, and set it to false in `onMount`, i checked `browser` in the `{#if ...}`
  - i still needed to add `js` (or something) as a class, to use in the css selector -- i called my [action](https://svelte.dev/docs#use_action) something different, but i did the same thing
    - i originally tried to add it some other way, but i didn't find another way that worked -- specifically, adding it in `onMount` didn't work, because if js is enabled, `<nav>` won't exist in the dom when it's not visible
      - and i could have tried to animate it myself and leave it always in the dom, but using svelte transitions was too tempting and nice :) lol
- oops, not exactly working, links are hidden in desktop view
- nope, can't use svelte transitions without removing from the dom https://github.com/sveltejs/svelte/issues/6336
- https://stackoverflow.com/questions/20827367/why-doesn-t-height0-hide-my-padded-div-even-with-box-sizingborder-box
- hmm, transitioning with js on height doesn't work, because when `height: 0;` in css, `element.offsetHeight === 0` (i think)
  - so `offsetheight` isn't what i want https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/offsetHeight
  - neight is `clientHeight` https://developer.mozilla.org/en-US/docs/Web/API/Element/clientHeight
  - oo but `scrollHeight` is! https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight
- interesting -- css [`box-sizing`](https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing) doesn't seem to effect any of the three javascript height properties
- media queries in js https://www.w3schools.com/howto/howto_js_media_queries.asp
  - but if i can use media queries in js, i could just use svelte transitions... lol
- testing things
  - using `use:classListAdd` or something similar to add a class when js is available is unnecessary -- can also say `class:js={browser}` in the attribute list, after `import { browser } from "$app/env";` in the js
  - setting css `max-height: 50%;` doesn't work any different than setting `height`, as far as i can tell
- other thoughts
  - deciding to leave the `--color-` variable prefixed for now (instead of changing them to e.g. `--text-color`)
  - not making a js file to mirror the sass `$screen-` variables (in `vars.scss`) -- this would be useful if i was going to be mirroring media queries in js in some cases, but i think i can work around that more prettily with js setting css variables for what i'm doing now
- set css vars in js
  - https://css-tricks.com/updating-a-css-variable-with-javascript/
  - https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties
- tried transitioning the button with svelte `crossfade` -- it doesn't keep the elements in the same place though, which is that difficult part (other wise i could just use svelte `transition:fade`)
  - `const [send, receive] = crossfade({ duration: 5000 });`
  - and `<span in:send={{key: "navOpen"}} out:receive={{key: "navOpen"}}` around each icon
- tried transitioning the button with `position: absolute` (i've tried this before; it works, though it's not perfectly the way i want it; especially this time, the padding was off, even though i added extra to the element)
  - i think i like it better with no transition actually -- which is easier :)
- was thinking of making a variable local to a function in Nav.svelte, but `this` didn't behave the way i thought it would -- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this
  - for fun https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes -- but i'm not gonna go and make it complicated
- if the nav bar expands and is bigger than the viewport (as with my phone in landscape), you can't view the end of the nav without going to the top of the screen
  - https://stackoverflow.com/questions/2950382/onscroll-for-div/16463707
  - https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector
  - https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll
  - what is the `name` attribute on `label` for? https://stackoverflow.com/questions/26061651/what-is-the-purpose-of-the-html-name-attribute
  - i would have to redo padding if i wanted a clean scrollbar for nav in this case -- and i'm not sure it would work that well, since then you can't scroll in that area to actually move down the page
  - you can `transition` on `transform`, and `translateY` can transition percents :) https://stackoverflow.com/questions/54809093/simple-css-animation-not-smooth-in-safari
  - `position: absolute;` with `top: 100%;` puts the element at the same verticle position it would be relative to it's parent (or maybe the first non `position: static;` parent?) https://stackoverflow.com/questions/22599675/positioning-a-div-below-its-parent-position-absolute-bottom-0-not-working
  - https://developer.mozilla.org/en-US/docs/Web/CSS/z-index
  - so, i think the best i could do here is scroll the nav bar in js in `onscroll`, if it's bigger than the window -- but i'm not sure i wanna be scrolling in js anyway -- gonna leave this for now
- can i make the nav items portion like a second pull down shade?
  - i think i could :) maybe with an extra wrapper `<div>` (or.. hopefully not two) around `<nav>` -- i think i'll save that for later though
  - importnat to remember, if i do try later: i would have to adjust `var pageTop` to include `nav.offsetHeight`, or something similar -- or else i'd have problems if the nav menu was bigger than the window

### research and cleanup (2021-10-29)

- asciidoctor linking to external documents https://docs.asciidoctor.org/asciidoc/latest/macros/inter-document-xref/
- twitter card validator https://cards-dev.twitter.com/validator
  - also https://www.bannerbear.com/tools/twitter-card-preview-tool/
- testing https://dev.to/d_ir/introduction-4cep
  - didn't read the whole thing, but looks really interesting -- saving for later
- ios pwa, airplane mode
  - https://dev.to/hurricaneinteractive/pwa-fetchevent-respondwith-error-on-safari-148f
    - testing by building and previewing locally, then adding the page to my homescreen
    - adding `<meta name="apple-mobile-web-app-capable" content="yes" />` didn't appear to make a difference
  - the service worker appears to be working on desktop
  - hmm, but if i install it on desktop and turn off wifi, it doesn't work here either
  - so there's a problem with my service worker
  - it's `fetch(event.request)` that's failing, in the fetch event
  - about service workers https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API
    - example https://github.com/mdn/sw-test/blob/gh-pages/sw.js
  - oooo -- the original tutorial i was followinhttps://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers
    - but i didn't read down far enough -- they do eventually cover error handling
    - hmm, but they return a jpg if cache misses -- it looks like my problem is that it's trying to redownload the service worker on load, and failing -- if i return an image when the cache misses and fetch fails, it downloads on reload when the network isn't available lol
  - can i return nothing?
    - https://developer.mozilla.org/en-US/docs/Web/API/FetchEvent/respondWith
    - https://developer.mozilla.org/en-US/docs/Web/API/Response
    - using `Response.error()` changes the error message, but the problem persists
    - hmm... not sure
  - more about service workers https://developers.google.com/web/fundamentals/primers/service-workers/lifecycle
    - updates happen when ... https://developers.google.com/web/fundamentals/primers/service-workers/lifecycle#updates
  - so, turns out it was failing on the request for "/", rather than the request for the service worker
  - adding "/" to my list of paths to cach fixed the issue on chrome :)
    - though to be quite honest, i'm not sure what it measnt to cache "/"
    - oooh -- since the service worker is the one doing the cache, it requests "/" from the server when it tries to install -- and requesting "/" from the server returns the contents of "index.html"
      - so that makes sense -- it's still interesting though, since when i tried to cache "index.html" it failed lol
  - didn't fix the airplain mode issue on ios though
  - adding the meta fields suggested above still didn't effect anything
  - about pwas in ios https://firt.dev/ios-14/#progressive-web-apps
  - debugging mobile safari https://www.browserstack.com/guide/how-to-debug-on-iphone
  - interesting -- so if i open the pwa that i installed from my actual site, it shows a service worker (that fails, and then goes away) -- but if i open the pwa installed from localhost, it doesn't show a service worker at all
  - so there's a chance the thing that fixed chrome will fix ios too, just it needs to be served from (not localhost)
  - gonna try pushing the change, then
  - yay! it worked -- ios pwa (from www.benblazak.dev) works on airplane mode now
    - to test, open it to get it to refresh, and kill the app (in the app switcher), then airplane mode, then reopen
- updating `__error.svelte`
  - https://github.com/sveltejs/kit/issues/2712
  - using `Load` compiles and runs, but doesn't typecheck? vscode complains about it anyway
  - https://github.com/sveltejs/svelte-preprocess/issues/182
    - https://github.com/sveltejs/svelte-preprocess/blob/main/docs/preprocessing.md#typescript---limitations
- randomly saw this https://dev.to/masakudamatsu/don-t-nest-nav-inside-header-do-nest-the-hamburger-menu-button-inside-nav-6cp
  - recommendations for `<header>` and `<nav>` semantic html, for accessibility
  - recommends a layout different from the one i'm using, so i'll keep this in mind for later
- ios splash screen / startup image
  - https://www.npmjs.com/package/pwa-asset-generator
    - https://itnext.io/pwa-splash-screen-and-icon-generator-a74ebb8a130
  - pwa-asset-generator doesn't seem to want to connect to localhost...
    - the html page itself links to other things (with absolute paths, that don't work locally), so i can't point it directly at that
    - https://gist.github.com/dannguyen/03a10e850656577cfb57
      - but when i open it, the butterfly is solid black -- and it doesn't look like it adjusted all the links
    - i don't really wanna have to deploy it to point it to a page to generate icons and splash screens
    - i can save a working single html file from chrome :)
  - pwacompat https://developers.google.com/web/updates/2018/07/pwacompat might be a good solution
    - https://github.com/GoogleChromeLabs/pwacompat
      - https://github.com/v8/v8.dev/pull/310/files
        - https://bugs.webkit.org/show_bug.cgi?id=183937
    - maybe not, the icon it generates is not padded well, and the splash screen is kinda ugly
    - https://medium.com/@firt/you-shouldnt-use-chrome-s-pwacompat-library-in-your-progressive-web-apps-6b3496faab62
  - so, not sure if this is worth the effort... since _eventually_ apple will support the webmanifest fully (or... well... this part of it, probably, anyway) -- and it seems unlikely that lots of people will be adding my site to their homescreen in the meantime -- and i'll just have to update it as standards change...
    - for a web app that i specifically meant to be installed, this would be different, and i would want to make splash screens anyway
    - so maybe i should do it for practice...
    - but i think i know what i would do -- specifically, i would use pwa-asset-generator, pointing it towards either an html saved manually via chrome, or towards a `/lib/splash` on the deployed site (so that i could use svelte to generate the splash page, rather than writing it in bare html + css) -- the main thing is i don't want to have manual steps in a kind of complicated process when there's a decent chance things will change by the next time i need to do it (every year or so, when apple releases a new device, untill they support this part of the webmanifest) -- i also don't want to have to do something manual every year
    - wish pwacompat allowed to point towards a sub-path to use that to generate the splash screen...
- if you omit `start_url` from the webmanifest, the default is to open to wherever the user was when they saved the app https://stackoverflow.com/questions/46203661/possible-to-have-multiple-manifest-json-for-pwa -- tested on ios just now and appears to be true
  - i think i could also make a second manifest, and then link that in `<svelte:head>` in a nested `__layout.svelte` for things under `/blog/` -- not sure if it's worth the effort

### blog (2021-11-01)

- asciidoc using a block in a list https://blog.mrhaki.com/2017/10/awesome-asciidoctor-use-only-block-as.html
- looking for an asciidoc preprocessor

  - https://www.reddit.com/r/sveltejs/comments/p20ztw/asciidoc_preprocessor_for_use_with_sveltekit/
    - there is one for markdown https://www.npmjs.com/package/svelte-preprocess-markdown
    - https://github.com/asciidoctor/asciidoctor.js/
  - i think this is how the svelte blog converts their markdown https://github.com/sveltejs/svelte/blob/master/site/src/routes/blog/_posts.js
    - they have the conversion code and index in `/src/routes/blog`
    - they have the content in `/content/blog`
  - can have custom preprocessors with svelte-preprocess https://github.com/sveltejs/svelte-preprocess/blob/main/docs/preprocessing.md#custom-preprocessors
    - api https://svelte.dev/docs#svelte_preprocess
  - svelte society https://sveltesociety.dev
  - got a custom preprocessor set up (not doing anything, just as a proof of the configuration working)

    - `/svelte.config.js`

      ```javascript
      import adapter from "@sveltejs/adapter-static";
      import preprocess from "svelte-preprocess";

      /** @type {import('@sveltejs/kit').Config} */
      const config = {
        extensions: [".svelte", ".adoc"],

        // Consult https://github.com/sveltejs/svelte-preprocess
        // for more information about preprocessors
        preprocess: [
          preprocess({
            aliases: [
              // asciidoc https://asciidoctor.org/docs/asciidoc-recommended-practices/#document-extension
              ["adoc", "asciidoc"],
            ],
            scss: {
              prependData: `@import "src/vars.scss";`,
            },
            asciidoc({ content, filename, attributes }) {
              var code = content + filename + String(attributes);
              return { code };
            },
          }),
        ],

        kit: {
          adapter: adapter(),
        },
      };
      export default config;
      ```

    - `/src/routes/blog/__layout.svelte`
      ```html
      <template lang="adoc">
        <slot />
      </template>
      ```
    - `/src/routes/blog/test.adoc`
      ```asciidoc
      *hello* _world_ :)
      ```
    - but this doesn't quite do what we want -- for instance, `.adoc` files are treated just like `.svelte` ones -- also, we still need the `<template lang="adoc">` in a `__layout.svelte`, so we can't mix in other formats for some posts -- if we try to use `.svelte` files with templates inside for posts, we don't get asciidoc syntax hilighting (or at least not by default, didn't look into it)

  - i wonder if i need a special markup language at all... -- i could use e.g. [pug](https://pugjs.org/api/getting-started.html), and do code highlighting with [highlight.js](https://highlightjs.org/usage/#importing-the-library) directly (?), and such -- and svelte helps with a lot too
    - so does vscode with "emmet abbreviations", apparently
    - tested pug syntax with the beginning of my Header component, and there's so much markup that has to be there anyway that it looks about the same with pug
      - not sure how i'd feel if i were writing prose (like a blog post)
  - there is also https://www.npmjs.com/package/rollup-plugin-asciidoc which might make it easy to do things the way the svelte blog does

#### (2021-11-02)

- wondering if i can make a preprocessor like [svelte-markup](https://npm.io/package/svelte-markup) -- or like the officially supported one for pug? -- that processes all the ["markup"](https://svelte.dev/docs#svelte_preprocess) in a `.adoc` file
  - from the docs
    - `preprocess:` config options passed to svelte-preprocess https://kit.svelte.dev/docs#configuration
    - "it is also possible to manually enqueue stand-alone processors" in `rollup-plugin-svelte` https://github.com/sveltejs/svelte-preprocess/blob/main/docs/usage.md#with-rollup-plugin-svelte
      - maybe this would work with sveltekit too https://github.com/sveltejs/svelte-preprocess/blob/main/docs/preprocessing.md#difference-between-the-auto-and-stand-alone-modes
    - how svelte-preprocess handles `<template lang="...">` tags
      - `markupTagName` https://github.com/sveltejs/svelte-preprocess/blob/2632a88d380a14843393d5246f471a8c6446a3b5/docs/preprocessing.md#auto-preprocessing-options
      - `markupPattern` https://github.com/sveltejs/svelte-preprocess/blob/3a020e991928989ea45c5e2163679dca938a6967/src/modules/markup.ts#L42
      - `templateMatch` https://github.com/sveltejs/svelte-preprocess/blob/3a020e991928989ea45c5e2163679dca938a6967/src/modules/markup.ts#L44
      - `if ...` https://github.com/sveltejs/svelte-preprocess/blob/3a020e991928989ea45c5e2163679dca938a6967/src/modules/markup.ts#L47
        - this looks like if there is no template tag, it will run the transformer over the whole "content"?
  - source
    - https://github.com/sveltejs/svelte-preprocess
    - https://github.com/reed-jones/svelte-markup
    - https://github.com/asciidoctor/asciidoctor.js/
- is there a way to do it without writing a preprocessor?
  - if i just put a `.adoc` file in `/src/routes/...`, it's vite that complains (thinking it's javascript?)
  - interesting, partilaly relevant https://codechips.me/svelte-with-vitejs-typescript-tailwind/
  - i don't see a way though
    - trying to use svelte-preprocessor to make one doesn't seem to have good support for filetype extensions -- even `.pug` doesn't make it preprocess as pug source
- tried svelte-markeup
  - added '.md' to `extensions` in the svelte config, and imported and put the `markdown()` preprocessor first, like the readme shows
  - it works for markdown files with `.md` extension!
  - unfortunately, i had to install with `--force` as there were dependency mismatches, and npm warned me about vulnerabilities
  - this does verify that the author's methods still work though
- interesting
  - svg engine prototype https://twitter.com/sarah_edo/status/1454155879083741186?s=21
  - svelte + d3, and renderless components https://twitter.com/h_i_g_s_c_h/status/1452672806068670467?s=21
- for markdown, if i want to add it
  - https://www.npmtrends.com/commonmark-vs-markdown-it-vs-marked-vs-remark-vs-remark-parse

#### (2021-11-03)

- learning about js and ts
  - https://stackoverflow.com/questions/21117160/what-is-export-default-in-javascript
  - https://www.typescriptlang.org/docs/handbook/2/functions.html
  - type assertions (type casting) https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#type-assertions
  - how to pass all arguments from one function to another
    - `arguments` object https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments
      - rest parameters preferred
      - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters
  - https://stackoverflow.com/questions/57992841/how-to-point-index-file-for-npm-package-written-in-typescript
    - so it looks like i can either use js for functions that need to be imported in `svelte.config.js`, or i can make a module (and build it so that it can be imported)
  - the types if i write my preprocessor things in typescript (that i've found so far) are `import type { Preprocessor, MarkupPreprocessor, PreprocessorGroup, } from "svelte/types/compiler/preprocess";`
  - i had a lovely function for that ended up being not what i want
    - ```typescript
      export function filetype(extensions: string[], fn: CallableFunction) {
        return (obj: { filename: string }) => {
          if (extname(obj.filename) in extensions) return fn(obj);
          else return undefined;
        };
      }
      ```
    - i was hoping to be able to say `filetype([".adoc", ".asciidoc"], asciidoc())` in the `preprocessor:` section of `svelte.config.js` but i think i'll have to do it a different way
  - pretty printing objects (even ones with circular references) https://stackoverflow.com/questions/11616630/how-can-i-print-a-circular-structure-in-a-json-like-format
- learning about html
  - `<header>` and `<footer>` can be in `<main>` if they are specific to that page https://stackoverflow.com/questions/20470345/should-the-header-and-footer-tags-be-inside-the-main-tag
  - https://developer.mozilla.org/en-US/docs/Glossary/Semantics
- learning about asciidoctor
  - asciidoctor.js https://docs.asciidoctor.org/asciidoctor.js/latest/
  - header and footer https://blog.mrhaki.com/2015/04/awesome-asciidoctor-adding-custom.html
    - looks like it's effort to add stuff to either -- so i don't need them -- so i probably want `standalone: false` (the default) -- which makes more sense anyway
  - looking at the ast (abstract syntax tree) for my little test document
    - line numbers and such are in there when i set `sourcemap: true` in the options, but i don't see a function in the api docs to get them out -- svelte says a sourcemap should be included in the return object from preprocessors, but i don't see how in this case -- hopefully it's fine and any errors are printed during compilation anyway
  - the only other related plugin i found includes title and date in its output https://www.npmjs.com/package/rollup-plugin-asciidoc
  - but, since i'm doing it this way, i can also just write any javascript i want in a script tag (in a passthrough block) -- so maybe it's okay that i can't think of a good way to pass document data back to the module programmatically
  - trying out a broken xref
    - i don't see any warning -- and running in dev it's perfectly happy to send me to 404 :) but the build fails, so that's good (i'll just need to check for it)
    - i don't see any line numbers anywhere -- so i guess i can remove sourcemap: true`
      - actually... i could look at this more -- but for now i think i'll leave it

#### (2021-11-04)

- reference
  - https://github.com/reed-jones/svelte-markup
  - https://svelte.dev/docs#svelte_preprocess
  - https://docs.asciidoctor.org/asciidoctor.js/latest/
  - https://highlightjs.org
- learning
  - update js object
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign -- doesn't recurse
    - the way svelte-markup does it (for `options`) works :) https://github.com/reed-jones/svelte-markup/blob/master/src/renderers/markdown.js
  - commonmark doesn't allow namespaced tags (yet) https://github.com/commonmark/commonmark-spec/pull/648
  - js `delete` https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/delete
- source highlighting
  - asciidoctor provides several built in adapters for highlight.js https://asciidoctor.github.io/asciidoctor.js/2.2.5/#syntaxhighlighter
    - but i can't find what they are (besides just highlight.js)
  - [rollup-plugin-asciidoc](https://github.com/carlosvin/rollup-plugin-asciidoc/blob/master/src/rollup-plugin-asciidoc.js) used [asciidoctor-highlight](https://github.com/jirutka/asciidoctor-highlight.js/)
  - normally, highlight.js for asciidoctor is runtime (in the browser), but i wonder if svelte would pre-render, so i could just use the regular library
    - ahh, highlight.js uses `document`, so it can't work during prerender
  - https://github.com/jirutka/asciidoctor-highlight.js/ might work
    - maybe -- there are open merge requests from dependabot
  - could maybe use something like [constexpr](https://github.com/fctorial/ConstexprJS), to prerender in chrome
  - could maybe use ruby asciidoctor
    - if i do, it looks like `rouge` is the highlighter to use
      - coderay doesn't support that many languages
      - highlight.js is client side (or limited a little (?) and with extra dependencies, if using asciidoctor-highlight)
      - pygments requires ruby and python
      - rouge is pure ruby, and compatible with pygments stylesheets
      - https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/rouge/
    - installing
      - https://docs.asciidoctor.org/asciidoctor/latest/install/ruby-packaging/
        - https://rvm.io -- for fish shell https://rvm.io/integration/fish
        - actually, reddit seems to recommend `chruby` over `rvm` https://www.reddit.com/r/rails/comments/f009mb/there_are_two_ruby_version_manager_rvm_vs_rbenv/
        - someone else too https://stevemarshall.com/journal/why-i-use-chruby/
        - sounds like chruby -- or frum? -- https://www.sitepoint.com/ruby-version-managers-macos/
        - https://news.ycombinator.com/item?id=27232152 doesn't seem like a strong consensus, but frum still sounds good -- and it has [instructions](https://github.com/tako8ki/frum#fish-shell) for fish shell :) -- i'm gonna try it

#### (2021-11-05)

- trying the ruby version of asciidoctor
  - i thought [frum](https://github.com/TaKO8Ki/frum) might have not been working yesterday because my macports was messed up, but i fixed macports, and it still gives me an error that google isn't helping with (and i don't see a `-v` optiond)
    - `error: No such file or directory (os error 2)` but i have no idea what file or directory it's talking about...
    - found some options (named differently than i was expecting) https://github.com/TaKO8Ki/frum#options but it looks like it's default is max verboisity? in any case none of the three options tell me anything more
    - looking at `env` in the shell, a few of the directories didn't seem to exist -- created them, but still get the error
    - get the same error in bash as in fish
    - and the same error trying to install a different (not newest) version of ruby
  - i guess [chruby](https://github.com/postmodern/chruby) is the next thing to try
    - in macports
    - the macports versions of ruby appear to be up to date too
    - `ruby-install` (in macports too) appears to be necessary if you want chruby to see the different ruby versions (or else, i just didn't look into how to get it to see different system rubies)
    - now that i think about it more though, i'm not even useing nvm to switch node versions... lol, i'm just using it to run the latest one -- so i'll keep these programs around, but i'll plan to just use the system ruby for now
  - now to learn about ruby gems
    - looks like cloudflare pages will look at the gemfile and run `bundle install` https://developers.cloudflare.com/pages/framework-guides/deploy-a-jekyll-site#creating-a-github-repository

#### (2021-11-06)

- saving current code

  - since i'm not checking this in, since it's all still a mess
  - `preprocess.js`

    ```javascript
    /**
     * @file local preprocessors
     *
     * inspired by https://github.com/reed-jones/svelte-markup
     *
     * references
     * * https://svelte.dev/docs#svelte_preprocess
     * * https://github.com/sveltejs/svelte-preprocess
     */

    import { dirname, extname } from "path";

    import Asciidoctor from "asciidoctor";
    const asciidoctor = Asciidoctor();

    /**
     * asciidoc preprocessor group
     */
    export function asciidoc(options = {}) {
      options = {
        safe: "server",
        ...options,
        _meta: {
          extensions: [".adoc", ".asciidoc"],
          ...options?._meta,
        },
        attributes: {
          "source-highlighter": "highlight.js",
          ...options?.attributes,
        },
      };
      const meta = options._meta;
      delete options._meta;
      return {
        markup({ content, filename }) {
          if (!meta.extensions.includes(extname(filename))) return;
          options = { base_dir: dirname(filename), ...options };
          const doc = asciidoctor.load(content, options);
          return {
            code: doc.convert(),
            dependencies: undefined, // _TODO
            map: undefined,
          };
        },
      };
    }
    ```

  - `svelte.config.js`

    ```javascript
    import adapter from "@sveltejs/adapter-static";
    import preprocess from "svelte-preprocess";

    import { asciidoc } from "./lib/preprocess.js";

    /** @type {import('@sveltejs/kit').Config} */
    const config = {
      extensions: [".svelte", ".adoc", ".asciidoc"],

      // Consult https://github.com/sveltejs/svelte-preprocess
      // for more information about preprocessors
      preprocess: [
        asciidoc(),
        preprocess({
          scss: {
            prependData: `@import "src/vars.scss";`,
          },
        }),
      ],

      kit: {
        adapter: adapter(),
      },
    };

    export default config;
    ```

- ruby
  - https://bundler.io/docs.html
  - it looks like there was much confusion about switching from `Gemfile` to `gens.rb` a while back https://github.com/ruby/setup-ruby/pull/84#issuecomment-686854903
    - too bad, because i liked the new name better -- but i guess this is consistent with `Pipfile`, if i ever need python again in this project
    - from various git issues, i'm guessing it's pretty generally supported, but not common and not often documented -- so rather than test cloudflare to see if they also support it, i think i'll just use the default `Gemfile`
  - `bundle init`
  - `bundle add asciidoctor`
    - "not allowed to install to the system RubyGems"
    - i'm not sure how common this is, but i think i'll try to install gems to my project dir as much as i can -- for the same reasons it's a good idea with e.g. python and node, specifically, this way i can clean it easily (to make sure what i'm running is the same as what cloudflare runs) if i need to, and also keep track of dependencies more easily
  - `bundle config set --local path 'ruby_gems'`
    - not sure whether there's a common place to put them -- but this matches `node_modules` nicely
    - looked at where [poetry](https://python-poetry.org) (for python) puts theirs, and it looks like it's not in the project dir at all
  - `bundle install`
  - hmm, might want to specify version in Gemfile rather than `.ruby-version`
    - https://devalot.com/articles/2012/04/gem-versions.html
  - `bundle add rouge` for source highlighting https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/rouge/
  - gem bin not in my path
    - should i use chruby...?
    - ahh, should use `bundle exec ...` https://stackoverflow.com/questions/32940541/should-i-install-ruby-gems-in-system-repository-globally-or-the-project-vendor
      - they also say it's better to just install stuff globally, which is the bundler default
      - hmm, i'm tempted, but then one would have to `sudo bundle install`, and i'd rather not
- asciidoctor
  - https://docs.asciidoctor.org/asciidoctor/latest/cli/man1/asciidoctor/#options
  - first pass at a command line `echo '_this_ *that*' | bundle exec asciidoctor --safe-mode server --attribute "source-highlighter=rouge" --base-dir ./src/routes/blog/ --no-header-footer -`
  - calling out to the shell from js
    - https://stackoverflow.com/questions/1880198/how-to-execute-shell-command-in-javascript
    - https://nodejs.org/api/child_process.html#child_processexecsynccommand-options
    - maybe should be using `spawn` https://www.freecodecamp.org/news/node-js-child-processes-everything-you-need-to-know-e69498fe970a/
    - `execsync` works just fine :) at least for my example
      - turned out my problem was that i had deleted the `!` in my file extension check
- source highlighting with rouge
  - with `--no-header-footer`, asciidoctor can output class names (with no css) or style attributes (but then how do i do dark/light modes?) -- without, it includes the css in a `<style>`
  - https://rouge-ruby.github.io/docs/

#### (2021-11-07)

- how should i get rouge styles into my page?
  - research
    - https://docs.asciidoctor.org/asciidoctor/latest/cli/man1/asciidoctor/#options
    - https://docs.asciidoctor.org/asciidoc/latest/attributes/document-attributes-ref/
    - https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/rouge/
    - https://rubygems.org/gems/rouge
    - https://github.com/rouge-ruby/rouge/issues/343
      - so you can generate stylesheets using ruby if you want
      - i don't see any pregenerated css
      - looks like the same is true for pygments https://github.com/richleland/pygments-css
    - asciidoc embedded vs standalone https://docs.asciidoctor.org/asciidoctor/latest/api/convert-strings/#embedded-output
  - can i have vite import ruby files...?
    - https://maximomussini.com/posts/a-rubyist-guide-to-vite-js/ -- cool, but not quite what i'm looking for
- style and script sections in asciidoc
  - https://github.com/sveltejs/svelte-preprocess/blob/3a020e991928989ea45c5e2163679dca938a6967/src/modules/markup.ts
  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp
  - https://svelte.dev/docs#svelte_preprocess
  - style and script preprocessors don't seem to get called at all for asciidoc
    - though if i remove the file extension check, they're called for all the svelte files... lol
    - ahh... it looks like markup runs first, then the output of that is used for script, then style
  - so what i want to do is extract the script and style blocks, and re-insert them afterwards --- mostly because svelte seems to want `<script>` to come first, and asciidoc seems to want `= title` and such to come first (before any passthrough block even)
  - svelte-preprocess uses `[^]` to match all characters, but apparently it's not recommended https://stackoverflow.com/questions/1979884/how-to-use-javascript-regex-over-multiple-lines
    - sounds like we could also use the `s` ("dotAll") flag https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/dotAll
  - js `split` with capturing groups includes the captured string in the array https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
    - ahh, though that's not what i want, since i'm not sure what element the script block will be
  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join
- nice regex tester https://regex101.com
- svelte sourcemaps https://github.com/sveltejs/svelte-preprocess/issues/140
  - are these the sourcemaps they're talking about? -- if so, not sure how (or whether i want) to manually generate them
  - so my line numbers will be off, when linking back to the .adoc file
  - but! it appears that a `console.log()` in my passthrough `<script>`
- parse html in js https://stackoverflow.com/questions/10585029/parse-an-html-string-with-js
- ruby
  - run a ruby script in the bundle context https://stackoverflow.com/questions/7958594/how-to-run-a-ruby-script-within-bundler-context/7958630
  - ruby stdin https://stackoverflow.com/questions/273262/best-practices-with-stdin-in-ruby
  - ruby directory name
    - https://ruby-doc.org/core-2.6.3/File.html#method-c-dirname
    - https://ruby-doc.org/stdlib-2.6.3/libdoc/pathname/rdoc/Pathname.html
  - vscode ruby extension https://marketplace.visualstudio.com/items?itemName=castwide.solargraph
  - ruby parenthesis
    - http://ruby-for-beginners.rubymonstas.org/bonus/parentheses.html
    - https://stackoverflow.com/questions/340624/do-you-leave-parentheses-in-or-out-in-ruby
  - ruby modules and scripts are generally kept separate https://stackoverflow.com/questions/2249310/if-name-main-equivalent-in-ruby
  - regex
    - https://ruby-doc.org/core-2.4.0/Regexp.html
    - vars https://stackoverflow.com/questions/2268421/ruby-regular-expression-using-variable-name/2268595
- current state of `svelte.config.js`

  ```javascript
  import adapter from "@sveltejs/adapter-static";
  import preprocess from "svelte-preprocess";

  const extensions = {
    svelte: [".svelte"],
    asciidoc: [".adoc", ".asciidoc"],
  };

  /** @type {import('@sveltejs/kit').Config} */
  const config = {
    extensions: Object.values(extensions).flat(),

    // Consult https://github.com/sveltejs/svelte-preprocess
    // for more information about preprocessors
    preprocess: [
      asciidoc(extensions.asciidoc),
      preprocess({
        scss: {
          prependData: `@import "src/vars.scss";`,
        },
      }),
    ],

    kit: {
      adapter: adapter(),
    },
  };

  export default config;

  // ----------------------------------------------------------------------------

  /**
   * preprocessors
   *
   * references
   * * good example of a svelte preprocessor
   *   * https://github.com/reed-jones/svelte-markup
   * * official
   *   * https://svelte.dev/docs#svelte_preprocess
   *   * https://github.com/sveltejs/svelte-preprocess
   * * https://nodejs.org/api/child_process.html#child_processexecsynccommand-options
   */

  import { dirname, extname } from "path";
  import { execSync } from "child_process";

  import Asciidoctor from "asciidoctor";
  const asciidoctor = Asciidoctor();

  /**
   * @returns a RegExp matching the html tag and its contents
   *
   * notes
   * * pass `"s"` to `flags` for multiline tags
   */
  function tagRegex(tag, flags = "") {
    return new RegExp(`<${tag}.*?>.*?</${tag}>`, flags);
  }

  /**
   * asciidoc preprocessor
   *
   * _TODO
   * * styling
   *
   * usage
   * * the first `<script>`, `<svelte:head>`, and `<style> blocks are removed from
   *   the content before processing, and inserted again afterwards.
   * * in the `.adoc` file, it seems best to use comment blocks (`////`) to avoid
   *   a resulting empty passthrough block, though passthrough blocks also work
   *   (and are source highlighted, in vscode).
   *
   * notes
   * * doesn't look for `dependencies`
   * * doesn't generate `map` (sourcemap)
   *
   * references
   * * https://docs.asciidoctor.org/asciidoctor/latest/cli/
   */
  function asciidoc(extensions) {
    return {
      markup({ content, filename }) {
        if (extensions && !extensions.includes(extname(filename))) return;
        const base_dir = dirname(filename);

        const reScript = tagRegex("script", "s");
        const script = content.match(reScript);
        content = content.replace(reScript, "");

        const reSvelteHead = tagRegex("svelte:head", "s");
        const svelteHead = content.match(reSvelteHead);
        content = content.replace(reSvelteHead, "");

        const reStyle = tagRegex("style", "s");
        const style = content.match(reStyle);
        content = content.replace(reStyle, "");

        const out = execSync(
          `bundle exec asciidoctor \
          --safe-mode server \
          --base-dir "${base_dir}" \
          --embedded \
          --attribute relfilesuffix="/" \
          --attribute source-highlighter=rouge \
          -`,
          { input: content }
        ).toString();

        const code = [script, svelteHead, out, style].join("\n");
        return { code };
      },
      script({ content, filename }) {
        if (extensions && !extensions.includes(extname(filename))) return;
        const doc = asciidoctor.loadFile(filename);
        // _TODO could get the attributes with js, but should probably do it with
        // ruby, if i'm going to be using ruby for conversion
        const a = doc.getAttributes();
        console.log(a);
        const code = [content].join("\n");
        return { code };
      },
    };
  }
  ```

  - going to change this though
  - currently, i have a mix of js and ruby for asciidoctor, so this is very much in transition
  - it'd be nice to use only js, but then i'd have to use highlight.js, and it needs client side js (or a workaround)
  - so i'm going to see how much i can do in ruby
  - still need to work out how to set up things with ruby in this environment
  - there's also the fact that i don't actually know ruby hehe

#### (2021-11-08)

- integrating ruby
  - could make a separate build step (with bash or make) to call ruby scripts, and generate derivatives somehow
    - still have to flesh this out, but could e.g. generate `*.css.rb` -> `*.gen.css`, and that way they might not be too hard to clean and gitignore
    - the downside to this is that then i'm kinda hacking a ruby prebuild step into a svelte project
    - the upside is that it's simple, and i could include e.g. python later if i wanted using the same system
  - could i use a svelte plugin?
    - googleing around, it looks like people have combined svelte and rails, but i don't see anything readymade for my simple purposes
  - could i use a vite plugin?
    - https://github.com/ElMassimo/vite_ruby/tree/main/vite_rails
    - https://github.com/ElMassimo/vite_ruby/tree/main/vite_hanami
    - https://getlavanda.com/engineering/2020-10-30-hanami-vs-ruby-on-rails
    - looks like these are set up for making a ruby web app, packaged with vite -- rather than what i want, which is just a simple "import and convert this for me" type of plugin
  - looks like it's gonna be a prebuild step, at least for now
- prebuild setup
  - can i watch source files for changes and rebuild automatically?
    - using `inotifywatch` https://serverfault.com/a/780522 -- which doesn't seem to be present, or available on macports
  - where to put files?
    - current ideas are `lib` (a general place for libraries local to my project) and `src/pre` (a place for things that need to be prebuilt)

#### (2021-11-09)

- references
  - ruby
    - `freeze` https://www.honeybadger.io/blog/when-to-use-freeze-and-frozen-in-ruby/
    - writing docs
      - https://stackoverflow.com/questions/1681467/how-to-document-ruby-code
      - rdoc https://github.com/ruby/rdoc
      - yard is newer, but not installed by default https://yardoc.org
  - asciidoctor
    - vscode indent helper (feature request) https://github.com/asciidoctor/asciidoctor-vscode/issues/456
- research
  - lots of good ideas on how to keep build artifacts up to date (like for stuff that i'm prebuilding) https://stackoverflow.com/questions/7539563/is-there-a-smarter-alternative-to-watch-make
    - oh good, cloudflare does have make https://developers.cloudflare.com/pages/platform/build-configuration
  - shebang with multiple arguments https://unix.stackexchange.com/questions/399690/multiple-arguments-in-shebang
    - says it's possible starting in coreutils 8.30
    - currently i'm running 8.32 via macports
    - not sure what version of coreutils cloudflare is running though
  - html entities https://dev.w3.org/html5/html-author/charref
- questions
  - i wonder if i can make a special asciidoc block type for svelte code
    - extensions https://docs.asciidoctor.org/asciidoctor/latest/extensions/block-processor/
    - block delimiters https://docs.asciidoctor.org/asciidoc/latest/blocks/delimited/
  - svelte treats curly braces as special -- but they're not escaped in asciidoc source blocks
    - escaping curly braces in svelte https://stackoverflow.com/questions/61089042/using-svelte-how-can-i-escape-curly-braces-in-the-html
    - asciidoc substitutions
      - https://docs.asciidoctor.org/asciidoc/latest/subs/special-characters/
      - code https://github.com/asciidoctor/asciidoctor/blob/main/lib/asciidoctor/substitutors.rb
      - doesn't look like you can add to the list of special characters, unless maybe you can runtime modify the code used to detect and substitute them
- prebuild setup
  - gonna put stuf in `/lib`
    - because otherwise, e.g. for the asciidoctor preprocessor, there will be stuff that belongs in `/src/pre` (like the rouge css) and other stuff that belongs in `/src/lib` or something (like the conversion function)
    - and actually... it might make a lot of sense to do that -- but on the other hand, i want to keep `/src` as stuff that can also be run client side, and right now this is stuff that can only be run at build time
  - https://git-scm.com/docs/gitignore
    - no way to ignore e.g. `rouge.css` if `rouge.css.erb` exists
  - makefile
    - https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html
    - https://www.gnu.org/software/make/manual/html_node/Multiple-Targets.html
    - https://www.gnu.org/software/make/manual/html_node/One-Shell.html
    - https://stackoverflow.com/questions/453447/in-unix-can-i-run-make-in-a-directory-without-cding-to-that-directory-first
    - makefile wildcard and patsubst https://www.gnu.org/software/make/manual/html_node/Wildcard-Function.html
- rouge css
  - https://github.com/rouge-ruby/rouge/issues/343#issuecomment-219926122
  - https://alexpeattie.com/blog/better-syntax-highlighting-with-rouge/
  - might need to use pygments styles
- pygments css
  - Pipfile doesn't support "greater than" python version https://stackoverflow.com/questions/51492423/pipenv-specify-minimum-version-of-python-in-pipfile
  - https://github.com/richleland/pygments-css
    - doesn't have all the styles, but does have a command line that will generate them
  - i forget now where i saw this, but to generate css with pygments in a python script `from pygments.formatters import HtmlFormatter` and `print(HtmlFormatter(style='solarized-light').get_style_defs())`
  - and you can also generate css for all the styles :) see source code

#### (2021-11-10)

- python
  - formatter
    - prettier plugin deprecated, recommends using black https://github.com/prettier/plugin-python
    - black https://github.com/psf/black
    - black vs yapf https://news.ycombinator.com/item?id=17155048
  - pipfile https://github.com/pypa/pipfile
  - pipenv
    - install https://pipenv-fork.readthedocs.io/en/latest/basics.html#pipenv-install
      - if you pass a package, it will install it and add it to the
- ruby
  - `require_relative` https://stackoverflow.com/questions/16856243/how-to-require-a-ruby-file-from-another-directory
  - gonna change and install gems to the system https://bundler.io/man/bundle-install.1.html#SUDO-USAGE
    - even though it's annoying to have to type in my password every time something new is installed (since i'm still in the process of installing stuff)
  - formatting
    - rufo vs rubocop https://flexport.engineering/approximating-prettier-for-ruby-with-rubocop-8b863bd64dc6
    - rufo https://github.com/ruby-formatter/rufo
    - hmm, rufo isn't really formatting the way i want -- also, if i run rubocop for linting, it warns me about style things?
    - the link above actually recommends considering the prettier plugin, though it wasn't mature enough when they tried it
    - so i want to use prettier for formatting, and rubocop for warnings and such
      - but i want to keep my trailing commas lol
        - configuration docs https://docs.rubocop.org/rubocop/configuration.html
        - defaults https://github.com/rubocop/rubocop/blob/master/config/default.yml
        - flexport example https://gist.github.com/maxh/2fc21e458cde793532d15411f061bd92
        - fixing a warning https://github.com/rubocop/rubocop/issues/7771
  - keyword arguments https://thoughtbot.com/blog/ruby-2-keyword-arguments
  - default arguments https://learn.co/lessons/methods-default-arguments
  - separation of positional and keyword arguments in ruby 3.0 https://www.ruby-lang.org/en/news/2019/12/12/separation-of-positional-and-keyword-arguments-in-ruby-3-0/
  - kwargs https://mikerogers.io/2020/08/17/ruby-using-the-double-splat-with-keyword-arguments
  - ruby update hash
    - update https://stackoverflow.com/questions/5215713/ruby-what-is-the-easiest-method-to-update-hash-values and a few rails functions
    - dig https://stackoverflow.com/questions/5544858/accessing-elements-of-nested-hashes-in-ruby/34624909 kind of like `?.` in javascript
      - ruby does conditional chaining too, with `&.` https://stackoverflow.com/questions/33735228/why-does-ruby-use-its-own-syntax-for-safe-navigation-operator
        - ahh, but for methods only?
        - hmm, can't access has keys with dot in normal hashes either https://stackoverflow.com/questions/9356704/unable-to-use-dot-syntax-for-ruby-hash
        - so, use `dig` for safe hash access
    - https://stackoverflow.com/questions/23985520/passing-splat-on-nil-as-argument
  - hash syntax https://launchschool.com/books/ruby/read/hashes
  - single vs double quotes https://rubyinrails.com/2014/01/03/ruby-difference-between-single-and-double-quotes/
    - and backticks are for shell commands https://readysteadycode.com/howto-execute-shell-commands-with-ruby-backticks
    - interesting about quotes https://avdi.codes/overriding-the-backtick-in-ruby/
  - fun stuff about the splat operators https://www.honeybadger.io/blog/ruby-splat-array-manipulation-destructuring/
  - double splat doesn't work on nil like i thought it did https://stackoverflow.com/questions/34125769/double-splat-on-nil and i'm not alone :)
    - https://bugs.ruby-lang.org/issues/8507 the inconsistency is acknowledged, but they didn't see a reason to change it (and would have rather made `*nil` raise an error if they did want to make it consistent)
  - array syntax
    - `{'a': 'b'} == {'a' => 'b'}` => `false`
      - this is because `{'a': 'b'}` has `:a` as the key, while `{'a' => 'b'}` has `'a'` as the key
    - can try this out in `irb` :)
  - https://blog.appsignal.com/2018/09/11/differences-between-nil-empty-blank-and-present.html
  - more about formatters
    - https://metaredux.com/posts/2019/03/30/the-missing-ruby-code-formatter.html
    - https://github.com/ruby-formatter/rufo rufo preserves some spacing choices
  - pretty print https://docs.ruby-lang.org/en/2.4.0/PP.html
- asciidoctor extensions https://docs.asciidoctor.org/asciidoctor/latest/extensions/
- asciidoctor special block
  - example https://docs.asciidoctor.org/asciidoctor/latest/extensions/block-processor/
    - https://www.rubydoc.info/github/asciidoctor/asciidoctor/Asciidoctor%2FExtensions%2FProcessor.use_dsl
- python
  - linting
    - pre for just one thing in pipfile https://github.com/pypa/pipenv/issues/1760
      - doesn't seem to be allowed yet
      - black might just release a stable version first lol we'll see
    - can use `==` for black rather than allowing pre for everything https://github.com/psf/black/issues/517 but then new versions of black aren't pulled

#### (2021-11-11)

- was thinking of using poetry or pdm instead of pipenv -- but [cloudflare pages](https://developers.cloudflare.com/pages/platform/build-configuration) has pipenv, and not the others (at least not listed)
- working on makefiles
  - https://www.gnu.org/software/make/manual/html_node/Phony-Targets.html
  - interesting, haven't used these https://www.gnu.org/software/make/manual/html_node/Static-Pattern.html#Static-Pattern
  - recursive make, passing target https://stackoverflow.com/questions/23701771/how-to-pass-target-name-to-list-of-sub-makefiles
    - https://stackoverflow.com/questions/3477292/what-do-and-do-as-prefixes-to-recipe-lines-in-make
      - https://www.gnu.org/software/make/manual/make.html#How-the-MAKE-Variable-Works
    - https://www.gnu.org/software/make/manual/html_node/Goals.html
  - can a rule match anything? https://www.gnu.org/software/make/manual/html_node/Match_002dAnything-Rules.html
    - more about patterns https://stackoverflow.com/questions/42983728/makefile-pattern-matching-not-working-as-i-expected
  - https://www.gnu.org/software/make/manual/html_node/Text-Functions.html
- linting
  - ruby
    - frozen string literal
      - https://stackoverflow.com/questions/37799296/ruby-what-does-the-comment-frozen-string-literal-true-do
      - https://github.com/rubocop/rubocop/issues/7197
    - if a method is too long (over 10 lines) https://github.com/rubocop/rubocop/issues/494
  - python
    - `No name 'HtmlFormatter' in module 'pygments.formatters'`
      - https://github.com/PyCQA/pylint/issues/491
      - recommended at this time i think to disable the warning
- formatting
  - prettier wants to have a fixed version https://prettier.io/docs/en/install.html
  - using prettier with rubocop https://github.com/prettier/plugin-ruby#usage-with-rubocop
  - different ways of ignoring files for prettier being considered https://github.com/prettier/prettier/issues/8048 -- currently, it looks like there can only be one ignore file, and my package.json currently sets it to `.gitignore` (which was the default in the sveltekit skeleton project)
- (taking this out of my "dependencies" section for ruby)
  - if you need to switch between versions, recommend using [chruby](https://github.com/postmodern/chruby) (with bass, if you're using fish shell), with rubies installed via [ruby-install](https://github.com/postmodern/ruby-install) (both available via macports) -- for automatic switching in fish, you could try [chruby-fish](https://github.com/JeanMertz/chruby-fish), or else write your own [event handler](https://fishshell.com/docs/current/language.html#event-handlers) -- [vscode](https://code.visualstudio.com) might also have a plugin
- can i have ruby comments auto comment the next line, like other languages do?
  - https://stackoverflow.com/questions/54617164/continue-ruby-comment-on-enter-in-visual-studio-code
  - https://marketplace.visualstudio.com/items?itemName=kevinkyang.auto-comment-blocks
- asciidoctor customizing output
  - https://discuss.asciidoctor.org/Creating-a-visible-Revision-History-td6594.html
    - template https://github.com/asciidoctor/asciidoctor-backends/blob/master/haml/html5/document.html.haml
  - converters https://docs.asciidoctor.org/asciidoctor/latest/convert/
    - templates https://docs.asciidoctor.org/asciidoctor/latest/convert/templates/
      - https://github.com/asciidoctor/asciidoctor-backends/tree/master/slim/html5
- gonna change things -- old ruby code (incomplete) that i'd like to remember

  ```ruby
  def load(**kwargs)
    options = {
      safe: :server,
      **kwargs,
      attributes: {
        'relfilesuffix' => '/',
        'source-highlighter' => 'rouge',
        **(kwargs[:attributes] || {}),
      },
    }
    Asciidoctor.load($stdin.read, **options)
  end

  doc = load(base_dir: File.dirname(ARGV[0]))
  ```

- ruby
  - command line option parser https://ruby-doc.org/stdlib-2.4.2/libdoc/optparse/rdoc/OptionParser.html
  - https://stackoverflow.com/questions/1255324/p-vs-puts-in-ruby
  - command line name of script https://stackoverflow.com/questions/4834821/how-can-i-get-the-name-of-the-command-called-for-usage-prompts-in-ruby
  - https://stackoverflow.com/questions/2449171/how-to-parse-an-argument-without-a-name-with-rubys-optparse
  - constant private vars in classes https://frontdeveloper.pl/2019/01/not-so-private-constants-in-ruby/

#### (2021-11-12)

- thinking
  - i thought before about how highlight.js only allowed client side highlighting, but i'm not sure that's right -- thinking about it, i suspected that was the case 😅 but it sounded annoying, and i thought the ruby solution might be cleaner -- having learned (yay!) as much as i have, and spent this much time (lol, such is the cost of learning) i'm now feeling like reinvestigating highlightjs -- i'm glad i got python and ruby working in my environment, and a little prebuild makefile setup going on, and for the improvements in my bash scripts, and especially for learning more about ruby (and asciidoctor), and how to set up an environment, and such -- but it would be nice (always) to remove dependencies and keep everything in ... fewer languages -- also, someone mentioned that asciidoctor.js should be about feature equivalent to the ruby version by now (not sure when that comment was written) -- and asciidoctor templates (which it's looking like might be my best option) aren't compatible between the ruby and js versions (because of different templating langauges support in the base languages, i think), so it seems like a good time to circle back -- oh and also, i think i'd been feeling like the ruby version would be easier to look stuff up about, or be more flexible and such, but i've gotten much better at navigating their docs (which are fairly good, but confusing at first) and learned that the js version should be about the same in terms of flexibility -- also again, now that i've fussed with makefiles and figured out a good feeling way to do that, i think i'd be comfortable making a typescript library that was built so that it could be included in svelte.config.js, or at least, i think that wouldn't be harder than all the ruby stuff i've been doing
  - so we're back to the issue that i stopped on before: can you server side highlight with highlight.js in asciidoctor?
- research
  - hmm, so syntax highlight section is in asciidoctor, but not asciidoctorjs, in the processing tab
    - here https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/
    - not here https://docs.asciidoctor.org/asciidoctor.js/latest/
  - ahh, and in the asciidoc tab, source highlighting section talks about both https://docs.asciidoctor.org/asciidoc/latest/verbatim/source-highlighter/
  - and putting in the classes (for css to style later) was an option i saw with rouge, not highlight.js https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/rouge/#rouge-attributes
  - so... i don't see what i thought -- i was correct before lol in that server side highlighting with highlight.js doesn't appear to be officially supported in asciidoc (or asciidocjs, though... i think that's a little weird)
  - so i think i can
    - use ruby (of course)
    - write a custom adapter https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/custom/
      - someone already did this, haven't tried it https://github.com/jirutka/asciidoctor-highlight.js/
        - has some (small?) limitations
        - looks like a lot more work than i would at first suspect 😉
    - find a way with templates (?) or extensions to pass source to highlight.js (installed as a node library)
      - this might actually not be bad, since i already need to find a way to escape `{` and `}` in my source blocks (and possibly elsewhere -- ideally everywhere that's not a passthrough)
    - hope (and test) that svelte runs highlight.js during prerender
      - oh wait, i think i tried this, and found out that it tried to look at `window` or `document` or something, which isn't available during prerender -- so then there was that small sidetrack about "it's possible to prerender stuff serverside with a headless chrome..." which is cool, but feels like an overkill solution
  - people's syntax highlighter preferences
    - https://elixirforum.com/t/what-syntax-highlighter-do-you-use-for-web-projects/26289/7
    - kinda seems like people use whatever is easy to use in their language
    - https://docs.racket-lang.org/pollen/mini-tutorial.html
    - pygments seems kinda universal
  - i was thinking before that one advantage of using highlight.js is that then i could use the same setup in-browser (if i ever wanted) -- but if it's in browser, i could just use highlight.js as cdn served javascript, so that's easy -- though, styles might be different between that and e.g. rouge, so it might be best to be consistent anyway... 🤔
  - highlight.js has some cool themes
    - https://highlightjs.org/static/demo/
    - https://github.com/highlightjs/highlight.js/tree/main/src/styles
  - pygments of course has plenty of themes https://pygments.org/styles/
  - (themes)
    - i don't see any mention of them being compatible or translatable (googling around)
    - paraiso (light and dark) look nice, and are available on both
    - pygments also has solarized, unlike hljs (highlight.js)
    - and pygments preview is nicer lol
    - hljs seems to have more themes
    - hljs kimbie (light and dark) also look nice
  - lots of stuff on the asciidoctor github (though, i don't see any themes) https://github.com/asciidoctor
  - asciidoctor skins https://github.com/darshandsoni/asciidoctor-skins
    - demo page http://darshandsoni.com/asciidoctor-skins/
    - probably has a really good test document, so i want to look at that later
    - another asciidoctor demo / test document https://github.com/opendevise/asciidoc-samples/blob/master/demo.adoc
  - interesting, about the paraiso theme https://github.com/idleberg/Paraiso.tmTheme
- so after all that, maybe ruby is the way to go
  - keeping js code for now
  - also, finally checking all this in -- planning to clean up before merge, but i want it in the history now that my possible directions are a bit more clear
- working on ruby solution
  - which templating engine to use?
    - https://www.clariontech.com/blog/revisiting-template-engines-in-ruby-on-rails
    - i feel like i like [slim](http://slim-lang.com) best -- still -- thought that when i looked at the [asciidoctor-backends](https://github.com/asciidoctor/asciidoctor-backends) repo too -- :)
  - slim pretty mode
    - https://github.com/slim-template/slim/issues/428
    - alternativly, pretty print html afterwards https://stackoverflow.com/questions/2191989/a-command-line-html-pretty-printer-making-messy-html-readable
    - ahh, passing options to template engins https://docs.asciidoctor.org/asciidoctor.js/latest/extend/converter/template-converter/#template-options
    - and the option to set https://github.com/slim-template/slim
  - slim references
    - https://github.com/slim-template/slim
    - http://slim-lang.com/index.html
  - types of URI https://stackoverflow.com/questions/743247/names-for-types-of-urls
  - references for template solution
    - doc attributes https://docs.asciidoctor.org/asciidoc/latest/attributes/document-attributes-ref/#document-metadata-attributes
    - common template api https://docs.asciidoctor.org/asciidoctor/latest/convert/templates/#common-apis
    - convertible contexts (allowed template names) https://docs.asciidoctor.org/asciidoctor/latest/convert/contexts-ref/
    - slim examples and reference https://github.com/slim-template/slim
    - document template https://github.com/asciidoctor/asciidoctor-backends/blob/master/slim/html5/document.html.slim
    - template helpers https://github.com/asciidoctor/asciidoctor-backends/blob/master/slim/html5/helpers.rb
  - trying templates
    - will they still work if i only want to change part of something?
    - e.g. the template just for the header is kinda long https://github.com/asciidoctor/asciidoctor-backends/blob/master/slim/html5/_header.html.slim so it'd be nice to just leave that as is, without pulling the template file into my code
  - https://discuss.asciidoctor.org/Small-changes-to-default-HTML5-templates-td6851.html
    - someone trying to make small modifications (i think) -- which is kinda like what i want
    - sounds like templates and extensions might have the same problem...? not sure how to verify this
    - with extensions, you can call `super` ... but i'm not sure if you can modify super -- could easily be wrong, but it feels unlikely
      - the [documentation](https://docs.asciidoctor.org/asciidoctor/latest/convert/custom/) only shows an example of either calling `super` or overriding the method completely -- and i'm doubing that there's a way in ruby to say "change the third statement of `super` to ..." (and, if there was, that sounds super brittle) -- without going to look in the asciidoctor source, i'm guessing they implemented these roughtly like the asciidoctor-backends project did (except possibly in ruby), and that you have to override a whole context at a time -- inconvenient that one of the contexts i want to override is `document`, which is so big
  - someone mentioned docinfo https://docs.asciidoctor.org/asciidoctor/latest/docinfo/ but i don't think it works for the type of customization i need

#### (2021-11-13)

- working on templates
- https://stackoverflow.com/questions/3632166/should-i-include-a-meta-generator-tag
- asciidoctor character replacements https://docs.asciidoctor.org/asciidoc/latest/attributes/character-replacement-ref/
  - not done on source blocks by default anyway https://docs.asciidoctor.org/asciidoc/latest/subs/replacements/
  - i'll have to look at where in insert the additional substitutions i need
- in the document template, there's the line `title=((doctitle sanitize: true) || (attr 'untitled-label'))` -- had to look really hard for what `sanitize: true` meant, and why it was allowed (i thought doctitle was a string) -- turns out it's a function https://github.com/asciidoctor/asciidoctor/blob/a227fcd0129e03e2478390a27dd02965a3146ab4/lib/asciidoctor/document.rb#L733
- hmm -- i can't get docinfo to work when i passin the document as a string -- it works when i pass in the file name (and pass out a string) -- but when i pass a string it doesn't, even when i set
  ```ruby
  options = {
    safe: :unsafe,
    attributes: {
      'docdir' => 'src/routes/blog',
      'docinfo' => 'private',
      'docinfodir' => 'src/routes/blog',
    },
    template_dirs: ['lib/asciidoctor/templates'],
  }
  ```
  - not sure if this matters, since i'm customizing things things enough that i can work around it
  - but it is making me wonder if i should, e.g. put script and style things into custom docinfo files (that i then look for and load myself in my convert script or template)
    - that would be more in line with how asciidoctor does things -- but less in line with how svelte does things...
    - so for now i think i like my first idea of making custom `[svelte]` blocks -- which is more in like with svelte and less in line with asciidoctor, but..
  - or i could just pass in the file names... but that would break an expectation (that i'm not currently planning to use, but still) of the svelte preprocessing data flow
  - oops, in this case `'docinfodir' => '.'` would have been correct -- still doesn't work when i load the content string from stdin lol (and when i'm loading the file by filename, it isn't needed)
- thinking about whether it's actulaly that helpful to write in asciidoctor, since vscode makes writing html easy (and there's pug if i want)
  - comparing
    - asciidoc https://raw.githubusercontent.com/darshandsoni/asciidoctor-skins/gh-pages/index.adoc
    - html https://github.com/darshandsoni/asciidoctor-skins/blob/gh-pages/index.html
    - render (with a skin) https://darshandsoni.com/asciidoctor-skins/
  - table of contents might be work
    - here's a really good example of someone making one https://github.com/janosh/svelte-toc -- with a link to their blog
  - links are also cleaner in asciidoc
  - and maybe lists -- but asciidoctor adds a lot of (maybe) unnecessary `<div>`s and `<p>` and such
  - it's so much work to try both...! lol -- but i might have to -- or just try writing html
  - it would be an easier decision if i was going to use asciidoctor as is, along with a skin that someone else made (or the default styles) -- since i'm doing so much myself (making templates, planning to re-style) it's a harder decision
  - really, as long as the html isn't too hard to write directly, my only concern is keeping it future portable
  - also, writing it directly would allow me to drop python (for pygments styles) and ruby (for asciidoctor) as dependencies -- as much fun (and work) as i've had getting to this point.. :)

#### (2021-11-15)

- trying to get pug working... thought this would be easy
  - https://github.com/sveltejs/svelte-preprocess/blob/main/docs/preprocessing.md#difference-between-the-auto-and-stand-alone-modes
    - but `import { pug } from "svelte-preprocess";` fails when i try to run it
  - https://simonplend.com/node-js-now-supports-named-imports-from-commonjs-modules-but-what-does-that-mean/
    - yay :) figured it out -- though i'm not sure i completely understand what's going on
    - `import preprocess from "svelte-preprocess";` works
    - `import { pug } from "svelte-preprocess";` does not
    - `import preprocess from "svelte-preprocess";` followed by `const { pug } = preprocess;` works
    - and thus, `preprocess.pug` is also what i want
    - the weird thing here is that `preprocess` is not behaving the way i'd expect it to, based on the typescript types -- but i think this article explains what's going on -- i'm just happy i got it to work, and moving on (and letting it simmer) rather than digging deep into this just now
- js mapping over an obj https://stackoverflow.com/questions/14810506/map-function-for-objects-instead-of-arrays
- js default return valye is `undefined` (except for a constructor called with `new`) https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions
- some fun functional programming in js https://opensource.com/article/17/6/functional-javascript
- you can use jstransformers as filters in pug
  - https://pugjs.org/language/filters.html
  - https://github.com/jstransformers/list-of-jstransformers/blob/master/list-of-jstransformers.json

#### (2021-11-17)

- https://stackoverflow.com/questions/271743/whats-the-difference-between-b-and-strong-i-and-em
  - https://stellify.net/html5-b-and-i-tags-are-going-to-be-useful-read-semantic-again/
- looking at color schemes more, the paraiso ones don't have the best contrast (at least the light theme)
  - also, sometimes highlightjs is more colorful than pygments (for the same theme) but i dunno if i like it better
- still considering whether to keep going with asciidoc
  - pug is looking quite nice -- and if i restrain myself (lol) it should be at least as portable as asciidoc (or, that's my current feeling)
  - i might keep pygments around for highlighting, which would keep python around -- wouldn't need ruby anymore, but could keep it around anyway just in case
  - if i use asciidoc, i think i'll write all new templates, and style it myself... which is why i almost might as well not use it -- in any case, if i'm doing that, i could call out from a template and highlight with pygments or whatever i like, so there's no need to use the ruby asciidoctor -- i could use asciidoctor.js
    - it'd be fun to mess with ruby, and template in slim, but.. every different thing i use is something i may have to remember and re-learn in the future -- so best to keep the list... down, if not small
- github markup library https://github.com/github/markup
- `@import` inside `@media` https://stackoverflow.com/questions/14943719/import-styles-not-working-in-a-media-query
- recommends `<pre><code>` https://stackoverflow.com/questions/4611591/code-vs-pre-vs-samp-for-inline-and-block-code-snippets
  - https://html.spec.whatwg.org/multipage/grouping-content.html#the-pre-element

#### (2021-11-18)

- working on pygments and such
- decided to move from `lib` to `src/lib`, since i'll probably modify and check in the css files i want to use
- remapping caps lock and escape on mac
  - my usual mapping of "df" to "esc" is causing some annoyance -- especially when typing "cloudflare pages" hehe
  - https://www.jeffgeerling.com/blog/2017/remapping-caps-lock-key-escape-macos-sierra
    - can remap caps lock to escape
  - don't see how to remap escape to capslock -- without a third party program -- gonna see if i need it and worry about it then
- pipenv isn't working anymore...? because it can't find the pipfile
  - env var for pipfile https://github.com/pypa/pipenv/issues/2237
  - env var for search depth https://github.com/pypa/pipenv/issues/183
    - which doesn't seem to exist anymore? https://pip.pypa.io/en/stable/topics/configuration/
  - yet again, i really would prefer to use something other than pipenv -- i wonder if cloudflare pages supports poetry or pdm https://dev.to/frostming/a-review-pipenv-vs-poetry-vs-pdm-39b4
  - comments on why pdm was started https://github.com/pdm-project/pdm/issues/392
- considered doing `pipenv install pdm` and then seeing (later) whether i could use pdm that way on cloudflare
  - pdm requires python 3.7, which is the latest python cf pages supports (lol)
  - but maybe this is just complicating life... if i put the project back in `lib` everything works as is (at least, on my machine, still haven't tested on cf)
  - also, putting it in `lib` does make it clear (to me) that these are sort of separate things, that may be called by `svelte.config.js`, and aren't meant to run at application runtime -- even though nothing is, since the entire thing is compiled to static pages lol
- reading more about semantic html5 tags
  - good reference https://learn-the-web.algonquindesign.ca/topics/html-semantics-cheat-sheet/
  - `<s>` vs `<del>` https://stackoverflow.com/questions/16743581/what-is-the-difference-between-s-and-del-in-html-and-do-they-affect-website
- thinking more about pug (html) vs asciidoc
  - if i do write in pug, i should probably still go through the asciidoctor templates, to see where and how they put stuff
- starting to set up css for code
  - interesting -- if i use `@import "..."` i have to use the absolute path, but if i use `@import "..." (all)` (or another media query) i can use `$lib` and it will work
  - hmm, if i take out `lang="scss"` from the `<script>` tag, i can use `$lib` in my path
  - so, using a media query might be leaving the import for css to handle, rather than sass
  - ooo, and i can still import `.scss` styles in a css style tag
    - this might be because css imports don't check filetype -- or something like that -- these files it was working with don't have any non-css in them (yet)
  - to force plain css imports in sass https://sass-lang.com/documentation/at-rules/import#plain-css-imports

#### (2021-11-19)

- working on pug things and experimenting
  - mixin content https://github.com/pugjs/pug/issues/1623
  - see 'inheritance' https://www.educative.io/edpresso/what-is-pug-syntax -- so, `block` is from template inheritance
    - haha yup https://pugjs.org/language/inheritance.html
  - don't see a way to access `block` as a variable
    - some people discussing the issue, mostly a long time ago https://github.com/pugjs/pug/issues/2055
    - more discussion about mixins and blocks (started a long time ago, continuing) https://github.com/pugjs/pug/issues/1693
  - pug is rendered by express? https://stackoverflow.com/questions/47626768/globally-set-dynamic-pug-variables
    - or maybe, it just can be? dunno what expressjs is used for
  - importing js (has to be done with pug options, looks like) https://github.com/pugjs/pug/issues/2604
  - how to use pug globals https://github.com/pugjs/pug/issues/3198

#### (2021-11-20)

- for pug -- maybe mixins are what i want after all
  - hmm, true in some ways (it doesn't make the main content all indented 1 extra level, gives more control, ...), and not in others (adds some boilerplate)
  - i could also use pug directly -- the svelte-preprocess code that actually calls pug isn't that long (and i don't need the stuff they have in other files, for dealing with `<template ...>` tags)
- trying svelte again
  - it's amazing how close it ends up being to pug (for these small examples)
  - didn't finish -- would still have some work to do, and pug at least doesn't have messy closing brackets everywhere, so that's nice
  - i could also use pug (or asciidoc or markdown) in a svelte file directly, if i wanted
    - but that would add an extra level of indent, and i'd have to test about syntax highlighting and formatting
  - also, it's nice how in svelte files, the ts and scss blocks are formatted by prettier
- trying asciidoc again
  - amazing how easy it is lol -- again, for this small example, something are much harder (at least, for me right now)
  - pug feels more fun -- it feels like programming -- but i wonder if that would be distracting (lol)
  - also, pug doesn't allow as easily for the kind of templating asciidoc does -- which is both nice (because far less templating is required in pug), and less nice (because it means adding boilerplate to my pug documents)
- looking again at markup languages https://github.com/github/markup
  - if i'm really going to consider using asciidoc, it wouldn't actually be harder to pipe my code through a ruby script... (which is... actually exactly what i'm considering doing for asciidoc) -- which opens up options
  - just learned about textile https://textile-lang.com/
    - seems pretty featureful, and flexible
    - ahh, but it bugs me that e.g. you can write `h2. ...` for a heading, but then `[1]` and `fn1. ...` (which is not an html tag) makes a footnote, and for html tags that aren't directly supported you have to write out the html in its entirety -- and spans are denoted by special characters (`%`)... -- so anyway, it doesn't feel consistent, or easy to remember
  - and org mode
    - https://irreal.org/blog/?p=6627
    - https://karl-voit.at/2017/09/23/orgmode-as-markup-only/
    - ahh, but https://github.com/wallyqs/org-ruby -- it doesn't support as much as asciidoc (outside emacs) it looks like, and isn't as extensible (e.g. i don't see macros or something like custom blocks in their list of supported features)
  - https://stackoverflow.com/questions/659227/compare-and-contrast-the-lightweight-markup-languages
  - hmm, so, what i'm looking for -- something with
    - a way to specify e.g. 'title' once, and have it appear in both `<head><title>` and `<body><h1>`
    - footnotes, or an easy way to script them -- and similar things
    - so in general
      - a way to have one thing go to multiple places
        - this basically necessitates templates, or some other way to process the document
        - svelte makes this a little hard, since it's not the easiest to inject html in (possibly) existing elements -- hmm... or maybe i just haven't messed with it enough... it might not be too bad (and i'd only really have to figure it out once)
        - on the other hand, i like pug better than svelte for blog entries, and if i'm using pug, i think using mixins would feel more natural -- it's not _that_ much boilerplate...
      - a way to generate and transform things -- e.g. footnotes, or things that aren't trivial in html (or trivial in the markup language, but i want to make them trivial when typing/reading)
    - also, i'd like it to be easy to type and read, with good vscode support for syntax highlighting, etc.
  - yup, so it looks like my favorite choices are still pug and asciidoc
  - pug is _really tempting_
    - but then, thinking about footnotes again -- in pug, i'll have to code them in javascript (since they'll need to generate a number and an id, and then put their text at the bottom, with links) -- i'd probably end up writing some mix of javascript, pug, and svelte to get it done -- hehe, which sounds fun, but in asciidoc, since it get's parsed into an AST and then translated (based on either code or templates) (and the ids and numbers are handled by the engine) this is as trivial as some iteration code in a template on my end; and cleaner to write in the blog post too
    - also, in asciidoc, i can escape out to pug or svelte (with passthrough blocks) whenever i want to do something asciidoc doesn't support, or which is just easier to do that way
    - i can also escape (well... it's different but still) into asciidoc (or markdown, etc) from pug or svelte -- so really, i can mix things as i like -- but it seems like most of the time asciidoc (or even markdown) would be easiest to write in
  - also, part of me really wanted to write in html, because getting more familiar with it :) and also because that's my target (and my only target) anyway -- but you know, i'll probably get plenty familiar with it by writing those templates...
- thinking about asciidoctor.js vs asciidoc (ruby) again
  - for the sake of syntax highlighting, ruby would be the easy choice, but... since i'm going to customize templates, i could do server side highlighting with the js version pretty easily -- so for the sake of limiting dependencies, it'd be better to use js
  - but, i also like rouge (and pygments) output better than highlightjs, which means pulling python anyway...
    - so i could pull in python to highlight with asciidoctor.js
    - or i could pull in ruby to translate (and generate and check in the pygments style, which i'm planning to do anyway so i can modify it a bit)
    - or i could pull in both ruby and python because why not
    - but i can't have only js...
  - so what would be better for others to reuse?
    - not sure... both will probably be pretty hard to reuse, to be honest
  - what will be a better learning experience?
    - haha probably ruby, since i feel like i've got pug down already (which is what i'd use for templates with asciidoctor.js) from experimenting
  - what will be better for maintainability?
    - ooo, asciidoctor.js with pug probably... but really it just comes down to whatever i'm more likely to remember in the future -- and on what cloudflare pages decides to do with stuff in the future (or other places, if i switch hosts)
      - though, on that note, i can always just build locally and push that
- working on ruby templates
  - get script dir https://stackoverflow.com/questions/2206714/can-a-ruby-script-tell-what-directory-it-s-in

#### (2021-11-21)

- still working on deciding between asciidoctor and asciidoctor.js
  - i'd prefer ruby, i think, because it feels cool, and it's the reference implementation
  - js would work, especially since i could call out to python (or anything) for syntax highlighting
  - ruby makes page refreshes a bit slow -- but this is relative to using pug, haven't tried recently with asciidoctor.js
- wanting to see if i can get ruby and python both working on cloudflare pages :)
  - failed at ruby... `Unknown ruby interpreter version (do not know how to handle): ~>3.0`
  - i guess pessimistic version matching isn't supported on ruby versions by whatever's installing ruby here?
  - don't really wanna specify exactly...
  - `ruby '>= 3.0', '< 4.0'` didn't work either
  - https://bundler.io/gemfile_ruby.html
  - https://guides.rubygems.org/patterns/#pessimistic-version-constraint
  - well... looks like i can either '>= 3.0' or just say '3.0.2'
  - tried `ruby '>= 2.7', '< 4.0'`, didn't work
  - `>= 3.0` didn't work
  - trying `3.0.2` -- but its default is 2.7.1, so i'm guessing that's the latest they have...
  - ... same error for 3.0.2 -- so "do not know how to handle" must also mean "don't have this version"
  - same error for `ruby '>= 2.7'`
  - but it seems to go ahead and install the gems anyway... lol
  - having python and ruby in here doesn't seem to make it take longer to initialize the build environment
  - `pipenv run ...` is failing with `pipenv: commnd not found` (from make in `lib/pygments`)
  - trying `ruby '2.7.1'`
    - hmm... locally, i can `chruby 2.7` and get 2.7.4 (since i installed it before!) -- but 2.7.4 != 2.7 and i can't say `>=2.7` in my Gemfile, because that will fail on cloudflare -- but i don't wanna be messing with trying to get exact minor versions on my dev machine... and i don't really feel like building a new ruby just now
    - super tempted to just build locally, and not make cf pages do the build... check in the built files and let cf pages just do the deploy
    - thinking about it more... the reason it's nice to have pages do it (besides not having to check in generated files) would be because in a professional setting, you'd want everyone testing with the same build environment, and that's easier to do if you build on a server somewhere -- but -- given these shortcomings, i don't think i'd want to build on cf pages professionally... so i'd be stuck solving that issue anyway
    - github (and gitlab) have ci pipelines you can run, right? -- not for free repos, but for public ones? -- worth it?...
    - hmm, actually, it seems to work even when it fails... as long as the selector accepts 2.7.1, which is the version cf pages seems to want to use
    - gonna keep `ruby '>= 2.7', '< 4.0'` and see how that goes for now
  - trying putting pipenv as a dependency in pipenv...
    - fails install on my machine
  - trying, trusting `pipenv shell` to have been run
    - ahh, but linting fails because `black` isn't found... and how can i make sure a venv is running when linting during a git hook?
    - i suppose linting isn't done server side, so i can use pipenv there...
  - pygmentize call in svelte.config.js also fails, so added pipenv run back there too... though i feel like that will fail on cf pages
    - and yup, that failed in cf pages
    - not sure what to do about that... -- like, i could write a `pipenv` command that tests whether we're in a venv and if we are then it just calles the script (assuming `run`) and if we're not it passes it's arguments to the real pipenv (and if neither is true, it would have failed no matter what) -- but i don't want to
  - so overall what this means is that i can either install this stuff to my system (not using a venv at all), fuss with venvs much more than i want to, not use python (lol) (i could get away with this if i didn't use pygments, letting asciidoctor use rouge and not highlighting in pug at all; or just using highlight.js after all), or build myself
    - i could also use https://github.com/alecthomas/chroma instead of pygments, maybe -- go doesn't seem to have these kind of problems (haha, though it has different ones, like everything)
    - oooh, or i could use pipenv to install pdm (or poetry), use that to install everything else and provide my venv (or equivalent) -- maybe -- worth fussing with?

#### (2021-11-22)

- about python
  - i kind of want to solve this problem, but i don't think it's worth the time
  - so i'm gonna see if i can get away from depending on python -- shouldn't be too hard at the moment
- yay! ruby works as expected (now that i've got the version line sorted out, after working on it yesterday)
  - this includes calling things with `bundle exec ...`
  - and i wasn't doing anything complicated, so i was able to replace my call to `pygments` with a modified call to `rougify`
- asciidoctor implementation...
  - i have to keep a dependency outside node anyway, for syntax highlighting, so i think i'll keep ruby either way
  - i think it basically comes down to, do i want to write my templates in pug or slim?
  - mm, also -- i think most of the logic will be in the templates -- but if i was going to write more, i might want to do it in ruby -- alternately typescript, but i already have ruby working, and because i'm going to be calling it in svelte.config.js, i would have to figure out typescript compilation (and do it in a prebuild step) (which is actually more complicated than using ruby, which i can just pipe stuff through)
- working on templates
  - found a linter for slim :) -- can't find a formatter though
  - if there are multiple authors...
- switching to markdown
  - the `*` characters at the beginning of asciidoc lists were hurting my finger, and i didn't find an easy way to add editor support for automaticaly continuing a list, or adding a `*` on indenet, etc
  - now that i'm using markdown in vscode, i'm actually surprosed by how good the editor support is (at least, for lists, relative to asciidoc)
  - also, i was surprosed that prettier formatted my md files, including properly formatting some of the source in code blocks lol
  - still not sure whether it'd be better for blog posts... but it's better for what i'm doing in the readme and this notes file -- i was worried that markdown still didn't support code blocks in lists, but it does :) and very nicely -- and that's the most complicated things i'm doing here
- looking into markdown for posts
  - https://github.com/markdown-it/markdown-it
    - via extensions, it handles footnotes (inline too :) )
    - still can't decide whether i want to trust extensions to stick around super long (i feel confident commonmark will), but if i don't want to use them, i can try to script some things (like footnotes) in svelte/js

#### (2021-11-23)

- working on markdown -- how to do the header and title stuff?
  - might be nice to do it in the layout... so, using svelte to template (rather than pug, or asciidoc)
    - another benefit of doing it this way is that if i get the header stuff into the layout and put it in `<svelte:head>` there, i don't need to worry about making another `<svelte:head>` in the article if i want, because that's allowed (you just can't have two in the same file)
  - passing data from child to parent
    - https://codechips.me/svelte-cross-component-communication/
    - or maybe i can use a load function? https://kit.svelte.dev/docs#loading
      - maybe in a `[slug].json.js` file? https://kit.svelte.dev/docs#routing-advanced-rest-parameters
  - why are they called "slug"s anyway?
    - https://itnext.io/whats-a-slug-f7e74b6c23e0
  - i don't think you can make a svelte component from a string (yet) https://github.com/sveltejs/svelte/issues/2324
    - so if i want svelte syntax in my markdown, i'll have to render it via preprocessors, as i'm doing
    - if not, it might be convenient to render it via a json route, the way the svelte and kit blogs do
    - the only reason this is an issue is because i want to get the front matter / metadata from the file to render, preferably in the layout since i don't want to be templating in svelte.config.js, and preferably without parsing twice -- but i don't think i can have all those things
    - the main thing though is that i don't want to repeat code... might need to make a little typescript module to convert markdown (with markdown-it), and then compile it so it can also be used in svelte.config.js too
  - svelte blog code https://github.com/sveltejs/svelte/tree/master/site/src/routes/blog

#### (2021-11-24)

- trying out other things today
- for reference
  - svelte blog code https://github.com/sveltejs/svelte/tree/master/site/src/routes/blog
  - https://codeburst.io/https-chidume-nnamdi-com-npm-module-in-typescript-12b3b22f0724
- can i dynamic import?
  - yes (maybe) i can `let f = "../blog/test3.md"` and `let s = import(f)` -- but vite complains that it can't analyze it
    - ahh, but they give instructions https://github.com/rollup/plugins/tree/master/packages/dynamic-import-vars#limitations
    - and following them makes it not complain :) (and i should be able to follow them)
      ```js
      let f = "blog/test3";
      let s = import(`../${f}.md`);
      ```
  - https://svelte.dev/repl/16b375da9b39417dae837b5006799cb4?version=3.25.0
  - you have to `await` the import, so i tried doing it in a `load` function
  - works!
- can i import a template in `__layout.svetle` and put the slot content in there depending on the filename?
  - ahh... but the filename extension isn't visible in the svelte+html (which runs at least sometimes in the browser), since it's not part of the path -- and the file isn't accessible then either, so we can't look to see if it exists
  - but we can switch on the path :) and i'm sure we could find the filename somehow (during build time, and save it) and switch on that, with a little effort
- i think my favorite of the ways i've seen so far is dynamic import
- since i'll be doing markdown parsing in both `[slug].json.ts` and `svelte.config.js` (which can't directly import ts files), i'll need to set up a little ts module
  - https://codeburst.io/https-chidume-nnamdi-com-npm-module-in-typescript-12b3b22f0724

#### (2021-11-25)

- for reference
  - svelte blog code https://github.com/sveltejs/svelte/tree/master/site/src/routes/blog
  - https://codeburst.io/https-chidume-nnamdi-com-npm-module-in-typescript-12b3b22f0724
- js
  - `dirname` must not be available in the browser... because `path` is a node library
    - so how to get the directory? https://stackoverflow.com/questions/29496515/get-directory-from-a-file-path-or-url/29496594
    - https://stackoverflow.com/questions/3745515/what-is-the-difference-between-substr-and-substring
  - what's the type of a svelte component?
    - https://stackoverflow.com/questions/63551277/typing-sveltecomponents-this-property-in-typescript
    - https://github.com/sveltejs/language-tools/issues/486
  - build error
    - `Unknown variable dynamic import`
    - looks like someone trying to do the same thing i am? https://github.com/sveltejs/kit/issues/1326
    - https://issueexplorer.com/issue/vitejs/vite/4945
      - https://github.com/rollup/plugins/tree/master/packages/dynamic-import-vars#how-it-works
        - ooo -- "globs only go one level deep" -- and also, variables are turned into globs during build, so that whatever file it is will be available at runtime? -- not sure i like dynamic imports best anymore... -- though i suppose for my use case it doesn't matter (since i'll want everything in the directory full of posts) -- hmm... so i guess that behavior makes sense -- still, lol, glad i know about it -- and anyway, sounds like your variable can't have any `/`s in it -- that appears to work in dev, but will fail in build

#### (2021-11-26)

- for reference
  - svelte blog code https://github.com/sveltejs/svelte/tree/master/site/src/routes/blog
  - https://codeburst.io/https-chidume-nnamdi-com-npm-module-in-typescript-12b3b22f0724
- what gets built for the blog posts?
  - no html files, it seems
  - but i can still access them (in preview, locally) without javascript
  - if i include a link to the page in the blog index
    - then it generates an html :)
    - but i have to say `<a href='/blog/1'>test 1</a>` rather than `<a href='1'>test 1</a>` for some reason
      - https://stackoverflow.com/questions/69939125/sveltekit-relative-anchor-tag-href-based-on-the-folder
      - https://dev.to/brittneypostma/make-pathing-easier-with-aliases-in-sveltekit-37l0
        - not what i was looking for but interesting!
        - i was trying to get `$lib` to work in a scss style the other day, and it wasn't working -- but if i add `$lib` to the list of vite aliases, it works :)
        - but i'm actually only doing this in one place so far -- the other place i'm loading a style, it's the global one -- and other places (e.g. inside svelte.config.js) i have to use the full (relative) path anyway -- so i'm gonna leave it alone for now
        - still, i was wondering about this the other day, so it's cool :)
    - if i include the link via an `{#each}` it also works :)
      - though, for now the loop is over a constant declared in the same file
- thinking, rather than templating posts in svelte (in `[slug].svelte`), i could maybe template them in pug, and render during preprocessing
  - not sure if this would be better -- it would allow me to put the posts directly in `/blog` -- on the other hand, it's kinda nice having them in `/blog/_posts` -- on the other hand, even if i left them in a subdir, it might be nice to template in pug rather than svelte
- working on typescript module for markdown
  - https://yaml.org/spec/1.2.2/
  - yaml vs toml vs a yaml variation https://stackoverflow.com/a/65289463/2360353
  - seems to be the recommended js yaml parser https://github.com/nodeca/js-yaml
  - https://github.com/markdown-it/markdown-it
    - has a front matter plugin https://www.npmjs.com/package/markdown-it-front-matter
    - front matter in general seems to use `---` for both open and close -- which is interesting, since yaml uses `...` to end a document without starting a new one, so that would seem like a natural choice of ending
  - https://regex101.com
  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
  - https://github.com/sveltejs/svelte/blob/81fc3f898a49f10856db17014521556a2f18b9dc/site/scripts/get-examples-from-tutorials/index.js#L8
  - https://stackoverflow.com/questions/62096269/cant-run-my-node-js-typescript-project-typeerror-err-unknown-file-extension didn't seem to work
  - https://stackoverflow.com/a/66626333/2360353 this does :)
    - and this, if it has ts imports without `.ts` https://stackoverflow.com/questions/63807613/running-node-with-loader-ts-node-esm-js-requires-imports-to-have-the-js-extensi
  - typescript typing objects
    - can i have a type for an object that has a few maybe fields, and allows others too?
    - https://www.typescriptlang.org/docs/handbook/2/objects.html
    - https://stackoverflow.com/questions/42723922/can-you-declare-a-object-literal-type-that-allows-unknown-properties-in-typescri
    - https://github.com/microsoft/TypeScript/issues/7803
    - yes, but then you have to specify the key type, and i'd rather not for now -- also, i'm not sure it matters, since the receiving code needs to check if things exist anyway
  - markdown
    - what did gitlab do? https://about.gitlab.com/blog/2019/06/13/how-we-migrated-our-markdown-processing-to-commonmark/
    - they use [cmark-gfm](https://github.com/github/cmark-gfm) like github
    - different parsers https://css-tricks.com/choosing-right-markdown-parser/
    - looks like gitlab actually uses a few different markdown flavors https://about.gitlab.com/handbook/markdown-guide/
    - vscode uses markdown-it https://code.visualstudio.com/docs/languages/markdown#_does-vs-code-support-github-flavored-markdown
- using markdown-it along with 3rd party extensions though has me a little scared -- but it doesn't do everything i want out of the box -- should i consider using cmark-gfm?
  - trying to find documentation for the github flavored markdown extensions is kind of disconcerting -- they released a standard (with differences from commonmark highlighted) a few years ago, but then it seems they have added other things (like footnotes) and documented it... only on their blog? and on the page describing github markdown markup? -- which makes things about the same as they were before, where github flavored markdown is one of many valid markdowns lol
    - funnily, the kramdown people (or at least one, based on one comment) might feel this way -- that kramdown and commonmark are both markdown dialects -- and based on how everyone who implements a commonmark parser also extends it lol, i feel like that's pretty accurate
  - so my main question is, what will stick around and keep being used and maintained?
  - i feel like github flavored markdown should, and commonmark of course
  - maybe kramdown too, and also gitlab flavored markdown
    - kramdown is also default with jekyll? https://developer.rhino3d.com/guides/general/how-this-site-works/
  - markdown-it, since vscode uses it
  - q&a with kramdown author (few years old) https://www.reddit.com/r/ruby/comments/48c1sq/qa_with_thomas_leitner_author_of_the_kramdown/
- typescript markdown module is coming along
- reading more about gitlab, redcarpet, kramdown, and commonmark
  - https://docs.gitlab.com/ee/user/markdown.html
    - they use kramdown for their documentation and blog, and extended commonmark for issues, comments, etc
  - https://gitlab.com/gitlab-org/gitlab-foss/-/issues/43011
    - talking about migratng from redcarpet to commonmark
    - apparently a main benefit over kramdown is that github and others use it (so, compatibility)
  - https://gitlab.com/gitlab-org/gitlab-docs/-/merge_requests/108
    - talking about migrating from redcarpet to kramdown
    - apparently, it has features that their doc team needed, over e.g. extended commonmark

#### (2021-11-27)

- reference
  - https://github.com/markdown-it/markdown-it
    - https://markdown-it.github.io/markdown-it/
    - https://github.com/markdown-it/markdown-it/blob/master/docs/architecture.md
  - https://github.com/nodeca/js-yaml
  - https://spec.commonmark.org/0.30/
- typescript and javascript
  - https://stackoverflow.com/questions/40641370/generic-object-type-in-typescript
  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/switch
- markdown-it
  - anchor links https://nicolas-hoizey.com/articles/2021/02/25/accessible-anchor-links-with-markdown-it-and-eleventy/
    - https://amberwilson.co.uk/blog/are-your-anchor-links-accessible/

#### (2021-11-28)

- test
  - if you have `routes/blog/index.svelte`, `routes/blog/__layout.svelte` applies
  - if you have `routes/blog.svelte`, it doesn't apply
- typescript
  - question marks after parameters and types https://stackoverflow.com/a/60038363/236035

#### (2021-11-29)

- reading
  - https://kramdown.gettalong.org/documentation.html
  - https://kramdown.gettalong.org/rdoc/index.html
  - https://rouge-ruby.github.io/docs/Rouge/Formatters/HTML.html
  - https://github.com/gjtorikian/commonmarker
  - https://talk.commonmark.org/t/when-do-you-think-well-ship-1-0-of-the-commonmark-spec/2797/7
  - https://www.reddit.com/r/webdev/comments/8y09se/can_we_talk_about_markdown/
    - https://ericholscher.com/blog/2016/mar/15/dont-use-markdown-for-technical-docs/
  - https://docutils.sourceforge.io/rst.html
  - https://github.com/markdown-it/markdown-it
    - https://www.npmjs.com/package/markdown-it-attrs
  - https://github.com/inikulin/parse5
    - https://github.com/jsdom/jsdom
    - https://github.com/inikulin/parse5/blob/master/packages/parse5/docs/index.md

#### (2021-12-08)

- back from resting my fingers for a week or so :) -- still need to take it slow
- gonna try moving the repo to gitlab, since cf pages just added support for that recently
- so gotta push everything to the server first
- so gotta fix errors in my in-progress code
  - https://stackoverflow.com/questions/59729654/how-ignore-typescript-errors-with-ts-ignore
- got it, but for some reason cloudflare pages is queuing two buils per push, for both main and blog (my current dev branch)
  - deleting the project and trying again
    - maybe the fact that a build was in progress (taking way too long) when i deleted the first one messed things up...
    - or maybe it's coz an `origin/HEAD` seems to have appeared, when i migrated to gitlab?
      - tried to delete it, and it goes away locally, but when i clone again it shows up https://stackoverflow.com/questions/39205681/how-to-get-rid-of-remote-origin-head-pointing-to-origin-master
- build seems to fail the first time, but succeed the second, when making a new project with this gitlab repo
- testing push again
  - wow... now it queued 3 times -- which is the number of times i've created this project connected to the gitlab repo (so far)
  - i wonder if it will fix itself if i leave it alone for a day or so
  - hmm... also this build (actually deployment) failed -- so builds are failing for no reason (it says "internal error occurred", but... the code hasn't changed...)
- making a new cf pages project, and choosing a different name
  - if this build fails, i'm gonna try going back to github
- trying github again
  - and... it worked, first try
  - cloning the repo (during the cloudflare build) is also quicker
  - only queues one build
    - though, this may be unfair... but i don't want to delete and recreate it again just so i can see if it fails lol
  - hmm... but the push to `blog` failed to deploy, with the same "internal error"
    - this is after enabling cloudflare pages analytics
  - well... cloning the git repo in the cf build was faster the first time anyway -- this time it's taking super long...
- i'm gonna have to leave this alone, and reinvestigate later
  - maybe gitlab is the best thing to use -- since switching is apparently a pain, and i like the way they handle git-lfs better (per repo, rather than per account, and i think more free space too)
  - now i'm wondering again if i should just switch to building locally lol
    - i feel like, it'd be good to get a remote build working, since that's what one would want "in real life" (i.e. when working with multiple people)
    - but at the same time, if i was working with multiple people, i would want a solution that worked a bit better...
- cancelled that build, and retried
  - clone took 3s this time -- even though it was hanging before...
  - succeeded
- so yeah, not sure what to make of this
- here are some other people having the same issue, but without a fix https://community.cloudflare.com/t/cloudflare-pages-deployment-repeatedly-fails/263610/2
- here someone had a nonexistant "root" set https://community.cloudflare.com/t/pages-fails-to-build-internal-error-while-cloning-repository/319586
- and another that was wolved (maybe a cloudflare backend fix) https://community.cloudflare.com/t/cloudflare-pages-failed-due-to-an-internal-error/262234/23
- this one was because of a too long file name https://community.cloudflare.com/t/cloudflare-pages-failed-an-internal-error-occurred/273333/6
- mmm, just realized -- it'd be possible to switch without site downtime
  - would have to make a new project (with a different name)
  - and then change the CNAME record
  - now that i think about it, does it really matter that cloudflare pages knows that another domain is pointing at the one it assigns? not sure -- if it does, there might be some downtime after all, but if not, then not, just a transition period
  - and then can delete the old project afterwards
- done for today, i'll maybe think about switching to gitlab again later

#### (2021-12-09)

- for document format, might want to look at pandoc flavored markdown https://pandoc.org

#### (2021-12-11)

- pandoc
  - so, the reasons i'm considering pandoc are
    - it's been around a while, and seems like a pretty mature tool
    - it has lots of output formats (and input formats)
    - many of the several markdown-it extensions i was thining to use say their syntax is based off that of pandoc
    - markdown-it, for all its good qualities, scares me a bit in that i'll have to rely on 3rd party extensions for certain features
      - and because it's written in javascript, and as nice and convenient as that is, i feel like it requires more maintenance than some other languages (i.e. i feel like the node + js environment isn't as stable as e.g. haskell, in which pandoc is written, or ruby in which asciidoctor is written, etc)
  - one negative is that cloudflare pages doesn't support haskell, so i'll have to switch to building locally
    - but that would have benefits too, i think -- cf pages isn't my favorite build environment
    - or i could maybe build with gitlab, or something
- research
  - gitlab ci can be run locally https://stackoverflow.com/questions/32933174/use-gitlab-ci-to-run-tests-locally but it's a bit more complicated than i'd like
  - getting stuff into cloudflare pages without building it there (or checking it in to the repo) looks like kind of a pain
    - no way to upload directly https://community.cloudflare.com/t/deploying-a-static-site-via-existing-cicd-platform/331167
  - someone (lost the link) suggested to someone else to put a linux executable for the tool they needed in their repo, and execute it as part of their build step
    - pandoc looks kinda big though https://github.com/jgm/pandoc/releases/tag/2.16.2 -- and not sure if i can install .deb packages in a build step
  - i wonder how hard it would be to build 'releases', for either gitlab or github (locally or with github actions or gitlab ci), and then download the release in the cloudflare pages build step and use that to deploy
    - seems like the kind of thing that would be different between github and gitlab
    - i think i'll see about using pandoc first, and if i like it, i can look more into that
      - i can always check in the build artifacts, or push them to another repo, or to gcp (i suppose), or something too
      - either way, i don't really wanna be _tied_ to cf pages build env, even though it would be convenient if my build worked there
- pandoc
  - for things like
    - adding `target='_blank'` to anchors
    - html escaping `{` and `}`
    - (possibly) generating a table of contents
  - i could
    - generate the html, parse it (with parse5, or maybe i need jsdom, i forget), and then do those things in javascript
    - use a custom writer https://pandoc.org/custom-writers.html
    - use a filter https://pandoc.org/lua-filters.html
  - of these, filters look the simplist
  - it looks like for writers you have to specify everything, even if it's just modifying the default -- and i'd rather just override a little, if i can
  - and anything i can't do with filters, i guess i can do by operating on the dom, if i need to
    - hmm, though in this case, i suppose it'd be worth considering a custom writer

#### (2021-12-12)

- thinking about how to deploy
  - should i switch hosting providers?
    - netlify vs cloudflare pages https://zhauniarovich.com/post/2021/2021-07-comparing-netlify-and-cloudflare-pages/
    - i kinda wish i could host it on google cloud with cloudflare in front of it (for caching), but i think you have to give cloudflare control of the whole domain, not just a subdomain like i want to do
  - i could perhaps make a repo just for build artifacts lol, and point cloudflare pages at that
    - as part of my push git hook i could make sure that repo was on a branch of the same name and check in the build artifacts there
      - it'd be harder to manage, but it'd keep build artifacts out of my code repo
  - i could also check in generated html (really, generated svelte) files for the markdown, with a "this is generated" comment
    - this would have the advantage that i could monitor changes to the generated files, which could happen e.g. because of a change to pandoc config
    - this does feel a bit dangerous though, having generated files checked in
  - oh yeah, i could also look in to using "releases"
    - the idea being, on git push, create a release, appropriately named based on the branch, and then download that during cloudflare pages build
    - the negative here is that then cf pages is downloading the repo, and then the build files, and ignoring the repo except for a few scripts and version files
      - so it would be simpler to just have a different repo which cf pages is monitoring
      - it would also be less work, because i'd be using the same tools (git, git-lfs) for deploying as for source

#### (2021-12-13)

- cleaning up and merging with main
  - then planning to
    - move to gitlab
    - change deployments to use a separate repo
      - so that i can build locally and not depend on the cloudflare pages build environment
    - set up pandoc for blog posts
    - write blog posts :)
- how to deploy code to cloudflare pages if i'm building it locally?
  - since i want to use pandoc, i think
    - haha, at least, that's what i currently want to try using
  - i think i like the "separate repo for build artifacts" idea best, since it requires the least amount of fussing with cloudflare pages
  - so how to treat the build artifacts repo?
    - https://www.atlassian.com/git/tutorials/git-submodule
    - https://www.atlassian.com/git/tutorials/git-subtree
    - or i could just clone the repo alongside the source repo, and write a deploy script (which i'll have to do anyway i think) to copy things there
    - submodules and subtrees sound like they're for tracking external source, which you might need to pull changes from, and might possibly also want to push changes to (or make a merge request for)
      - which is the opposite of what i'm doing
      - what i'm doing is kinda silly anyway, essentially using a git repo as an object store, so it isn't surprising that it wouldn't be super common to do
    - having a repo manually cloned alongside seems like it makes most sense
      - submodules would also be fine i think, but the metadata they keep is completely unnecessary -- if anything, i would want it the other way around in this case, with the build repo tracking the source repo that the artifacts came from -- but i don't think that would be super easy, so it wouldn't make sense to do
      - subtrees i think would make the least sense, because the whole point of this is to keep the artifacts out of the source repo
    - i think the best way to think of the build repo is not as a repo at all, but rather as part of a little system, tied together with scripts, for deploying the site
      - rather than rsyncing to a server and being done (that would be nice...), i'll rsync to a directory, do some branching stuff (to make the branch of the build repo the same as the branch of the source repo), and then git commit and push -- or something like that

### cloudflare (2021-12-14)

- https://news.ycombinator.com/item?id=26778894
  - apparently it's possible to install applications as part of your build, since it's some type of ubuntu container?

#### (2021-12-15)

- moving to gitlab
- creating build repo
  - debated on making this public (since it may as well be hidden, it's just build artifacts), but i want to make everything about the site is easy for other new learners to see :)
- moving to ssh (from https) for git access
- working on deploy
  - find current git branch name https://stackoverflow.com/questions/1593051/how-to-programmatically-determine-the-current-checked-out-git-branch
- working on redirecting
  - research
    - https://news.ycombinator.com/item?id=26778894
      - look into using `<link rel="canonical">` so the `.pages.dev` site doesn't get indexed (e.g. by google) (since i only want my `www.` site to be indexed)
      - might want a canonical url on every page https://yoast.com/rel-canonical/#when-canonical
      - or might want to redirect via javascript https://developers.google.com/search/docs/advanced/guidelines/sneaky-redirects?hl=en&visit_id=637752253998118725-2381755612&rd=1
      - one suggestion for redirecting via js https://community.cloudflare.com/t/redirect-cloudflare-pages-domain-to-custom-domain/298210/2
      - another thread https://community.cloudflare.com/t/disable-page-dev-subdomain/272774/3
        - apparently you can restrict access to the `.pages.dev` subdomain if you want https://community.cloudflare.com/t/pages-redirect-or-remove-pages-dev-domain-to-custom-domain/271076/2
    - canonical links are not redirects https://www.searchenginepeople.com/blog/125-redirect-or-canonical.html
    - if i use js to redirect, better to use `window.location.replace` to avoid adding the `.pages.dev` to the navigation history (like `window.location.href` would) https://www.contentkingapp.com/academy/redirects/faq/javascript-redirect-bad-seo/
      - it mentions that using `...href` causes visitors to get stuck in back-button loops, which i hadn't thought of as a consequence, and feels scammy, so i want to avoid it
    - what does google say about redirects
      - javascript ones are okay, but server side or `meta ... refresh` redirects are better https://developers.google.com/search/docs/advanced/crawling/301-redirects#jslocation
      - but `meta ... refresh` tags are recommended against by the w3c apparently -- even though google supports them -- https://developers.google.com/search/docs/advanced/crawling/special-tags
    - so hmm... if 301 redirecting doesn't work, maybe i should do a js redirect, along with a canonical link? -- or a `meta ... refresh` link -- i'll have to try and think about it more
    - google redirects page https://developers.google.com/search/docs/advanced/crawling/301-redirects
  - tried adding `/* https://www.benblazak.dev/:splat 301` to `/_redirects`, but i can still navigate to both my custom and `.pages.dev` urls

#### (2021-12-16)

- working on git
  - new branch wasn't pushing
  - https://stackoverflow.com/questions/37770467/why-do-i-have-to-git-push-set-upstream-origin-branch
  - adding `-u` to the `push` fixed it
  - changing `push.default` might also work https://git-scm.com/docs/git-config
- adding cloudflare analytics
  - via js snippet
    - since using a CNAME record, it looks like i can't use their automatic setup https://developers.cloudflare.com/analytics/faq/web-analytics#can-i-use-automatic-setup-with-a-dns-only-domain-cname-setup
      - this doesn't quite make sense, because if it was just dns, why would there be errors on the custom domain? wouldn't it just not count those page views? hmm
      - but anyway there are errors on the custom domain (and not on the `.pages.dev` one) so i'm gonna try this
  - and removing the setting from cf pages
  - i'll have to push to main to test though
  - testing
    - were page views registered?
    - hmm, still not showing up -- i wonder how long it takes
    - i'll make a note and look later
    - ahh, in the built files it's coming out as `data-cf-beacon="[object Object]"` rather than what it should be -- oops -- because that's what happens if you use `{{this: 'that'}}` (e.g.) as an attribute
    - if i just put `{}` around it, it changes quotes to `&quot;`
    - if i use `JSON.stringify` it does the same thing
    - i didn't think this would work, but it shows up right in the browser, and i appear to be getting page views now :)
- working on redirects

  - https://developers.google.com/search/docs/advanced/crawling/301-redirects
  - trying `_redirects` file again
    - https://developers.cloudflare.com/pages/platform/redirects
    - `/ https://www.benblazak.dev`
    - nope
    - it looks like it should work though, from the example
    - `/blog/* https://www.benblazak.dev/blog/:splat`
    - this appears to work :)
    - except that, if i go to `https://www.benblazak.dev/blog` directly, it can't open, because of too many redirections... lol
    - nope
    - and it specifically says domain level redirects are not supported
  - interesting, `meta refresh` looks well supported https://caniuse.com/?search=meta%20refresh -- even if the w3c does dislike it lol
  - you can set `meta refresh` in the html, or in a the http header

    - and you can set http headers based on full url? https://developers.cloudflare.com/pages/platform/headers
    - trying
      ```
      https://:project.pages.dev/*
        Refresh: 0; url=https://www.benblazak.dev/:splat
      ```
    - works for `/blog` -- but not for `/`
    - trying

      ```
      https://:project.pages.dev
        Refresh: 0; url=https://www.benblazak.dev

      https://:project.pages.dev/*
        Refresh: 0; url=https://www.benblazak.dev/:splat
      ```

    - same

  - in the blog post https://blog.cloudflare.com/custom-headers-for-pages/ they say that they enhanced `_redirects` too -- so i wonder if domain level redirects would work after all
    - trying `https://:project.pages.dev/* https://www.benblazak.dev/:splat 301`
    - nope
  - more `_headers`

    - trying
      ```
      https://:project.pages.dev
        Refresh: 0; url=https://www.benblazak.dev
      ```
    - ah! this one redirects the root!
    - but does not redirect `/blog`...
    - what if i change the order

      ```
      https://:project.pages.dev/*
        Refresh: 0; url=https://www.benblazak.dev/:splat

      https://:project.pages.dev
        Refresh: 0; url=https://www.benblazak.dev
      ```

    - getting a `404: not found` with some stuff after it...
    - trying

      ```
      https://:project.pages.dev/*
        Refresh: 0; url=https://www.benblazak.dev/:splat

      https://:project.pages.dev/
        Refresh: 0; url=https://www.benblazak.dev/
      ```

    - same thing
    - from the docs "If a header is applied twice in the \_headers file, the values are joined with a comma separator" -- maybe that's what i'm seeing?
    - trying this again
      ```
      https://:project.pages.dev/*
        Refresh: 0; url=https://www.benblazak.dev/:splat
      ```
    - yay! for some reason it works now, or at least it seems to
    - one interesting thing is that in analytics, it gives `homepage-1.pages.dev` as the referer -- so i won't know where that page was being referred from lol, since it immediately redirects -- hmm...
      - maybe i should just set `canonical` after all -- and maybe redirect in javascript
      - on the other hand... doing it this way, google treats it as a 301 redirect, and most places should have a hard time bookmarking (or anything) the `.pages.dev` site, since it redirects immediately -- so maybe this is okay...
    - thinking more about this
      - using headers like this will work for people with js off
      - also doing it in js means the check has to be done on every page, client side, wheras this is done on the server
      - hmm, i could always leave it like this (which seems nicer) and then switch to `canonical`s and js redirects if it turns out i'm getting traffic from `.pages.dev` and want to know where that's coming from
        - i don't think using an html `meta refresh` would work in this case, because that would still probably be processed before js
        - even with js, i'd have to make sure the analytics were executed before the js redirect
      - in any case, i don't think it'll be a problem, and i can solve it if it is -- so in the mean time, this looks like the smallest solution for sure
      - ooh, one other benefit to using `canonical` and a client side refresh (html or js) would be if someone copied the site, but didn't change those -- then their serch indexing would point to my pages, and their users would be redirected, lol
        - though this would matter more for an app or something
        - and also, i haven't heard of others doing this -- not sure if they do or not, but haven't heard anyone talk about it
      - ooh, actually, i couldn't use a `meta refresh` in the html, unless i could wrap it in some js to check whether it was being served from the `.pages.dev` hostname or not -- but at that point i might as well use js?

  - summary
    - my two options are to use `refresh` in the header, or to use `canonical` links and redirect with javascript
      - using `refresh` in the header is much closer to a server side redirect (which apparently still isn't possible), and it's simple and less total code, so that's what i'll stick with for now
      - if i wanted to be clever (and was worried about people copying my site without changing anything) i could use `canonical` links and redirect with js
        - this would also be useful in the case that i ended up getting a lot of traffic to `.pages.dev` domain, and wanted to know where it was coming from (the current method doesn't leave time for the analytics to load before redirecting)

### about me (2021-12-17)

- moving external links (besides "page source") from header to index
  - can curve text along a path https://css-tricks.com/snippets/svg/curved-text-along-path/
  - can do it in css too, but a bit more complicated https://1stwebdesigner.com/set-text-on-a-circle-with-css/
  - possibly a better css way https://stackoverflow.com/questions/2840862/is-there-a-way-to-curve-arc-text-using-css3-canvas
  - svg
    - https://developer.mozilla.org/en-US/docs/Web/SVG/Element/circle
    - https://developer.mozilla.org/en-US/docs/Web/SVG/Element/textPath
    - but inserting the fontawesome icons in place of text (at least in the trivial way) doesn't work
  - css
    - https://developer.mozilla.org/en-US/docs/Web/CSS/transform-origin
  - but maybe i don't wanna curve the icons around my picture anyway (though, at some point i'd still like to figure out how)

#### (2021-12-18)

- working on external links on about me page
  - reference
    - https://css-tricks.com/snippets/css/complete-guide-grid/
    - https://developer.mozilla.org/en-US/docs/Web/CSS/grid
    - https://www.digitalocean.com/community/tutorials/css-align-justify
      - prefix
        - `justify` : horizontal (inline) axis
        - `align` : vertical (block) axis
      - suffix
        - `items` : items within grid box, set on container
        - `content` : grid tracks within container
        - `self` : item within grid box, set on item
  - debugging
    - safari can show grid lines, but apparently i don't have that feature (yet) https://webkit.org/blog/11588/introducing-css-grid-inspector/
    - chrome can show them though :)
  - notes
    - not sure if this is right, but it looks like you can
      - set `grid-auto-flow: row;` and then use `grid-template-columns: ...` to control the number of colums
      - set `grid-auto-flow: column;` and then use `grid-template-rows: ...` to control the number of rows
    - `grid: repeat(2, auto) / auto-flow;` seems equivalent to
      - `grid-auto-flow: column;` and `grid-template-rows: repeat(2, auto);`
    - so it looks like
      - `grid: auto-flow / auto` is one column, infinite rows
      - `grid: auto / auto-flow` is one row, infinite columns

#### (2021-12-19)

- decided to leave my "about me" in an `article` tag, rather than a `section`, or a `section` inside an `article`?
- checking in and deploying :)
- deleted github repo and associated pages project
- checked, and i can still navigate to preview deployments on cloudflare, even with the header redirecting to my custom domain -- i think since preview deployments are a sub-sub domain of `.pages.dev`

#### (2021-12-20)

- ios not refreshing
  - both in safari, even on close and reload (of safari and the page) and in the one that's "saved to home screen"
  - is refreshing properly on macos
  - research
    - https://stackoverflow.com/questions/55581719/reactjs-pwa-not-updating-on-ios
    - https://github.com/sveltejs/sapper/issues/1681
    - https://stackoverflow.com/questions/51435349/my-pwa-web-app-keep-showing-old-version-after-a-new-release-on-safari-but-works
    - https://forums.expo.dev/t/update-ios-pwa-after-installed-to-homescreen-use-the-updates-api/36013
    - https://brainhub.eu/library/pwa-on-ios/
    - maybe https://crystalminds.medium.com/how-to-present-an-update-available-banner-for-a-progressive-web-app-pwa-95a3f6604c43
  - so, it looks like there are ways to hack around it
  - but, it never made much sense for my site to be installable as a web app anyway :) it was just for fun and research
  - so, similar to how i decided to postpone ios splash screens, i think i'll just take out the pwa functionality
    - ios might (lol) get fixed by the time i want to do this for real
    - and in the mean time, i think that's a better solution than implementing hacks (which will be a pain to test) and keeping track of when this is no longer needed
  - i think i'll leave the web manifest file, as well as the `meta theme-color` tags in `__layout.svelte`
    - this way it'll still look nice when adding to my home screen
      - some of the web manifest tags are partially supported, and the `meta theme-color` tags are i think ios specific
    - and i don't wanna test what i can take out
    - i also don't think i'll test it on android just now
      - i don't think i did initially either

#### (2021-12-22)

- working on license
  - https://github.com/github/choosealicense.com#license
  - https://opensource.stackexchange.com/questions/10201/is-it-common-to-licence-blog-posts-under-creative-commons
  - https://scotthelme.co.uk/why-my-blog-is-creative-commons-licensed/
    - i was thinking everything i wanted to allow was already allowed via fair use, but this has me thinking that releasing under a cc-by would be nice
  - https://gitlab.com/Nowaker/nowaker-blog/blob/master/LICENSE.md
  - https://wiki.creativecommons.org/wiki/Modifying_the_CC_licenses
    - also https://creativecommons.org/faq/
  - apparently having a disclaimer and such could be important https://sweetandsimplelife.com/why-every-blog-needs-a-privacy-policy-disclaimer-and-terms-conditions/
  - https://creativecommons.org/about/cclicenses/

#### (2021-12-23)

- working on license
  - using a cc license
    - marking your work https://wiki.creativecommons.org/wiki/Marking_your_work_with_a_CC_license
    - attribution https://wiki.creativecommons.org/wiki/Best_practices_for_attribution
- went over readme
  - probably want to go over it again in the future

#### (2021-12-24)

- playing with things for fun :)
- merry christmas eve!
- tried makinng my splash screen (with background color, and big centered butterfly)
  - ran into some css things that i'd like to figure out some day
  - also ran into an interesting issue (?) where some (?) global styles don't get reset when you make a `__layout.reset.svelte` -- at least, not the way i was going it
  - decided that really should be part of a separat branch and such, so i'll work on it later, along with test files for markdown styling, etc.
  - planning to put all this in `/lib`, which i won't link to anywhere, but it can be deployed

#### (2021-12-26)

- looking at image loading stuff
  - lazy loading https://css-tricks.com/lazy-loading-images-in-svelte/
  - resizing in js https://www.npmjs.com/package/jimp
  - a library that already does what i want, mostly? https://www.npmjs.com/package/svelte-image
    - github https://github.com/matyunya/svelte-image
    - uses "sharp" https://github.com/lovell/sharp
    - about `srcset` https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images
  - really interesting tutorial + library https://rodneylab.com/sveltekit-image-plugin/
    - uses sharp, and some vite plugins
    - https://www.npmjs.com/package/vite-imagetools
      - uses sharp
    - https://www.npmjs.com/package/vanilla-lazyload
      - native lazy loading is partly here https://caniuse.com/loading-lazy-attr

#### (2021-12-27)

- tracking files with lfs
  - https://git-lfs.github.com
  - https://rehansaeed.com/gitattributes-best-practices/
  - git untrack without removing https://stackoverflow.com/questions/1143796/remove-a-file-from-a-git-repository-without-deleting-it-from-the-local-filesyste
  - migrating
    - https://stackoverflow.com/questions/51782043/what-does-git-lfs-migrate-do
    - tutorial https://github.com/git-lfs/git-lfs/wiki/Tutorial#migrating-existing-repository-data-to-lfs
    - migrate multiple filetypes https://github.com/git-lfs/git-lfs/issues/2593
    - gitlab enable force push https://docs.gitlab.com/ee/user/project/protected_branches.html
  - set up git-lfs for the build repo as well
  - made sure to re-disable force push to both repos
- installed vite-imagetools

#### (2021-12-29)

- started a new 'about-me' branch today
- working on serving gravatar image directly
  - starting references
    - tutorial https://rodneylab.com/sveltekit-image-plugin/
      - https://github.com/rodneylab/sveltekit-components/blob/main/src/lib/components/Image.svelte
    - vite library that might do most of what i'd like https://www.npmjs.com/package/vite-imagetools
  - https://stackoverflow.com/questions/56988717/how-to-target-a-component-in-svelte-with-css
  - https://stackoverflow.com/questions/55957386/use-reserved-word-as-prop-name
    - this works, but doesn't appear to do what i wanted (make it so i could declare 'class' as an attribute on a svelte component), according to both vite and vs code
  - was trying to write this as just a svelte component -- foiled again by the fact that vite can't analyze dynamic imports (even if they're known at svelte compile time)
    - oh, oops -- can write dynamic stuff in a `context='module'` just as easily as in a `.json.ts` (or something) route
  - deleting stuff
    - planning to look into using sharp directly
    - if that doesn't work, i probably want to try using vite-imagetools directly, without a component
    - but if i go that route and it gets to be lot of typing, i can make a component of course
- research
  - another js source highlighter https://prismjs.com
    - did i see this one before? for some reason i thought highlightjs was basically the only one
  - vite plugin for cloudflare workers https://github.com/Aslemammad/vite-plugin-cloudflare
    - uses an alternative to "wrangler dev" written in typescript https://miniflare.dev to run workers in a local sandbox

#### (2021-12-31)

- happy new years eve :)
- starting to try out sharp, just a little, for fun
  - tutorial https://rodneylab.com/sveltekit-image-plugin/
    - but it looks like all they do with sharp directly is generate the string encoded placeholder image
  - https://sharp.pixelplumbing.com/api-resize
  - using with ts
    - https://github.com/lovell/sharp/issues/1029
    - but i don't see this in the docs now? but i don't see any ts in the docs
    - tutorial just uses `import sharp from 'sharp';`
  - https://github.com/JonasKruckenberg/imagetools
    - https://github.com/JonasKruckenberg/imagetools/tree/main/packages/vite
      - didn't read carefully, but it looks like this https://github.com/JonasKruckenberg/imagetools/blob/main/packages/vite/src/index.ts is a toplevel file that handles vite caching and url parsing (for import directives), and then interefaces with the lower level core lib
  - can't `await` in a synchronous function https://stackoverflow.com/a/47227878/2360353
    - have to use `{#await} ... {/await}` block in svelte
  - the `src` that i pass doesn't work if i use `$lib`
    - have to say `src='src/lib/assets/me.jpg'`
  - getting an error in sharp
    - might be due to the sharp version -- this is mentioned in the [tutorial](https://rodneylab.com/sveltekit-image-plugin/) -- but i don't think vite-imagetools is installing the same version for me as it was when the tutorial was written
    - if that's the case though, vite-imagetools should also fail? hmm -- but i haven't tested it
    - hmm, vite-imagetools works, and thus so does sharp
    - even leaving my current `Image` imported in `index.svelte` makes the page err
    - hmm, `import sharp from "sharp";` on it's own makes the error
    - mmmmm -- maybe it's because sharp requires node, and this is also trying to import in the browser
    - you can only import and export from toplevel... and even `context='module'` stuff is run in the browser?
      - not sure, but appears that way
    - if so, i think i'd have to put this code in a route
      - maybe that's why they did it that way in the tutorial lol
  - how someone else is using the vite plugin https://www.reddit.com/r/sveltejs/comments/mdd9ov/using_viteimagetools_with_sveltekit/
    - maybe i don't need to go through the effort of using `<picture>` unless i want backwards compatibility? i'll have to look at this more later -- if not though, that makes it pretty simple to use
  - `img` vs `picture` tags
    - https://www.reddit.com/r/web_design/comments/877gpx/img_vs_picture/
    - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture
    - so, use `img` with `srcset` if you wanna serve different sizes of the same image, and `picture` if you wanna serve different images for different screen sizes, or allow the browser to choose between image formats
    - https://caniuse.com/webp
      - safari only has partial support for webp on macos
  - summary
    - if i want to write it myself, i probably need to make a route, perhaps under `routes/api/` (saw that somewhere)
    - might be able to use vite-imagetools directly without way too much trouble, if i don't care about backwards compatibility
      - which i don't too much, but a little
      - will have to test
    - vite-image tools `?...` urls in imports are causing a ts error underline in vscode, and i'm not sure why or how to fix it yet (things work correctly otherwise)
    - oh -- or maybe if i wanna use vite-imagetools, but it ends up having lots of repition, i can use a pug template
      - someone else wrote a preprocessor for `img` tags

#### (2022-01-01)

- happy new year!
- thinking
  - probably can't use pug, since how would i get the data (src, width, height) to the pug svelte component?
    - if i pass the props in the svelte way, they're not in the component till svelte compilation, and they have to be received in a `script` tag, in js
    - with a lot of acrobatics, this could probably be overcome, because pug is transformed before svelte compilation, but i don't thik it'd be worth it
- research
  - can use vite dynamic import, but subject to the same restrictions as always https://github.com/JonasKruckenberg/imagetools/issues/5
  - in addition to a proprocessor solution, guess it'd be possible to run a prebuild script https://rodneylab.com/sveltekit-dynamic-image-import/
  - vite-imagetools docs https://github.com/JonasKruckenberg/imagetools/tree/main/docs
  - mdn responsive images https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images
  - mdn pwa progressing loading https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Loading
    - to load placeholder images first, they use `data-src` attribute on `img` and then do stuff in js on load
    - the rodneylab tutorial looks like it does about the same thing, just using `vanilla-lazyload` plugin
  - mdn img https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img
  - ooo, can write custome transformations for vite-imagetools :) https://github.com/JonasKruckenberg/imagetools/blob/main/docs/guide/extending.md
- so far
  - use `picture` for "art direction" (different images for different screen sizes)
  - use `img` for images, with `srcset` attribute
  - loading a placeholder first is a javascript thing
  - can extend vite-imagetools to cover common cases easily :)
    - https://github.com/JonasKruckenberg/imagetools/blob/main/docs/guide/extending.md
- using srcset
  - it's working in chrome :)
  - but using multiple formats, with webp first, makes safari try (and fail) to load the webp image, without skipping down to the png one -- and putting png first makes chrome grab that one
    - https://stackoverflow.com/questions/67515110/can-i-use-different-file-types-in-srcset
- reading
  - https://css-tricks.com/a-guide-to-the-responsive-images-syntax-in-html/
    - `picture` tags are also best for different formats
- thinking
  - the webp image is about 10% the size of the png one...
  - but if i need to use a `picture` tag, i don't want to write all this outmyself...
  - so my options are
    - just use jpg, png, or something common -- wait a few years maybe, and switch then
    - make a component about it (as complicated as that would be to get the information moved around before vite deals with it)
      - or put all the images in one directory (possibly with long filenames) and use dynamic imports -- but i'm not sure if that would work in this case
  - maybe i can use this to make some quick preprocessor macros? :) https://github.com/sveltejs/svelte-preprocess/blob/main/docs/preprocessing.md#replace
    - and i could put it in an `Image` or `Picture` component if i want to
      - but then... it might be helpful to keep the macros out in the open, in case i want to do any art direction of images in the future
- current active references
  - mdn
    - img https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img
    - picture https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture
      - source https://developer.mozilla.org/en-US/docs/Web/HTML/Element/source
  - css-tricks
    - responsive images https://css-tricks.com/a-guide-to-the-responsive-images-syntax-in-html/
  - vite-imagetools
    - docs https://github.com/JonasKruckenberg/imagetools/tree/main/docs
  - svelte-preprocess
    - replace https://github.com/sveltejs/svelte-preprocess/blob/main/docs/preprocessing.md#replace

#### (2022-01-02)

- working on images
  - format https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_typess
  - if you put `loading='lazy'` in the `img` tag, it should specify laziness even if a different source is chosen, inside a `picture` tag https://stackoverflow.com/questions/24025464/lazy-loading-html5-picture-tag
- summary
  - wrote it out without a component or anything, using vite-imagetools
  - still thinking about how (and whether) to package it up and remove the repition

#### (2022-01-03)

- working on images
  - dynamic imports -> nope
    - and... looking at the vite limitations again, "imports must end with a file extension"
      - https://github.com/JonasKruckenberg/imagetools/issues/5
      - https://github.com/rollup/plugins/tree/master/packages/dynamic-import-vars#limitations
    - so dynamic imports won't work, even if i keep (a symlink to) `Images.svelte` in each image directory, and inside it use `./` to start the path
    - can't `delete` certain things in ts (js strict mode) https://stackoverflow.com/questions/16652589/why-is-delete-not-allowed-in-javascript5-strict-mode
  - vite-imagetools extension -> would work i think :) still trying
    - example https://github.com/JonasKruckenberg/imagetools/issues/86#issuecomment-878944821
    - i think it's really an output format that i want https://github.com/JonasKruckenberg/imagetools/blob/main/packages/core/src/output-formats.ts
      - though maybe in combination with a default (really, easily accessible composite) transformation
  - svelte-preprocess replace -> unknown (didn't try)
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function
- general
  - js loops https://thecodebarbarian.com/for-vs-for-each-vs-for-in-vs-for-of-in-javascript.html
  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/instanceof
  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/fromEntries
  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push
  - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/at
    - also, how to get last element of array
  - js object and map key order https://stackoverflow.com/questions/5525795/does-javascript-guarantee-object-property-order
  - svelte iterate over object https://stackoverflow.com/questions/64909382/how-to-print-both-object-key-and-value-with-each-block-in-svelte
    - apparently this is the official way https://github.com/sveltejs/svelte/issues/894#issuecomment-385262773 or at least it was
  - https://stackoverflow.com/questions/58342203/typescript-declare-variable-with-same-type-as-another

#### (2022-01-04)

- working on images
  - you don't set elements on js Map objects the way you do on regular objects
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
    - wow that was hard to figure out lol
    - and this fixed some type inference "problems" -- that weren't really problems, now that i know what's going on, but they were really confusing before
  - trying to fix the typescript error on import
    - https://github.com/JonasKruckenberg/imagetools/issues/86
    - https://www.typescriptlang.org/docs/handbook/declaration-files/introduction.html
    - ahh https://github.com/JonasKruckenberg/imagetools/issues/160
      - multiple `*` not allowed in module declarations
      - but presets might help
      - in the end though, exactly what i'm wanting can't be done, so i'll have to make do with one of the workarounds
  - testing
    - widths weren't working quite like i expected
    - reading more about `sizes`, `width`, `srcset`, ... https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img
    - `srcset` and `sizes` have been supported for quite some time by the major browsers

#### (2022-01-05)

- other
  - just learned today that there's a PR for an RFC for changing the svelte preprocessor api https://github.com/sveltejs/rfcs/pull/56
- working on images
  - testing
    - research
      - svelte faq https://svelte.dev/faq#how-do-i-test-svelte-apps
        - svelte testing library https://testing-library.com/docs/svelte-testing-library/example/ example uses jest
        - "example using uvu test runner with jsdom" https://github.com/lukeed/uvu/tree/master/examples/svelte
      - great write up https://dev.to/d_ir/introduction-4cep
        - he uses [jasmine](https://jasmine.github.io) rather than jest, because it's simpler -- also tried out mocha
        - i think he also uses a package he wrote
        - references to rollup, so it might be out of date relative to what's in the svelte faq (but haven't checked)
      - someone using svelte-testing-library with jest https://timdeschryver.dev/blog/how-to-test-svelte-components
        someone else too https://blog.logrocket.com/build-your-own-component-library-svelte/
      - jest vs mocha vs jasmine
        - https://www.lambdatest.com/blog/jest-vs-mocha-vs-jasmine/
          - jest is from facebook
            - sounds like it includes everything needed
            - also used by airbnb, the nyt, twitter
          - mocha requires separate mocking
          - jasmine requires a runner
            - used by gitlab
        - https://dev.to/heroku/comparing-the-top-3-javascript-testing-frameworks-2cco
          - mocha and jasmine better at back end
          - jasmine is used more often with angular
          - jest was created to use with react
          - all three are good
        - https://www.ponicode.com/blog/which-test-framework-should-i-use
          - jasmine is 11 years old (!)
            - and it has a test runner?
          - jest is inspired by jasmine
      - it sounds like jes is mostly compatible with jasmine or mocha https://jestjs.io/docs/migration-guide
      - thoughts
        - i'm not planning to write much that will require testing, so this is largely just a "dip my toes in" experiment -- as much as i want to "get everything right" lol
        - especially, i'm not planning to have much backend going on in this project -- maybe in a different project, or in the future
        - i don't think i can evaluate between them very well without trying one
        - and i'm not really sure how to even set this up right now, so specific tutorials would probably be quite helpful
      - summary
        - as much as i want to like jasmine (or maybe mocha) (haha, and i seem to be biased against facebook), all the tutorials (except that one that was a little old) use jest
        - so i should probably try jest out first
      - thoughts
        - or really, maybe i don't need to test this... or anything yet
          - Image.svelte is really small, now that i've deleted just a few things
          - most of my current testing is figuring out what the browser wants (and... rereading the docs becasue i invariably miss things)
          - Header.svelte is much more complicated, but difficult to test because the complicated part is visual, and the next most difficult question is "does it work without javascript?"
  - documenting
    - https://svelte.dev/faq#how-do-i-document-my-components
    - https://typedoc.org/guides/doccomments/
    - https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html
    - looks like `///` comments don't work for documentation https://www.typescriptlang.org/docs/handbook/triple-slash-directives.html so we only have `/** ... */` comments
    - it seems that if you don't have any space on the left of your block `<!-- -->` comment, lists don't work correctly. e.g.
      ```html
      <!--
      - this
        - that
      -->
      ```
      renders the same as
      ```html
      <!--
      - this
      - that
      -->
      ```
      at least in vscode, but if you indent it
      ```html
      <!--
        - this
          - that
      -->
      ```
      it does what you'd expect

#### (2022-01-08)

- working on image component
  - wrote in readme
  - checking in
- serve gravatar image directly

  - extending vite-imagetools

    - docs https://github.com/JonasKruckenberg/imagetools/blob/main/docs/guide/extending.md
    - example https://github.com/JonasKruckenberg/imagetools/issues/86#issuecomment-878944821
    - vite plugin options https://github.com/JonasKruckenberg/imagetools/blob/main/packages/vite/src/types.ts
    - default resolve configs https://github.com/JonasKruckenberg/imagetools/blob/main/packages/core/src/lib/resolve-configs.ts#L17
    - entry point for vite imports https://github.com/JonasKruckenberg/imagetools/blob/c231165033105fad9e9af1c0965af081791ce8b0/packages/vite/src/index.ts
      - it looks like output format is handled separately from the rest of the config -- but that the rest of the config isn't passed to the function that can override that -- so that `&meta` must be specified separately
      - testing, this seems to be true
    - someone else doing essentially the same thing i did with Image.svelte https://github.com/JonasKruckenberg/imagetools/issues/260
    - can't use `===` to test `NaN` https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/isNaN
    - the following config allows me to type

      ```typescript
      import me from "$lib/assets/me.jpg?preset=224&meta";
      ```

      instead of

      ```typescript
      import me from "$lib/assets/me.jpg?w=224;448&format=webp;png&meta";
      ```

      ***

      ```javascript
      vite: defineConfig({
        plugins: [
          imagetools({
            extendTransforms: (builtins) => [
              (config, ctx) => {
                console.log(config); // debug logging
              },
              ...builtins,
            ],
            resolveConfigs: (config) => {
              // lets us write `?preset=100&meta`
              // instead of `?width=100;200&format=webp;png&meta`
              const preset = config.find(([key]) => key === "preset");
              if (!preset) return; // default resolve configs

              const [, value] = preset;
              if (value.length !== 1)
                throw new Error("invalid preset: expecting 1 value");
              if (isNaN(value[0]))
                throw new Error("invalid preset: expecting integer");

              const widths = [1, 2].map((e) => e * value[0]);
              const formats = ["webp", "png"];
              const out = widths
                .map((w) => formats.map((f) => ({ width: w, format: f })))
                .flat();
              console.log(out); // debug logging
              return out;
            },
          }),
        ],
      }),
      ```

      - the `extendTransforms:` only helps for debug logging

    - typescript warning for import
      - i can silence the typescript warning by using
        ```typescript
        import me from "$lib/assets/me.jpg?preset=224&meta&imagetools";
        ```
        and in `globals.d.ts` including
        ```typescript
        declare module "*&imagetools" {
          const out: any;
          export default out;
        }
        ```
        which is kind of silly, but... i think it might be the cleanest solution for now lol -- at least, it's the cleanest solution i've thought of so far
      - other ways
        - trying to use `@ts-ignore`, but eslint is unhappy
        - can disable that unhappiness https://stackoverflow.com/questions/59729654/how-ignore-typescript-errors-with-ts-ignore/59729735
        - or can `allow-with-description`
        - but `@ts-expect-error` is already allowed with a description https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/ban-ts-comment.md
        - vscode seems to be unhappy with this idea, but eslint seems to be okay with it

#### (2022-01-09)

- extending vite-imagetools
  - looking at typescript wildcards
    - https://github.com/microsoft/TypeScript/issues/38638
    - docs https://www.typescriptlang.org/docs/handbook/modules.html#wildcard-module-declarations
  - other
    - https://stackoverflow.com/questions/51439843/unknown-vs-any
  - might be able to avoid the last `&meta`? https://github.com/JonasKruckenberg/imagetools/issues/160
    - but he does mention that one of the goals was for the output type to be immediately apparently from the query string
      - so maybe i should keep `&meta` -- and add type information?
    - gonna try it
    - https://developer.mozilla.org/en-US/docs/Web/API/URL
    - https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams
    - tried
      ```javascript
      vite: defineConfig({
        plugins: [
          imagetools({
            defaultDirectives: (url) => {
              if (!url.searchParams.has("preset")) return;
              const value = url.searchParams.get("preset");
              if (!value) return;
              if (isNaN(value)) return;
              return new URLSearchParams(
                `width=${value};${value * 2}&formats=webp;png&meta`
              );
            },
          }),
        ],
      }),
      ```
      and even with that it seems that the `&meta` is required in the query string when it's imported (in `index.svelte`)
    - it does work though, other than that
    - might me a cleaner way to write what i currently have in `resolveConfigs:` -- or at least shorter -- but i would have to reconsider the error checks, so i'm not sure it would end up being much cleaner
    - for now i'll leave it as is (but glad i tried)
- deployed new site version :)
  - appears to have worked!
  - interestingly, the paths to the image versions are shorter in the built version
    - in dev, it looks like all image paths are just a hash (no prefix, no extension)
    - in production, the paths look like `.../me-2bc09a6d.png`
- oops... missed an error
  - when i go to blog, then back to home, i get `Unhandled Promise Rejection: TypeError: [...sources.keys()].at is not a function. (In '[...sources.keys()].at(-1)', '[...sources.keys()].at' is undefined)`
  - lol... `Array.prototype.at` is not supported on ios, and it's in TP on macos https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/at
  - https://javascript.plainenglish.io/array-at-will-change-how-we-access-array-values-in-javascript-517c3a13d505
  - guess it's newer than i thought
  - switched to `let image = sources.get([...sources.keys()].slice(-1)[0])[0];` -- works now :)

#### (2022-01-10)

- updating `Image.svelte` doc
- commented on
  - about using with typescript https://github.com/JonasKruckenberg/imagetools/issues/160
  - about easier creation of picture tags https://github.com/JonasKruckenberg/imagetools/issues/260
- svelte files aren't highlighted by gitlab
  - github seems to support them
  - https://gitlab.com/gitlab-org/gitlab/-/issues/195631
  - trying the `.gitattributes` workaround
  - not perfect, but better than nothing :)

### cleanup (2022-01-10)

- bug report https://github.com/sveltejs/svelte-preprocess/issues/459
- scoping query selectors https://stackoverflow.com/questions/61598941/how-to-scope-queryselector-to-component-in-svelte
- noticed that the external link icons under my picture in index are on top of Header when it gets pulled down
  - hmm.. if i put them in a `div` rather than an `aside` the problem goes away
  - nevermind, it's something to do with the css
  - it's the opacity
  - if i set `z-index: -1;` that fixes it
  - interesting -- opacity and stacking contexts interact -- so complicated https://stackoverflow.com/questions/2837057/what-has-bigger-priority-opacity-or-z-index-in-browsers
  - instead of doing that, i decided to work on my use of the solarized color scheme -- there is a comment color for deemphasized text (that actually deemphasizes a little more than opacity 75%) -- now using that :)
- researching layout reset issue
  - https://github.com/sveltejs/kit/issues/2154
    - the root `__layout` and `__error` are loaded as soon as possible, in case of an error -- thus, if the root `__layout` has side effects, they happen even if there has been a layout reset
  - i wonder if global styles count as side effects
    - thinking about what it means to have a global style that's always loaded (even on layout reset), i might want to find another solution to loading those styles
  - https://giters.com/sveltejs/kit/issues/1100
    - https://github.com/sveltejs/kit/pull/1356#issuecomment-833572267
      - talking about this behavior more
      - using `<link>` to load is not recommended, because of FOUC (flash of unstyled content)
      - links back up to https://github.com/sveltejs/kit/issues/2154 among others
  - i wonder if i can load the google font with the other style sheets too (if i end up doing it via js? with vite?)

#### (2022-01-11)

- global styles / global css
  - research
    - svelte docs https://svelte.dev/docs
      - interesting stuff, but nothing about global styles
    - sveltekit docs https://kit.svelte.dev/docs
      - not much about styles at all
    - you can use `:global { ... }` in scss https://dev.to/natclark/writing-global-styles-in-svelte-7he
    - https://github.com/sveltejs/kit/issues/1948
      - https://github.com/sveltejs/kit/issues/3127
      - https://github.com/sveltejs/kit/issues/714
        - i think this was the most useful of the pages i saw before
        - changes in `<link ...>`ed stylesheets aren't recognized by vite, and so won't be updated during dev -- but can import `src/global.css` in `src/routes/index.js` https://github.com/sveltejs/kit/issues/714#issuecomment-808758450
        - merged PR https://github.com/sveltejs/kit/pull/726
          - puts global css in `src/app.css` and in `src/routes/$layout.svelte` does an `import '../app.css';` -- also should work with e.g. scss
            - `$layout` was changed to `__layout` at some point https://github.com/sveltejs/kit/issues/1149
    - for what i tried previously, search 'without js' under 'nav (2021-10-23)'
      - not sure if i tried links
      - `import` didn't work when js was off
- trying
  - what i am currently doing is using `<style global> @import ...`
    - the problem with this is that these styles don't get reset by `__layout.reset.svelte` (see above)
    - another problem is that i can never remember whether sass is text-including them, or loading them later, etc.
  - using `<link rel="preload stylesheet" href='src/app.scss' as="style" />` seems to work :)
    - i do have to `@import 'src/lib/styles/vars';` inside it
    - but auto reloading by vite seems to work (though i may have had to manually refresh the first time)
    - works without javascript
    - but i am seeing FOUC -- though i'm not sure if that's vite generating stuff on the fly in dev, or whether it's because it's a link
    - oh interesting -- DOESN'T WORK IN PRODUCTION with scss
      - in dev i can link to `src/app.scss`, but when i build i get a 404
      - oh well
  - trying `include` in ts
    - major FOUC (though it might go away in prod, dunno)
    - doesn't load at all with js disabled (tested in safari)
      - oh!! -- this is only true in dev -- in preview (after building) it works without js :)
      - that's a bit annoying, for testing, but it does mean things work -- and might work better this way than other ways, net
- notes
  - the example project (if you `npm init svelte@next` ...) does global styles with `import` in the `<script>` -- the regular `<script>`, not the module one
  - can't `import "https://..."` -- so it looks like google fonts have to be in a `<link ...>`
    - link recommended here https://stackoverflow.com/questions/59605327/how-do-you-load-and-use-a-custom-font-in-svelte
    - another way (component) https://github.com/svelte-web-fonts/google
    - i remember reading a while ago that it's best to keep them imported instead of serving them directly because 1) users might have them cached, they're pretty common, and 2) google optimizes what it sends, based on the client
    - i was mostly just curious if i could import them with everything else -- i think i'm pretty happy with my link -- though i do get a bit of FOUC for the font
  - just found out `@use` works in sass (just `not how i thought) -- even though the docs say it's only supported in dart sass, and i didn't think i was using that version -- but yay :)
    - for future reference, with a namespace of `vars` for the vars import in sass, instead of `--color-accent: #{$sol-blue};`, you would say `--color-accent: #{vars.$sol-blue};`

#### (2022-01-12)

- global styles
  - nope, loading them with js doesn't make `__layout.reset.svelte` work
    - the styles are still applied
    - the template code is reset though (e.g. i can remove the header)
    - interesting
      - if i put the styles back in a `<style global ...>`, there isn't a FOUC on the main pages, but there is on the page under the layout reset
  - hmm, i could
    ```css
    <style global>
      * {
        all: revert !important;
      }
    </style>
    ```
    but that's hardly what i'm actually trying to do lol
  - apparently this whole `__layout` issue is still being thought out https://github.com/sveltejs/kit/issues/2169 -- i feel like the current behavior will change at some point
    - i guess this is why sveltekit is still in beta -- a lot of it feels good to go, but there are still areas like this where it's like... yup, that needs to be fixed, and it could easily be a breaking change
  - someone mentioned that `<link>` stylesheets get reset properly -- this makes sense to me, since resetting removes my Header (so, content is getting reset) -- but then i would have to compile the sass myself
  - i could also include the global css as scoped css in every top level component... lol
  - if i put
    ```scss
    <style lang="scss">
      @use "@fortawesome/fontawesome-svg-core/styles";
      @use "src/app";
    </style>
    ```
    in `<svelte:head>`, it appears to work, without FOUC in dev, and with expected scoping :)
  - interesting
    - looking at the dom in the browser for the page under layout reset, Header and the error page are still loaded (as i would now expect) -- i can see the (scoped) css in `<head>`
    - but the css in the root layout `<svelte:head>` isn't there
    - but going to a nonexistant path beneath that loads the error page, with the header -- hmm -- wonder (a little) how that's working
    - when i load the page under layout reset with js disabled, i don't see the css for Header or `__error` loaded
      - but navigating to a nonexistant path beneath behaves the same (must be ssr)
    - oh wow -- if i load the css in js, a page under layout reset will include that styling if js is enabled, and not have the styling if js is disabled
      - same with a `<style global ...>` outside `<svelte:head>`
      - but putting the style in head is consistent (unstyled under the reset in both cases)
  - when building
    - loading with js, looks like it goes into `<link rel="stylesheet" href="/_app/assets/pages/__layout.svelte-9d8a0c1a.css">`
    - svelte `<style global ...>`, winds up in the same place, just at the end, rather than the beginning `<link rel="stylesheet" href="/_app/assets/pages/__layout.svelte-f7a5e27c.css">`
    - `<style>` in `<svelte:head>`, the style tag and contents are rendered directly in index.html
  - so, is it okay to inline css?
    - https://www.sitepoint.com/how-and-why-you-should-inline-your-critical-css/
      - inlining critical css is good, but you should be careful, becaue non-inlined css can be cached, and this caching can save data
  - ahh -- i can also `import app_css from "../app.scss";` and then `<link rel="stylesheet" href={app_css} />`
    - grr... but the `<link>`, i'm not even sure what it's doing, since commenting it out doesn't change anything (in dev, js enabled)
    - oops, i was silly, `app_css` here is the css as a string https://vitejs.dev/guide/features.html#css
  - note
    - there's a CSSWG draft for nesting https://vitejs.dev/guide/features.html#css-pre-processors so i guess that would take away my main reason for using sass -- might be nice too, since sass does introduce some extra things to keep in mind -- but i don't feel like switching right now
      - https://scalablecss.com/sass-in-2020/
      - https://simonecorsi.medium.com/moving-from-sass-to-postcss-why-what-and-how-f68b1bc760dc
      - sass hasn't given me any problems since i got it set up (and started using 'sass' rather than 'node-sass')
      - postcss, from these, sounds relative like a bit of a pain -- you have to choose plugins (so, kind of like markdown-it) -- and so, if everyone has different syntax depending on plugins, will vscode support auto formatting?
  - vite auto optimizes css? https://vitejs.dev/guide/features.html#css-code-splitting
  - loading via a `<script>` in `<svelte:head>`, i still need to `@use "src/lib/styles/vars" as *;` inside `app.scss`
  - made some notes in the readme, testing the various methods more methodically
  - commented on https://github.com/sveltejs/kit/issues/1948
    - previously i found https://github.com/sveltejs/kit/issues/714 most useful, but it's closed, and the newer one is still open

#### (2022-01-18)

- layout using grid
  - https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Grids
  - https://developer.mozilla.org/en-US/docs/Glossary/Grid
  - https://stackoverflow.com/questions/54812239/expanding-to-fill-remaining-space-in-a-css-grid-layout
  - https://css-tricks.com/auto-sizing-columns-css-grid-auto-fill-vs-auto-fit/
  - https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors
  - https://stackoverflow.com/questions/47047011/set-row-height-to-fit-content-height-in-css-grid
  - https://css-tricks.com/introduction-fr-css-unit/
  - https://developer.mozilla.org/en-US/docs/Web/CSS/grid
  - https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Subgrid

#### (2022-01-19)

- layout using grid
  - from yesterday
    - using grid layout for the global css and the error page does fix the problem where if the stack trace was too long it would overflow (clipping on the left hand side) -- now it overflows making the minimum width of the page wider
  - deciding to leave the header with a flex layout, at least for now
    - don't see enough potential benefit in rewriting it with grid, and it was hard to write lol
- nofollow on social links
  - https://moz.com/community/q/topic/68580/should-we-nofollow-social-links/7 looks like there are opinions on either side -- but the commenter is right, google doesn't say to nofollow social links to your own sites https://developers.google.com/search/docs/advanced/guidelines/qualify-outbound-links?hl=en&visit_id=637782150373605245-1994864115&rd=1
  - taking `nofollow` off
- `lib/splash` page
  - made it :)
  - some scroll in mobile landscape though
    - https://bugs.webkit.org/show_bug.cgi?id=141832
      - https://benfrain.com/the-ios-safari-menu-bar-is-hostile-to-web-apps-discuss/
      - https://github.com/w3c/csswg-drafts/issues/4329
    - i'm gonna leave that alone for now -- it's a small thing -- and the only real use for this splash screen at the moment (besides for fun) would be to have [pwa asset generator](https://github.com/w3c/csswg-drafts/issues/4329) use it to generate static images of various sizes -- which should work regardless of this

### pandoc (2022-01-19)

- https://pandoc.org
- date format for posts paths
  - decided i like the format `2022-01-19--blog`
  - it makes it clear that it begins with a data, and is easy to read and look at

#### (2022-02-13)

- looks like pandoc supports both the new and the old github flavored markdown https://pandoc.org/MANUAL.html#markdown-variants
- `pandoc --list-extensions`
  - `abbrevations` isn't fully functional
    - https://pandoc.org/MANUAL.html#extension-abbreviations
    - https://github.com/jgm/pandoc/issues/167
  - `attributes`
    - general extension subsuming `inline_code_attributes` and `link_attributes`, which are enabled by default
  - `compact_definition_lists` might be useful some day
  - `footnotes` is on by default, but doesn't support inline footnotes
    - oo! but `inline_notes` enables inline footnotes, and is default enabled
  - `rebase_relative_paths` i don't think i'll need, but might want to take note of
  - `smart` quotes, dashes, and such -- what i want, but i also want to be aware of it
  - `sourcepos` default disabled, but interesting capability
- summary
  - not going to enable any of these options, the defaults look good to me :)
  - in the future, might be worth making it so that individual md files can turn on or off options just for themselves -- if i end up with a use case for that

#### (2022-02-14)

- feliz día del amor y la amistad :)
- lua
  - https://stackoverflow.com/questions/4297655/lua-string-replace
- excaping curly braces
  - doing this in a [lua filter](https://pandoc.org/lua-filters.html) doesn't work, for code at least, since it happens before code parsing
  - code highlighting is added by the writer https://groups.google.com/g/pandoc-discuss/c/nl2mFDrIqfA
  - other options
    - could make a custom writer https://pandoc.org/custom-writers.html
    - don't know if a template would work https://pandoc.org/MANUAL.html#templates
      - just thinking about it, it seems like templates are for something else
    - could also parse the resulting html (e.g. with `parse5`, or maybe `jsdom`) and do the escapes and maybe other processing there
    - could also do it the way the svelte blog does and not treat the output as svelte code

#### (2022-02-14)

- updating stuff
  - npm update https://stackoverflow.com/questions/16525430/npm-check-and-update-package-if-needed/23980257#23980257
  - `^` vs `~` https://www.geeksforgeeks.org/difference-between-tilde-and-caret-in-package-json/
  - sveltekit
    - https://kit.svelte.dev/docs/modules#$app-stores
  - fontawesome
    - https://www.npmjs.com/package/@fortawesome/fontawesome-svg-core
      - js api https://fontawesome.com/docs/apis/javascript/get-started
    - was getting an error about some of the imports not working, from adapter-static
      - turns out "@fortawesome/fontawesome-svg-core" 1.3 has this error, and 1.2.36 does not -- so i downgraded, at least for now
    - i also tried the fontawesome 6.0 icons, but didn't like them as well as the 5.15 ones that i'm currently using
      - specifically, home and gitlab didn't fit with the other icons as well
- parsing html
  - https://github.com/inikulin/parse5
    - https://github.com/inikulin/parse5/blob/v6.0.1/packages/parse5/docs/index.md
  - https://github.com/jsdom/jsdom

#### (2022-02-24)

- working just a little on the pandoc writer
  - https://dev.w3.org/html5/html-author/charref

#### (2022-02-25)

- putting python and ruby back in dependencies
  - i'm gonna use pygments, which i always get via pip
  - might want ruby and python (the languages) at some point

#### (2022-02-26)

- pandoc writer
  - https://pandoc.org/lua-filters.html
  - https://www.lua.org/manual/5.3/manual.html
  - https://prismjs.com
    - use `<pre><code>...`
    - they have `class='language-html'` on both `pre` and `code`
    - what i used to have was `class='highlight'` on `pre`
    - pygments puts is on a surrounding div by default

#### (2022-02-28)

- pandoc writer
  - lua doesn't have a lot of things on it's own, but you can often make them
    - https://stackoverflow.com/questions/1426954/split-string-in-lua
    - set type http://www.lua.org/pil/13.1.html
      - referened by https://stackoverflow.com/questions/2282444/how-to-check-if-a-table-contains-an-element-in-lua
    - print tables https://stackoverflow.com/questions/9168058/how-to-dump-a-table-to-console
  - you can also install stuff via luarocks
  - someone implemented the pandoc ast in svelte components -- super interesting idea -- looks like he's just personally using it though
    - https://twitter.com/Hadro/status/1490775723950411781
      - https://github.com/bmschmidt/pandoc-svelte-components

#### (2022-03-01)

- pandoc writer
  - pandoc seems to parse html by tag
    - `script`, `style`, and `textarea` tags are interpreted literally, even with `markdown_in_html_blocks` enabled https://pandoc.org/MANUAL.html#extension-markdown_in_html_blocks
    - `div` (and i imagine `span`) elements are parsed into their own pandoc type
    - `svelte:head` is sometimes treated as just a paragraph...
      - but not always... for instance the opening, but not closing tag, is treated differently if followed by a `meta` tag
      - on a related note, `:`s are treated interestingly by vscode syntax highlighting
        - if i have a backtick block with language `html:svelte:head` it will highlight the same as `html` in vscode
    - can't find a way to tell pandoc to parse certain html tags differently
      - besides maybe writing a custom reader, but didn't look into that

#### (2022-03-05)

- pandoc writer
  - mathml vs svg
    - https://math.meta.stackexchange.com/questions/8881/what-is-the-difference-between-the-3-math-renderers
    - mathjax can render to all 3
  - wrapping mathml and svg in a conditional tag of svg https://sora2455.github.io/MathMlNow/
  - mathjax can render server side now (more easily than before) https://docs.mathjax.org/en/latest/upgrading/whats-new-3.0.html
  - detecting mathml support
    - https://stackoverflow.com/questions/4827044/how-to-detect-mathml-tag-support-mfrac-mtable-from-javascript
  - escaping braces inside attributes
    - http://lua-users.org/wiki/TernaryOperator
    - https://svelte.dev/repl/2290fc67e7c0479881a266abb34e0f4d?version=3.20.1
    - https://dev.w3.org/html5/html-author/charref

#### (2022-03-16)

- pandoc writer
  - lua
    - moving some things into a `util` module
      - https://stackoverflow.com/questions/20667757/lua-modules-whats-the-difference-between-using-and-when-defining-func
  - thinking again about using filters rather than a custom writer
    - the advantage being mostly (i think) that then i don't have to have all that other stuff in there that i'm not changing
    - also, that way i get to keep some html writer features, just in case i need them
      - e.g. if you pass `--email-obfuscation javascript` on the command line using the default writer, it will do what it says -- i'm not sure how though, since i don't see that command line flag in my current custom lua writer -- do you have to do things like that in haskell? do i just now know how? a medium-quick google hasn't helped me
    - the documentation on the filters appears to be better: for a custom writer, there's essentially just an html example writer (that is close, they say, to the default one, but misses some important things like code highlighting)

#### (2022-05-01)

- it looks like `stuff` can be used to pass data from routes to layouts? https://kit.svelte.dev/docs/loading#input-stuff
  - hmm... but it might be better to use a template
- thinking about it more, i tink the only real reason to use filters rather than a custom writer would be to keep from having to write (or keep) various things
  - it's essentially an AST manipulation, rather than a marshalling of data
  - filters would also allow doing things in a different order -- like having several passes over the AST?
  - so i suppose it might be nice in some ways to use filters -- but also, using a custom writer makes more sense as far as _purpose_... to me, somehow

#### (2022-07-01)

- https://pandoc.org/custom-writers.html
  - new style custom writers are available in pandoc now (as of april or so)
    - i think i might want to use them :)

#### (2022-07-02)

- working on getting the project working on my new m1 mac mini :)
  - using `asdf` instead of `nvm` for node
    - modifying relevant files
  - had to `npm audit fix`
  - had to change my shortcut in safari to point to this computer instead of my macbook air
    - actually, changed to `localhost`
- working on lua stuff for pandoc
  - using `asdf` to install lua 5.3.x
  - working on tests

#### (2022-07-06)

- started working on a new style pandoc writer for svelte

#### (2022-07-07)

- new style pandoc writer

#### (2022-07-08)

- finished code and codeblock :)
- not going to implement default language for now
- not going to do "link rel canonical"s for now
  - shall consider them again if i start using paths for filtering, or other things
  - for now, all the paths i can think of will be canonical all on their own
    - provided someone doesn't copy my site :p
- mathml
  - https://caniuse.com/mathml
    - safari (desktop and mobile) support it
    - mathml core support will be in chrome 106
      - looks like it'll be stable released near the end of september https://chromiumdash.appspot.com/schedule
    - but no mathml for android chrome that i can see yet
  - my current chrome is 103
  - considered using mathjax
    - but client side depends on js
    - server side looks complicated... and mathml support looks like it's coming (back) to chrome, and i don't have plans to actually write math right now, and even if i do, it looks like it'll display as tex on browsers that don't support mathml (tested in chrome 103)
- npm update
  - fixed a few errors due to breaking changes in sveltekit
  - looked at fontawesome again
    - still like v5 icons better than v6
    - the `@fortawesome/fontawesome-svg-core` package now works at `^1.2`, so i changed it to that
      - i tried using the most current version, but there were some errors that i don't want to look into closely right now

#### (2022-07-09)

- pandoc writer
  - image

#### (2022-07-12)

- pandoc writer
  - almost done!

#### (2022-07-12)

- pandoc writer
  - finished! maybe :p
  - intereting https://english.stackexchange.com/questions/127892/are-curly-braces-ever-used-in-normal-text-if-not-why-were-they-created
  - i feel like {} are used infrequently enough in english that i don't need to escape them outside math and code for now
    - i'm not sure about this though
    - we'll see, as i write an actual blog post or two :)

#### (2022-07-14)

- pandoc writer
  - finished template for now :)
- blog post styling
  - how to markup subtitles http://html5doctor.com/howto-subheadings/
  - couldn't figure out how to get vite to properly reload on pandoc template change
    - ended up adding `include ... "...?raw"` so that the file is imported by the page. even though it's not used, seems to work :) (as a temporary fix, shouldn't need to have it this way for long)
  - https://dev.to/ananyaneogi/html-can-do-that-c0n

#### (2022-07-15)

- blog post styling
  - https://justmarkup.com/articles/2020-09-22-styling-and-animation-details/
  - rows can't have borders unless the table border is `collapse` https://stackoverflow.com/questions/10040842/add-border-bottom-to-table-row-tr
  - can't have rounded borders with table border `collapse` https://stackoverflow.com/questions/628301/css3s-border-radius-property-and-border-collapsecollapse-dont-mix-how-can-i

#### (2022-07-16)

- notes
  - using js (not ts) for `<script>`
    - tried ts, but errors (?) weren't reported, and bad errors (?) just made the page not render... lol
    - i feel like ts in this case won't be helpful, since it'll be hard to see where things are going wrong, and we don't expect much code in these files anyway
  - using scss (not css) in `<style>`
    - nesting and single line comments are just so useful!
    - error messages for wrong stuff seem to be shown :)
- styling
  - table styling is hard... https://unused-css.com/blog/css-rounded-table-corners/
    - but this is the best write up i've seen! helped a lot
  - so... maybe there's a good reason `//` comments aren't allowed in css after all https://softwareengineering.stackexchange.com/questions/329053/why-doesnt-css-allow-single-line-comments
  - lol, was about to add to the lua writer, when someone mentioned css ;) https://stackoverflow.com/questions/40213184/how-can-i-alter-the-formatting-of-pandoc-footnote-marks
    - just wanted to put a space before the return arrow for pandoc footnotes
    - might want to change their styling altogether later, and for that i might need to add to the lua writer, but for now i think this is good
- symlinks don't do what i would expect in gitlab
  - they just show the path (as text) which is rather inconvenient
  - other options
    - move license back :p
      - and possible allow vite to serve it
    - duplicate license (no)
    - make top level license a real md file, with a link to the license file
      - but then we're counting on relative links in gitlab
      - and it's less convenient for anyone who downloads the source
  - i think i'll leave it for now

### some fixes on main (2022-07-16)

#### (2022-07-16)

- portrait mode text size on iphone
  - https://www.saintsatplay.com/blog/2015-04-25-preventing-text-scaling-on-iphone-landscape-orientation-change

#### (2022-07-17)

- fixing splash page
  - i had misunerstood the new [svelte layout](https://kit.svelte.dev/docs/layouts) rules

### blog (2022-07-17)

#### (2022-07-17)

- animate details elements?
  - js https://css-tricks.com/how-to-animate-the-details-element-using-waapi/
  - you can do some things with css... https://stackoverflow.com/questions/38213329/how-to-add-css3-transition-with-html5-details-summary-tag-reveal
  - but it looks like for the type of animation i'd want, i'd need to use js (just like for the header animation)
- ts notes
  - https://stackoverflow.com/questions/23314806/setting-default-value-for-typescript-object-passed-as-argument
- moving preprocessor js to its own package
  - https://blog.logrocket.com/es-modules-in-node-today/
  - https://stackoverflow.com/questions/64453859/directory-import-is-not-supported-resolving-es-modules-with-node-js
  - `__dirname`
    - https://flaviocopes.com/fix-dirname-not-defined-es-module-scope/

#### (2022-07-18)

- considered removing `posts` from the route to blog posts
  - perhaps using a `[slug]...`
  - i think i like having it there
  - it also makes this more organized in various ways
  - it's also flexible in the future
    - i can still make a `[slug]` later if i want to, and put it there, and move posts to e..g `_posts`, without chaging the route of any post
- working on getting json metadata to layout and index
  - about svelte `stuff` https://www.youtube.com/watch?v=2EQpQhz30yY
  - `sfuff` gets passed down and combined in load functions, and the aggregate value is available in the `$page` store https://kit.svelte.dev/docs/loading#output-stuff
    - but it appears that the load functions for posts don't get run before the blog index loads -- which makes sense now that i think about it lol
      - at least in dev
    - so we can't use this method to get metadata for each post into the index -- even though it does work as the docs say to pass data from a page up to a layout
  - passing data from parents to children https://www.reddit.com/r/sveltejs/comments/tomtw3/ways_to_pass_data_from_parents_to_child/
  - ahh, i think i was passing data child -> parent wrong before
    - i think you're supposed to put a load function in the child that returns an object with `stuff` set, and then to get it in the parent (layout), use the `$page` store
  - truthiness of `[]` https://stackoverflow.com/questions/19146176/why-do-empty-javascript-arrays-evaluate-to-true-in-conditional-structures
- oops -- switching to blog branch now
- ts fixing type errors
  - https://bobbyhadz.com/blog/typescript-ignore-property-does-not-exist-on-type
  - https://stackoverflow.com/questions/51439843/unknown-vs-any
- how to import scss
  - tested a litte (not super thoroughly, but hopefully enough) and it looks like the problems i was noticing with imports via vite (in js) have been resolved
    - i now notice no difference with js off in dev or preview vs imports via `<style lang="scss">` in `<svelte:head>`
- make 'page source' go to the exact page?
  - tried
  - it would be possible, but would have a lot of special cases
    - e.g. for md files i think i'd have to pass in the file path as metadata when i call pandoc. but for other files, it can be inferred from the path. except that sometimes the file is `name.svelte`, and sometimes it's `name/index.svelte`.
  - so i don't think i'll do it for now
- escaping metadata
  - escaping, and striping formatting from, title and abstract, when they come from `title` and `abstract` (and not from `m.title`, ...)
  - passing the rest through
  - i'm not sure this is the best solution, but i think it's pretty good. i might have to change things up when i get some real data :) of course. this will at least keep the data valid though, since quotes might ligitimately appear in titles and abstracts (and are less likely to appear in other fields that would need to be escaped)
- adding a description meta field
  - recommended here https://kit.svelte.dev/docs/seo
  - gonna leave it the same as `m.abstract` for now

#### (2022-07-19)

- animate details elements
  - trying with js and css like with header
    - i can select the elements, but transitions on height aren't working. not sure if im doing something wrong, or if it's because the browser controls that, and my css / js gets preempted. but i kinda suspect the latter.
  - looking again at
    - https://stackoverflow.com/questions/38213329/how-to-add-css3-transition-with-html5-details-summary-tag-reveal
    - https://css-tricks.com/how-to-animate-the-details-element-using-waapi/
  - none of the css examples i've seen are good enough to warrent the effort, imo (for me)
    - reverting my attempt :)
  - the js from css-tricks works :) and it works to have it in the layout file
    - but it doesn't look quite right
    - i think maybe i have some padding issues
  - hmm, glad that i tried it, glad that it works, gonna leave it for later for now
- someone mentioned https://mdsvex.pngwn.io/docs for markdown with svelte
  - looks nice, uses remark though
- wondering if i could also use a bare `<script>` instead of a raw block
  - could :)
  - pandoc reads it in as a RawBlock
  - not sure i want to though, since the way i have it allows for more control
- blog index
  - example https://github.com/svelteland/svelte-kit-blog-demo/tree/main/src/routes
  - though maybe routing and props have changed since it was written https://kit.svelte.dev/docs/routing#endpoints-page-endpoints
    - not surprising since i `npm update`ed recently, and i just had to do it again to get this behavior lol
  - using js to go through the dir, and then pandoc to process each document for markdown, would work
  - setting up a "posts" store, and then adding to the store in the pandoc svelte template, and then making sure all pages are imported in the blog index (via `import.meta.glob`, and calling the resulting functions) also works
  - but! https://svelte.dev/tutorial/module-exports
    - "anything exported from a `context="module"` script block becomes an export from the module itself"
  - so setting the meta variable to be exported from the module script, then using `import.meta.glob` to import the modules we want and getting it directly from there, also works :)
  - was having the problem that the data would show up if i edited the script sections, and flash on refresh, but then disappear
    - turns out, `load` functions run on the server and the client -- and i think the rest of the script stuff does too -- and on the client we can't `import.meta.glob`, i suspect
    - so i put the logic back into an endpoint, where i guess it belongs if you only want it to run on the server
      - there might also be other ways to do it, but this keeps the code away from the client too, which is nice since it's not needed there
  - i wanted to avoid reprocessing the markdown since
    - that takes extra time
    - it doesn't generalize well in case i'd like to write a post or two in svelte or something else
  - was having the problem that the data would only show up on refresh
    - had to `await` the import
- about relative links
  - https://stackoverflow.com/questions/71134052/how-to-make-relative-href-work-in-sveltekit
    - https://github.com/sveltejs/kit/issues/1405
    - https://kit.svelte.dev/docs/configuration#trailingslash

#### (2022-07-20)

- 404 behavior changed
  - https://kit.svelte.dev/docs/layouts#error-pages-404s
- "source" link directly to file
  - can't use load functions in components (that aren't pages or layouts) https://kit.svelte.dev/docs/loading
  - same question :) https://stackoverflow.com/questions/72579031/how-to-fetch-data-inside-sveltekit-component-that-is-not-a-page
  - got it!!!
- added description to markdown metadata
  - debated whether to have it default to `abstract`
    - decided not to
      - they feel different
      - i include the abstract in the ld+json data, so i don't feel like it's necessary to make sure `<meta ... description ...>` is filled if there's nothing unique to put there
- make md external links target \_blank
  - don't see a pandoc option for this
    - or a thorough description of how links are read :p
    - pandoc does seem to distinguish between internal and external links though
    - `google.com`is read as an internal link, so apparently external links need to start with `...://`
  - https://en.wikipedia.org/wiki/URL
    - looks like a relative link will never contain `://` ?
      - i suppose, without reading everything thoroughly, that this is at least likely enough
  - relative links (e.g. `splash` from `/lib/test-markdown`) appear to work :) which is nice
    - and a bit surprising, given what i learned about svelte and the `trailingSlash` config option the other day
    - not gonna look into it right now, just glad :)

#### (2022-07-21)

- moving css import for md blog posts to the layout
- blog index
  - card design
  - nested links https://css-tricks.com/nested-links/
    - good thorough write up https://www.sarasoueidan.com/blog/nested-links/
      - except i don't like not being able to select the text...
    - also great, about cards in general https://inclusive-components.design/cards/
      - i think the solution he settled on was js, but i like those tradeoffs just as little :p

#### (2022-07-22)

- blog index
  - styling lists as command separated lists https://stackoverflow.com/questions/1517220/how-to-style-unordered-lists-in-css-as-comma-separated-text
    - this works 😊
    - but now i'm writing author lists in 3 separate ways... lol
      - svelte template way
      - js in copyright footer
      - this way in the card for each post
    - i could template the metadata parts in svelte, in the layout, rather than in the pandoc template, which would have some advantages, but... hmm. i kinda want that to be its own thing.
  - css `:` vs `::` https://css-tricks.com/almanac/selectors/a/after-and-before/#aa-vs
  - https://neumorphism.io/#e0e0e0
  - labeling a list https://stackoverflow.com/questions/1141639/how-to-semantically-provide-a-caption-title-or-label-for-a-list-in-html/35019847#35019847
  - what are data attributes https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes
  - svelte conditionally wrap a tag around something
    - workaround https://github.com/sveltejs/svelte/issues/7528
      - with a link to an rfc that might be a better solution, discussion, etc.
  - css can select empty elements https://css-tricks.com/almanac/selectors/e/empty/
  - about spacing
    - width of a space https://stackoverflow.com/a/24972177
    - https://css-tricks.com/fighting-the-space-between-inline-block-elements/

#### (2022-07-23)

- bug: header transparent on blog index page
  - fixed :) -- set z-index on header
- cloudflare supports direct uploads now! https://developers.cloudflare.com/pages/platform/direct-upload/
  - but i would have to recreate the project (again)
- there is also now an adapter specifically for cloudflare
  - https://github.com/sveltejs/kit/tree/master/packages/adapter-cloudflare
- admonitions
  - some simple styling from w3 https://www.w3schools.com/howto/howto_css_notes.asp
  - atlasian has https://confluence.atlassian.com/doc/info-tip-note-and-warning-macros-51872369.html
    - info -> tip -> note -> warning
  - asciidoctor has https://docs.asciidoctor.org/asciidoc/latest/blocks/admonitions/
    - note -> tip -> important -> caution -> warning
- bug: navigating to splash page then back to blog post would remove horizontal padding
  - removing margin on main?
  - refreshing fixed it
  - was because i had some unscoped :global styles in there
  - fixed :)
- checking and linting
  - general about ts in svelte https://github.com/ivanhofer/sveltekit-typescript-showcase
  - eslint was giving a lot of warning about "explicit any"
    - some things i kind of want to be `any` :)
    - but also, `$page.stuff` wasn't getting type inferred
      - for `$page.stuff` i found that i could type it (at least partially) if i wanted to, by exporting a type from `ls.json.ts`, and importing it in `_Header.svelte` and casting
      - but that seemed like a lot of work for what essentially boiled down to "i know there's an `ls` property on the `$page.stuff` object, pleast let me access without underlining it red and throwing compile errors"
      - so i think i'll just use `any` for this too, like i was
      - but i think i'll keep the type info in `ls.json.ts` :) at least for now

#### (2022-07-24)

- fixes
  - pandoc can also do bracketed spans https://pandoc.org/MANUAL.html#extension-bracketed_spans
  - interesting about sass selectors https://codeburst.io/how-to-do-sass-grandparent-selectors-b8666dcaf961
  - sass mix colors https://sass-lang.com/documentation/modules/color#mix
  - mixing colors in pure css https://developer.epages.com/blog/coding/css-can-do-that-color-manipulation-for-the-fearless/
- buy me a coffee
  - trying to customize the button
  - can a little on their site
  - can more if you mess with the `<script>` tag that they give you :)
  - but having the problem that svelte clears the button away when it hydrates
  - you can set hydrate to false for a page https://kit.svelte.dev/docs/page-options#hydrate
    - but this doesn't work for components
  - if you put the script in a bare html file, you can open it in safari, inspect the element, and copy the result :)
    - the svg, styling, etc, are all there

#### (2022-07-25)

- buy me a coffee
  - the button is nice (and is always a button, even when their script works) but in the end it's just a link, and a widget would be nicer
  - svelte partial hydration (open issue) https://github.com/sveltejs/kit/issues/1390
  - someone who used the buy me a coffee script in react https://stackoverflow.com/questions/62039217/add-buy-me-a-coffee-widget-to-react-application
    - and with some updated js methods, and translated to svelte, this works :)
      ```
      onMount(async () => {
        const event = new Event("DOMContentLoaded");
        window.dispatchEvent(event);
      });
      ```
    - but now the button is present on every page, even when i navigate back...
  - so there's a widget page
    - regular page https://www.buymeacoffee.com/benblazak
    - widget page https://www.buymeacoffee.com/widget/page/benblazak
    - someone suggested https://stackoverflow.com/a/67888640
    - haven't messed with iframes before, shall see how it goes
      - tooooo hardddddd lol -- for now -- gonna keep my button :)
  - added a bunch of comments in my BuyMeACoffee component

#### (2022-07-26)

- make link previews look nice
  - but i think to do this, i'm gonna change how page metadata is done
  - the current plan is
    - have every page put `meta` into `stuff` (and possibly also export it), like markdown blog posts currently do
    - have templates insert default values if they want to
    - have `title`, `description`, json+ld data, etc. generated in the root template based
  - this way we can have overridable defaults, and also enforce that every page has a title and such
    - i can't think of any other way to do that
- meta
  - reference
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
      - https://www.typescriptlang.org/docs/handbook/2/classes.html
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/get
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/class

#### (2022-07-27)

- meta
  - es6 proxies
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy
    - https://blog.logrocket.com/practical-use-cases-for-javascript-es6-proxies/
  - in ts, can't get the type of a private property https://github.com/microsoft/TypeScript/issues/47595
  - can't extend a type https://stackoverflow.com/questions/41385059/possible-to-extend-types-in-typescript
  - all the browsers support `hasOwn` now, but not yet typescript https://github.com/microsoft/TypeScript/issues/44253

#### (2022-07-28)

- meta
  - more about proxy and reflect (looks like a good article) https://javascript.info/proxy
  - ts inconsistencies :) https://blog.asana.com/2020/01/typescript-quirks/
  - someone trying to implement jsonld types; no answer; https://stackoverflow.com/questions/56096357/json-ld-typescript-classes
  - typescript mixins https://mariusschulz.com/blog/mixin-classes-in-typescript
  - ts namespaces are fancy objects? :) https://stackoverflow.com/questions/46857479/get-all-the-values-of-a-namespace-in-typescript

#### (2022-07-29)

- great explanation of js objects https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain
- multiple inheritance in js via proxy https://stackoverflow.com/questions/9163341/multiple-inheritance-prototypes-in-javascript
- extending proxy? https://stackoverflow.com/questions/37714787/can-i-extend-proxy-with-an-es2015-class
  - constructors can't have a specified return type https://github.com/microsoft/TypeScript/issues/11588#issuecomment-253602163
- js no native deep copy https://developer.mozilla.org/en-US/docs/Glossary/Deep_copy

#### (2022-07-30)

- https://stackoverflow.com/questions/61148466/typescript-type-that-matches-any-object-but-not-arrays
  - looks like it's hard to make a type that means "object but not array"
- got stuck messing with npm and npx
  - npx can install packages that you try to run
    - but where?
    - https://stackoverflow.com/questions/63510325/how-can-i-clear-the-central-cache-for-npx
    - was indeed in `~/.npm/_npx`
    - which i could delete, and that seems to have worked
- trying to run my code now that i've separated it into different files
  - the way i was doing it before still works, even though i thought it didn't, or at least that i didn't need to worry about it
    - aaaaa
- proxies in constructors

  - `this` in the constructor is the object (of child class) that is being constructed, even if the constructor is inherited
  - this is still true if the child also has a constructor
  - the example https://javascript.info/proxy#proxying-a-getter where the proxy needs to use the `receiver` argument to let the getter be transparent has the proxy declared in the file scope, not in a constructor
    - in a constructor, `this` is the child, so if `target` is `this` i never have to worry, it's always the object being constructed
  - there is a nuance here during construction, in that `'a' in this`will return false before `super()` returns if `a` is a property on the child -- but `this` can't be used in a child constructor till afer `super()` is called, at least in ts
  - anyway, i probably didn't write that all super well, might be easier to redo the experiment tomorrow if i forget, but.. :)

#### (2022-07-31)

- proxies and class constructors
  ```ts
  class A {
    c = "a";
    constructor() {
      console.log("constructor-A");
      return new Proxy(this, {
        get(target, prop, receiver) {
          console.log("proxy-A", this, { target, prop, receiver });
          return target[prop];
        },
      });
    }
  }
  class B extends A {
    c = "b";
    constructor() {
      console.log("constructor-B-before-super");
      super();
      console.log("constructor-B");
      return new Proxy(this, {
        get(target, prop, receiver) {
          console.log("proxy-B", this, { target, prop, receiver });
          return target[prop];
        },
      });
    }
  }
  let b = new B();
  console.log("--- done constructing");
  console.log(b.c);
  ```
  outputs
  ```
  constructor-B-before-super
  constructor-A
  constructor-B
  --- done constructing
  proxy-A { get: [Function: get] } { target: B { c: 'b' }, prop: '0', receiver: B { c: 'b' } }
  proxy-A { get: [Function: get] } {
  target: B { c: 'b' },
  prop: Symbol(nodejs.util.inspect.custom),
  receiver: B { c: 'b' }
  }
  proxy-A { get: [Function: get] } {
  target: B { c: 'b' },
  prop: Symbol(Symbol.toStringTag),
  receiver: B { c: 'b' }
  }
  proxy-A { get: [Function: get] } {
  target: B { c: 'b' },
  prop: Symbol(Symbol.iterator),
  receiver: B { c: 'b' }
  }
  proxy-B { get: [Function: get] } { target: B { c: 'b' }, prop: 'c', receiver: B { c: 'b' } }
  proxy-A { get: [Function: get] } { target: B { c: 'b' }, prop: 'c', receiver: B { c: 'b' } }
  b
  ```

#### (2022-08-01)

- proxies and private properties
  - https://stackoverflow.com/questions/67416881/es6-proxied-class-access-private-property-cannot-read-private-member-hidden-f
  - https://esdiscuss.org/topic/why-does-a-javascript-class-getter-for-a-private-field-fail-using-a-proxy
  - after reading the links above, the following code is obviously wrong
    ```ts
    class A {
      #a = "a";
      get a() {
        console.log("a-get");
        return this.#a;
      }
      constructor() {
        return new Proxy(this, {
          get(target, prop) {
            console.log("a-proxy-get");
            return target[prop];
          },
        });
      }
    }
    class B extends A {
      #b = "b";
      get b() {
        console.log("b-get");
        return this.#b;
      }
      constructor() {
        super();
        return new Proxy(this, {
          get(target, prop, receiver) {
            console.log("b-proxy-get");
            return target[prop];
          },
        });
      }
    }
    let b = new B();
    console.log("access-a", b.a);
    console.log("access-b", b.b);
    ```
    output
    ```
    b-proxy-get
    a-proxy-get
    a-get
    access-a a
    b-proxy-get
    a-proxy-get
    b-get
    file:///Users/ben/gitlab/homepage/src/lib/modules/meta.ts:181
        return this.#b;
                    ^
    TypeError: Cannot read private member #b from an object whose class did not declare it
    ```
    but i can't think of a way to fix it, especially if we consider that the inheritance chain might be more than two deep -- i can't think of a way to access the original object
  - since i'm using proxies so heavily, i might just need to not use private fields. if we use `_a` and `_b` instead of `#a` and `#b`, the above works just fine
    ```ts
    class A {
      _a = "a";
      get a() {
        console.log("a-get");
        return this._a;
      }
      constructor() {
        return new Proxy(this, {
          get(target, prop) {
            console.log("a-proxy-get");
            return target[prop];
          },
        });
      }
    }
    class B extends A {
      _b = "b";
      get b() {
        console.log("b-get");
        return this._b;
      }
      constructor() {
        super();
        return new Proxy(this, {
          get(target, prop) {
            console.log("b-proxy-get");
            return target[prop];
          },
        });
      }
    }
    let b = new B();
    console.log("access-a", b.a);
    console.log("access-b", b.b);
    ```
    output
    ```
    b-proxy-get
    a-proxy-get
    a-get
    access-a a
    b-proxy-get
    a-proxy-get
    b-get
    access-b b
    ```
- testing
  - need to somehow test all the stuff i just wrote lol
  - https://svelte.dev/faq#how-do-i-test-svelte-apps
    - example using vitest https://github.com/vitest-dev/vitest/tree/main/examples/svelte
  - https://vitest.dev/guide/
  - can't get vitest vscode extension to run...
  - but the command line works, so i think i'll keep it :)
  - the vscode extension looks really new... maybe it'll work in the future

#### (2022-08-02)

- proxies and private properties
  - https://www.mgaudet.ca/technical/2021/5/4/implementing-private-fields-for-javascript
  - after reading that, i wanted to try again
    ```ts
    class A {
      #a = "a";
      get a() {
        console.log("a-get");
        return this.#a;
      }
      constructor() {
        return new Proxy(this, {
          get(target, prop, receiver) {
            console.log("a-proxy-get");
            if (prop === "b") return this[prop];
            return target[prop];
          },
        });
      }
    }
    class B extends A {
      #b = "b";
      get b() {
        console.log("b-get");
        return this.#b;
      }
      constructor() {
        super();
        return new Proxy(this, {
          get(target, prop, receiver) {
            console.log("b-proxy-get");
            return target[prop];
          },
        });
      }
    }
    let b = new B();
    console.log("access-a", b.a);
    console.log("access-b", b.b);
    ```
    output
    ```
    b-proxy-get
    a-proxy-get
    a-get
    access-a a
    b-proxy-get
    a-proxy-get
    access-b undefined
    ```
  - so this works! kind of.
  - `#b` is defined on the proxy that's returned from the base class. but not initialized.
  - also, the base proxy now has to know abou the private properties being defined on it, so it's not that useful i think
  - could maybe mess around with it more -- don't feel like i've groked what's going on -- but for now i think i'm content with this :)
- testing
  - unitialized class properties aren't working as i expected
    - https://github.com/vitest-dev/vitest/issues/1167
      - https://github.com/evanw/esbuild/issues/2195#issuecomment-1103925306
- removing ts from my 'meta' module
  - several times typescript has bitten me by being _almost_ javascript, but changing behavior in subtle ways that took a long time to track down. most recently, hit the issue above with unitialized class properties disappearing. strangely, my tsconfig has target set to esnext, so this shouldn't be happening -- and it doesn't happen when i run with node (which... still requires an experimental loader to run ts modules...), or when i transpile with esbuild myself, but when i run it with vitest (which should use the same settings...) they disappear. i forget what the other things were but... for a project this size, it's questionably worth it. gonna try without.
  - also, there's a proposal for native js type annotations: https://github.com/tc39/proposal-type-annotations
    - i wonder how this will interact with ts, since js seems to be evolving separately
  - also, as someone mentioned, there are lots of languages that compile to js. is ts really the best one? i'm feeling grumpy about that right now lol.
- removing ts from other things
  - leaving it in a few places :) where i think it helps

#### (2022-08-03)

- meta data flow
  - looks like `stuff` gets passed from layout -> page in the loader, and then is available to the layout, which is what i expected from the docs :)
  - toplevel things set below override things set above all together (so, objects are not merged)
  - but we can get the current `stuff` in the loader :) and objects are passed (not just json)
- back to changing the meta stuff
  - class fields, as far as i can tell, are initialized in order https://github.com/tc39/proposal-class-fields#execution-of-initializer-expressions
  - optional chaining with bracket https://stackoverflow.com/questions/58780817/using-optional-chaining-operator-for-object-property-access
  - is an object iterable? https://stackoverflow.com/questions/18884249/checking-whether-something-is-iterable
  - writing a generator https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function*
  - recursive generator https://stackoverflow.com/questions/32789593/recursive-generators-in-javascript

#### (2022-08-04)

- thoughts from last night
  - would it work to have a .update on each class, and then have constructor proxies just for default values (like ‘updated’)?
  - maybe a .check to verify that eg @type is valid and title and description exist
  - mmm, json-ld makes this hard, coz changing @type should change the object type…
  - could construct jsonld on the fly in a getter? … for every get though… —— hmm, or maybe only when @type changes?
  - maybe each class could have an \_outer prop — for @type changes, a class could this.\_outer?.\_inner_type_changed(this[“@type”])
  - might wanna rename @type outside of jsonld
  - classes could have constructor({copy, outer}) — and they could ignore any ‘\_’ prefixed props when copying
  - can’t decide whether proxy for default value should be returned from constructors inside eg Tag and OG, or wrapped around eg the tag and og props inside Meta
- meta
  - generators are a bit heavy. probably doesn't make much difference here, but...
    - https://dev.to/alekseiberezkin/es6-generators-vs-iterators-performance-1p7
    - https://www.measurethat.net/Benchmarks/Show/10218/0/generator-vs-array#latest_results_block
  - js no op https://stackoverflow.com/questions/21634886/what-is-the-javascript-convention-for-no-operation
    - seems like `() => {}` was the recommended one
  - looks like you have to set a global node version for vscode to debug, even though it'll run the local one https://stackoverflow.com/questions/59564623/how-to-use-nodejs-debugging-in-visual-studio-code-with-asdf-cannot-find-runtim

#### (2022-08-05)

- meta
  - properties and functions behave differently with `extends`, and properties are all defined on `this` (so, every non-private property is an own property)
    ```js
    class A {
      #a;
      a = 1;
      constructor() {
        console.log(
          "A",
          this.constructor.name,
          Object.keys(this),
          Object.getOwnPropertyNames(this)
        );
      }
      print() {
        console.log("A", this.a, this.b);
      }
      f1 = () => {};
      f2() {}
    }
    class B extends A {
      #b;
      b = 2;
      constructor() {
        super();
        console.log(
          "B",
          this.constructor.name,
          Object.keys(this),
          Object.getOwnPropertyNames(this)
        );
      }
      print() {
        super.print();
        console.log("B", super.a, this.a, this.b);
      }
      f3 = () => {};
      f4() {}
    }
    let b = new B();
    console.log(
      "b.hasOwnProperty",
      b.hasOwnProperty("a"),
      b.hasOwnProperty("b")
    );
    console.log(
      "b.hasOwnProperty",
      b.hasOwnProperty("f1"),
      b.hasOwnProperty("f2"),
      b.hasOwnProperty("f3"),
      b.hasOwnProperty("f4")
    );
    b.print();
    ```
    outputs
    ```
    A B (2) ['a', 'f1'] (2) ['a', 'f1']
    B B (4) ['a', 'f1', 'b', 'f3'] (4) ['a', 'f1', 'b', 'f3']
    b.hasOwnProperty true true
    b.hasOwnProperty true false true false
    A 1 2
    B undefined 1 2
    ```
  - super setters and getters https://stackoverflow.com/questions/34456194/is-it-possible-to-call-a-super-setter-in-es6-inherited-classes
  - inserting json-ld in header
    - https://github.com/sveltejs/prettier-plugin-svelte/issues/240
      - https://github.com/sveltejs/prettier-plugin-svelte/issues/59
      - https://github.com/sveltejs/prettier-plugin-svelte/issues/290
  - making a getter enumerable https://stackoverflow.com/questions/34517538/setting-an-es6-class-getter-to-enumerable
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty
  - og:image path has to be a full URL https://stackoverflow.com/questions/9858577/open-graph-can-resolve-relative-url
  - js URLs are kind of interesting https://developer.mozilla.org/en-US/docs/Web/API/URL/URL
  - looks like if you have a "shadow endpoint" and a load function, the load function wins (and has to fetch the data manually)
    - shadow endpoint proposal https://github.com/sveltejs/kit/issues/3532
    - though i'm assuming this will all change when routing changes

#### (2022-08-06)

- meta
  - the number of things that i've cought only because i've been testing is truly amazing
- not going to add copy button to code blocks for now
  - it requires js, and the css wouldn't be trivial, so i'd rather leave it till maybe later

#### (2022-08-07)

- style
  - https://stackoverflow.com/questions/8533432/creating-or-referencing-variables-dynamically-in-sass
  - looked againat postcss
    - https://postcss.org
    - looks really cool, but i'm still scared that the major things i want (nesting, color, mixins) will become deprecated or change
    - looks like `@apply` has been deprecated https://github.com/pascalduez/postcss-apply
    - i'm guessing the simple cases of `color` that i want to use will be okay? and i don't know about nesting -- without having looked into it too much
- ios footer is below address bar
  - because address bar is not included in `100vh`
  - https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
  - can use `min-height: -webkit-fill-available;`
  - decided i liked it better the way it is though
    - this way, there aren't accidental clicks on things in portrait, since the "buy me tea" button is close to the nav bar on the bottom
    - also, the footer content is fine being hidden untill scroll

#### (2022-08-08)

- meta
  - tried to think of a good way to redo how defaults flow, but didn't come up with anything that wasn't a bit gratuitously complicated, given that the svelte routing changes aren't done
  - so i think i should wait till then and reconsider
  - worst case (which isn't bad at all), there should be a way to keep the current behavior, and let defaults flow down from layouts
  - and as far as duplicated defaults go, there aren't many, and they're all in blog -- and the blog posts have all the data they need
  - mmm, i think there are a few things about how i'm transforming md -> svelte that will have to change when the updates hit though
- header links
  - tried adding a link around the header content -- `<h2><a ...> ...` -- but it breaks safari read view
  - putting text inside the header adds it to the toc also

#### (2022-08-09)

- header links
  - forgot where my other resarch was (probably wayyyyy up there somewhere)
  - https://css-tricks.com/link-header-header-link/
  - making `<h2><a ...>` work in safari reader view https://www.leereamsnyder.com/blog/making-headings-with-links-show-up-in-safari-reader
- regular links in headers
  - dunno if it's documented (or will stay this way), but apparently links are allowed in headers still, and will merely end the first link (the id link that i'm wrapping around the content) and be placed after it
  - pretty cool that this works even though i'm wrapping the header content in a link and a span
  - didn't realize i was already doing this in my license file :) -- had been assuming it wouldn't work
- canonical links
  - oooooops
  - the way i'm currently doing it, the canonial link to e.g. `/` is `http://sveltekit-prerender/`
  - looks like `url` in the loader, and `$page.url` in the page, are both `http://sveltekit-prerender` during build, and don't seem to be updated when they get to the browser -- at least in this case
  - fixed :)
  - and overall, this is fine... i feel like i want to make the base url static anyway, and redo some other things about metadata flow... as long as i can find a good feeling way to do them
- meta (again lol)
  - adding `$routes` as an import shortcut https://mtm.dev/svelte-vite-aliases
    - this works for vitest, but not when running with node
  - https://arunmichaeldsouza.com/blog/aliasing-module-paths-in-node-js
    - https://nodejs.org/api/packages.html#subpath-imports
      - but node subpath imports have to start with `#`
  - showing the resolved tsconfig / full tsconfig / tsc settings / computed tsconfig after extends
    - `npx tsc --showConfig`

#### (2022-08-10)

- vitest async function that throws and error https://vitest.dev/api/#rejects
- got bit by the dynamic import restrictions again
  - might have to use glob https://vitejs.dev/guide/features.html#glob-import
  - but it worked in test... so that's weird
- for some reason, using `path.join` in my toplevel `__layout.svelte` would cause my css to be ... weird
  - importing it was fine, but using it, even as in `path.join('a', 'b')` without assigning to anything, would cause the weirdness
  - it was still weird even if i used a different name for `path`
  - ahhh...... because `path` is a node thing

#### (2022-08-11)

- error on Data about not being able to write to authorText
  - some links, though i couldn't quite find what i was looking for
    - https://stackoverflow.com/questions/54724875/can-we-check-whether-property-is-readonly
    - https://medium.com/jspoint/a-quick-introduction-to-the-property-descriptor-of-the-javascript-objects-5093c37d079
  - looks like `'a' in d` will return `true` if `a` is a getter
  - and makeing the property not enumerable doesn't change that
  - we can `getOwnPropertyDescriptor` and check if something is writable (or just a value), but we have to search up the prototype chain ourselves
  - someone suggested somewhere to make an array of properties to ignore, which seems like it might be the easiest thing to do in this case
  - actually :) -- went and wrote a `isWritable` function, i think it should search the prototype chain (in any case it works for these classes so far), and it wasn't too bad :)
- getting metadata to update
  - different kinds of fetch https://rodneylab.com/using-fetch-sveltekit/
  - hmm... i can get `beforeNavigate` to run (and see the console.log in the browser console), and even to update meta that i put in the body, but the meta in svelte:head isn't updated

#### (2022-08-12)

- metadata in head
  - https://github.com/sveltejs/kit/issues/1035
    - https://github.com/sveltejs/svelte/issues/6463
    - json-ld can be in page body https://www.youtube.com/watch?v=lI6EtxjoyDU&t=94s
  - cannot get the `{@html ...}` in the head of the root layout to refresh on navigate
  - so, looking at other ways to do it
  - could put meta into the session object in a hook (in `getSession`)
    - this at least spares each page from having to fetch it, before putting it (manually) in the page `<svelte:head>`
  - maybe could inject it in in hook `handle`?
    - https://adamtuttle.codes/blog/2021/sveltekit-customizing-app-html-at-runtime/
      - great short write up of eventual solution
    - but looks like things might have change since then? or like my use case has a different recommended solution? https://github.com/sveltejs/kit/issues/3091
    - ahh okay
      - https://kit.svelte.dev/docs/hooks#handle
      - reading through the type information, in light of these examples, was very helpful
    - here's where `transformPageChunk` came from https://github.com/sveltejs/kit/issues/4910
  - wow
    - so putting it in hooks `handle` doesn't work, because navigation once the page is loaded (with js enabled, i imagine it's diferent if not) clicking between "blog" and "home" isn't a server request
    - which brings me back to square one almost, in that ... how can i do something on every page if not in the layout, and also not in hooks?
    - hooks `getSession` is the same -- doesn't run when navigating between pages
  - ahh
    - easier to see in chrome, but in safari too:
    - doing it the way i was (with load in layout, and then referencing that in svelte:head) actually does update :) it's just that the original content sticks around
    - so i'm hitting the hydration issue i found earlier, rather than anything truly weird
    - lol ^\_^ gosh that was hard
  - so actually, the issue i'm hoping for a fix for is
    - https://github.com/sveltejs/svelte/issues/6463
      - looks like it's been debugged https://github.com/sveltejs/svelte/issues/6832
        - and there's a pr https://github.com/sveltejs/svelte/pull/6977 but it looks stalled to me
    - but this pr looks good :) https://github.com/sveltejs/svelte/pull/7745
      - last review 9 days ago, so hopefully will get fixed in not too long
  - looks like even if i do it without an `@html`, if i use `{#if}` blocks the (possibly same) problem is there
- got some errors while testing, and so also redid my 404.html
  - there's a native way to do this now https://github.com/sveltejs/kit/issues/1209#issuecomment-1120367821
  - so i don't need my postbuild script and such

#### (2022-08-15)

- thought of removing \_type_set from Base and putting it in Data and JSONLD as a property with a getter and setter (to avoid the proxy in Base)
  - silly me forgot that the Base proxy was handling searching :)
  - also, since it's a bahavior common to both Data and JSONLD, it isn't completely odd to have it in Base
- interesting
  - graphql server can receive a query via either GET (with query in url) or POST (with query in body) https://graphql.org/learn/serving-over-http/

#### (2022-08-16)

- the new svelte routing stuff became available yesterday! glad i saw it. updating today.
- update svelte
  - routing will change significantly
    - discussion https://github.com/sveltejs/kit/discussions/5748
    - migration guide https://github.com/sveltejs/kit/discussions/5774
- using `Response`s and `readablestream`s
  - https://stackoverflow.com/questions/40385133/retrieve-data-from-a-readablestream-object
  - https://developer.mozilla.org/en-US/docs/Web/API/Response

#### (2022-08-17)

- working on drafts
  - https://medium.com/swlh/why-i-wont-be-purchasing-tailwind-ui-but-maybe-you-should-3fb1ed3b6c35
    - in this post, for other reasons, he makes a `--- text ---` element in css, which i was wondering how to do
  - grid shrinking and growing https://stackoverflow.com/questions/19848697/css-grid-where-one-column-shrinks-to-fit-content-the-other-fills-the-remaning-s
  - setting a border on `hr` will cause it to grow right (and possibly down or up, didn't check), unless `box-sizing: border-box;` is set on it
    - https://stackoverflow.com/questions/19848697/css-grid-where-one-column-shrinks-to-fit-content-the-other-fills-the-remaning-s
    - https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing
- js
  - sets https://stackoverflow.com/questions/20069828/how-to-convert-set-to-array

#### (2022-08-18)

- md style
  - about margins collapsing (or not) https://stackoverflow.com/questions/19718634/how-to-disable-margin-collapsing
  - can't select things that are overflowed https://stackoverflow.com/questions/13213197/is-there-a-css-pseudo-selector-for-overflow
    - maybe with js, not gonna try right now
  - pre example that scrolls and doesn't cause the page to scroll https://codepen.io/SitePoint/pen/ZEVgJp

#### (2022-08-19)

- post style
  - trying to get the pre width working the way i'd like
    - https://stackoverflow.com/questions/32035406/how-to-limit-pre-tag-width-inside-flex-container
    - https://weblog.west-wind.com/posts/2016/feb/15/flexbox-containers-pre-tags-and-managing-overflow
    - hmm this says grid should behave the same way https://datacadamia.com/web/css/grid/overflow
    - in the end, setting `min-width` just on `article` turned out to be what was needed. didn't need to set it on any of the actual grid parents. didn't need to specify a max-width on `pre`.
  - just tested links in headers
    - from an accidental test in my license page, i knew it worked when the whole header was a link
    - i am honestly astounded that it works, with the filter i have, for headers that are only partially a link
    - 🤷🏻‍♂️😊
    - ahh, so pandoc is actually generating a nested link -- in both cases, even though i hadn't noticed that before
      - this is illegal in HTML5 https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a but it works in both safari and chrome 🤷🏻‍♂️
    - clicking on any part shows the whole title as clicked, which isn't perfect
    - google tab (to cycle through links) behavior is exactly what i'd want though, which wouldn't be true if i split the outer link into two different links
    - i also don't want to mess with any of the js solutions
    - and i'm not sure how i could cleanly do a generalized version of what i did in PostCard
    - reading things like [this](https://kizu.dev/nested-links/#update-from-2015-03-05), it sounds like, nested links used to break parsing
    - so just for fun i tried in firefox as well, and confirmed -- it works on current
      - macos (m1 mac mini) safari, chrome, firefox
      - ios (iphone xs) safari
    - so i'll leave it as is, and worry about workarounds if it breaks in the future :)
- made an image for sharing :)
  - references
    - https://ahrefs.com/blog/open-graph-meta-tags/
    - https://whatabout.dev/formats-for-open-graph-images
      - probably square
      - \>= 1200x1200
    - https://developers.facebook.com/docs/sharing/webmasters/#images
      - png
  - but i dunno how much it'll matter till the hydration issue gets fixed
    - hydration issue PR https://github.com/sveltejs/svelte/pull/7745
- linting
  - no constant condition
    - https://stackoverflow.com/questions/37466508/why-does-eslint-trigger-lint-errors-on-whiletrue-using-fibers
      - https://github.com/eslint/eslint/issues/2375
        - https://github.com/eslint/eslint/pull/6202
  - empty catch https://eslint.org/docs/latest/rules/no-empty#options
- general
  - nofollow https://www.contentpowered.com/blog/when-nofollow-links-blog/
- started writing a post!
  - realized i will need to include code from files
- lua
  - eval
    - called loadstring https://stackoverflow.com/questions/52230434/eval-function-in-lua-5-1
      - replaced by load
  - load https://www.lua.org/manual/5.4/manual.html#pdf-load
  - there's a python filter that does includes for source code https://github.com/jgm/pandocfilters/blob/master/examples/includes.py
  - so... oh yeah, lol, i can just add a attribute to the code block, i don't need a super general solution for this :)
  - might need to pass the filename to lua https://stackoverflow.com/questions/54458290/can-variables-passed-to-pandoc-be-used-in-a-lua-filter

#### (2022-08-20)

- in preview (and not in dev), meta for posts isn't being loaded, except after refresh
  - looks like my api pages are getting rendered once, with whatever query string is first passed
  - apparently you can't prerender pages with a query string?
  - so i guess i can't use query strings then, at run time
    - can still use them at build time, if i want... not sure how useful that is
  - another option would be to disable routing https://kit.svelte.dev/docs/page-options#router so that a full page load happens on each navigation
    - the pages themselves are prerendered correctly, so this works fine
- lua
  - was thinking to set "private" parameters dynamically, but... https://stackoverflow.com/questions/19648516/lua-multiple-parameter-into-table-depth looks like more work than it's worth
- source_url isn't working in prod
  - yay! fixed this
  - the issue was that `path.join()` was causint `https://...` to turn into `https:/...`, which for some reason worked in dev and preview, but once pushed caused it to act like `/...`
- 404.html isn't working in prod
  - what if my fallback ...path +page.svelte were a link to +error.svelte ?
    - this doesn't build
  - will have to experiment

#### (2022-08-21)

- 404.html isn't working in prod
  - looks like it was because i had a `[...path]` route with js throwing a 404, and once pushed, the empty accompanying page was being rendered, and the js to throw the 404 wasn't being run (or didn't matter)
  - using `fallback`, a 404.html page can be generated, but even though my `prerender` settings keep the site from becoming an SPA, the 404.html fallback page looks like an SPA entry point
  - if i delete my `[...path]` route and deploy, the error page is rendered
  - but there's a problem with named (nested?) layouts and error pages right now https://github.com/sveltejs/kit/issues/4582
  - ideas
    - i can mess with my layouts so they work with my error page
    - i can re-import what i care about in my error page
    - i can keep my `[...path]` route, and make it display an error
      - this breaks the non-static behavior of the site, i suppose, but it's the least invasive, while error / layout things are worked out in sveltekit
      - this also leaves my real error page looking a bit ugly, but... is this one of those things that will just fix itself as sveltekit moves towards 1.0? hopefully :)
  - well, grr, actually, my `[...path]` idea doesn't work
    - fallback page still renders completely blank in cloudflare pages
  - duplicating some of my imports, with comments -- hopefully error page layout issues will be fixed eventually :)

#### (2022-08-28)

- https://stackoverflow.com/questions/51131818/how-to-vertically-align-detailss-arrow-with-summarys-content

#### (2022-08-29)

- safari reader view is hiding the toc
  - not sure why...
  - it doesn't seem to be `<nav>`, and i don't see anything obvious about the links that would make it not work
  - didn't look closely or think hard about this though
  - gonna leave it for now

#### (2022-08-31)

- importing a file that may or may not exist is harder than i thought it'd be https://stackoverflow.com/questions/68027632/conditionally-import-conditionally-existing-file-in-vue

#### (2022-09-02)

- resume
  - if an svg has id attributes, and you include it in a document more than once
    - usually it seems not to matter
    - but if one of them is `display: none;` at some point (e.g. in a media query), the other may not work
    - had this problem with my colored butterfly :)
    - google turns up comments for react and vue, but nothing for svelte or vite that i saw
    - so what i ended up doing was, using svgo to simplify the ids that were already there, then making the svg into a svelte component, and putting `{prefix}-` in front of all the ids
      - prefix generated with `@lukeed/uuid` -- so it's not guaranteed not to clash, but should be pretty good
      - i also exported the `prefix` variable, so a component could assign it's own prefix when using the component
    - i don't think this is worth doing to every svg (manually) so i'll just do it as needed
  - having this problem with my images, where there's a gap beneath
    - https://stackoverflow.com/questions/5804256/image-inside-div-has-extra-space-below-the-image

#### (2022-09-03)

- resume
  - printing is harder than i thought it would be :p
    - safari messes with margins... somehow
      - the first page has ... an extra top margin? added?
      - subsequent pages have minimum top margin always?
    - safari also doesn't implement `@page`
    - chrome thinks pages are 640px? (or something small) when printing, which triggers my mobile layout
      - can be fixed by setting `size` in `@page` but i'm not sure exactly what it's doing, because setting it too large makes large side margins (as if it decided it was a different type of paper, even though it's printing, for me, to us letter) (the margins don't scale with the size exactly... there are increments)
    - in the end, i adjusted my media queries so that `print` is explicitly part of the selectors for larger size screens
    - at least chrome gets the margins right :)

### main (2022-11-01)

#### (2022-11-01)

- hydration issue
  - original notes in "waiting"
    - hydration issue PR https://github.com/sveltejs/svelte/pull/7745
      - will stop the duplication of my page metadata
      - merged :) -- unreleased (after 3.50.1) https://github.com/sveltejs/svelte/blob/master/CHANGELOG.md
  - fixed in svelte 3.51 :) https://github.com/sveltejs/svelte/blob/master/CHANGELOG.md
- npm update
  - brings svelte to 3.52 :)
- fixing issues
  - prerender things https://github.com/sveltejs/kit/pull/6197
    - more changes around prerender https://github.com/sveltejs/kit/pull/6392
    - i'm using adapter-static for build, and i don't think things are prerendered the same way in dev anyway
    - so i'm just going to remove the prerender setting from my config
  - layouts have changed yet again, it seems https://kit.svelte.dev/docs/advanced-routing#advanced-layouts
    - mentioned in september blog post https://svelte.dev/blog/whats-new-in-svelte-september-2022
      - docs https://kit.svelte.dev/docs/advanced-routing#advanced-layouts
      - pr & migration https://github.com/sveltejs/kit/pull/6174
    - it actually is cleaner to reorganize things this way, who would have known :p
  - nested layouts and error pages https://github.com/sveltejs/kit/issues/4582
    - once fixed, can remove duplicate imports in my error page
    - no longer an issue, now that layouts have changed
  - `$app/env has been renamed to $app/environment`
  - some errors about data not being serializable, eg
    ```
    Data returned from `load` while rendering /(root) is not serializable: Cannot stringify arbitrary non-POJOs (data.meta.display.page)
    ```
    - https://github.com/sveltejs/kit/discussions/7259
      - > you only return POJOs from `load` functions
      - even though [the docs](https://kit.svelte.dev/docs/load#shared-vs-server-output) sound to me like you can return anything that can become a POJO...
  - security warning
    ```
    ecurity: Anchor with "target=_blank" should have rel attribute containing the value "noreferrer"
    ```
    - googling lead me to believe that updated browsers handle this themselves, but I suppose it doesn't hurt to add it... at least for now
  - a11y warning
    ```
    A11y: visible, non-interactive elements with an on:click event must be accompanied by an on:keydown, on:keyup, or on:keypress event.
    ```
    - i guess i'll set a `on:keypress`
    - lots of examples use `e.keyCode` but that's deprecated. use `e.code` instead https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code. or use `e.key` maybe https://www.w3schools.com/howto/howto_js_trigger_button_enter.asp.
    - i don't see what good it does to add this, but it doesn't seem to hurt either
    - playing with this reminded me that it would be nice if the header closed on any click inside it
      - the nieve way of doing that though -- moving the `on:click` to `<header>` -- seems to make the button/checkbox not work for closing... which i don't really understand
      - wasn't my goal to mess with this today though, so as long as it works as well as it did before, i think that's okay :)

#### (2022-11-02)

- deployed and got an error

  - pages aren't loading correctly
    - browser console shows that it's the `__data.json` files that aren't loading
    - clicking around leads to other weirdness too
  - tried removing the fallback, since it didn't look like it was doing quite what i wanted in the build folder
  - and sure enough, it fails to build now

    - says that `src/routes/(root)` is dynamic and gives some options

    ```
    > Using @sveltejs/adapter-static
      @sveltejs/adapter-static: all routes must be fully prerenderable, but found the following routes that are dynamic:
        - src/routes/(root)

      You have the following options:
        - set the `fallback` option — see https://github.com/sveltejs/kit/tree/master/packages/adapter-static#spa-mode for more info.
        - add `export const prerender = true` to your root `+layout.js/.ts` or `+layout.server.js/.ts` file. This will try to prerender all pages.
        - add `export const prerender = true` to any `+server.js/ts` files that are not fetched by page `load` functions.

        - pass `strict: false` to `adapter-static` to ignore this error. Only do this if you are sure you don't need the routes in question in your final app, as they will be unavailable. See https://github.com/sveltejs/kit/tree/master/packages/adapter-static#strict for more info.

      If this doesn't help, you may need to use a different adapter. @sveltejs/adapter-static can only be used for sites that don't need a server for dynamic rendering, and can run on just a static file server.
      See https://kit.svelte.dev/docs/page-options#prerender for more details
    ```

  - removing the fallback of course removes my `/404.html` which isn't ideal
  - ahh
  - so it turns out that seting prerender to true fixes this
    - i shouldn't have simply removed the prerender setting, trusting that adapter-static would do the same thing. maybe it would under different circumstances, but in this case i needd it.
    - https://kit.svelte.dev/docs/page-options#prerender
    - adding `export const prerender = true;` to `routes/+layout.server.js` and putting the fallback setting in `svelte.config.js` makes the build folder look right at least :)
  - deploying again
  - yay! it seems to work

- error page is writing a `<title>` that isn't going away
  - tried using js to set the title on the error page
    - but then that doesn't go away when you navigate away
  - tried leaving the error page with its `<svelte:head>` and, rather than using `meta.tagHTML` in the root layout, using `meta.tag.title`, etc
    - this seems to work in dev, but doesn't work when deployed... the title is never updated for regular pages
  - tried just commenting out title and description on the error page
    - hmm... title still isn't updated when navigating back to `/`
    - it is still updated when navigating to `/blog`, and then updated when navigating to `/` after
  - i bet disabling ... routing? hydration? so that the pages just load as html ... would fix this, at the cost of reloading commen elements (right now just the header, which isn't too big)
    - hmm.. not sure if i want to do that though
  - i think this means that header hydration isn't working correctly, at least in relation to error pages, but... this is already taking longer than i planned to spend just now
  - trying out disabling csr https://kit.svelte.dev/docs/page-options#csr
    - doesn't fix the error page title problem, but does feel cleaner on mobile (at least, on ios)... so i suppose i'll leave it off :) for now
    - this does make the error page take a extra time to load (and makes things flash white in the meantime) -- but it's an error page, so...
  - so for now, the best option seems to be to not have a title (or description, though i didn't look at its behavior closely, and it may be different) on the error page
    - still doesn't work perfectly, but... i think it's acceptable, given the options i've found so far
- disabling csr for some reason messes with my header js
  - reenabling

#### (2022-12-30)

- npm update
  - sveltekit 1.0 announced!
    - https://svelte.dev/blog/announcing-sveltekit-1.0
  - `npm install @sveltejs/kit@1.0.0-next.588`
    - requires more recent vite
      - changing vite major version in package.json
      - npm update
    - and some other things
  - tested
    - it works! in dev
  - npm upgrade
  - test
    - still works in dev :)
  - deploy
  - test
    - seems to work properly!
  - that was painless lol

#### (2023-11-03)

- moved my domains from google to cloudflare pages
  - since google domains was sold to squarespace... which i really didn't see coming lol
  - glad that cloudflare added support for .app and .dev domains :)
  - was much harder than i thought it'd be actually
  - kept the notes in logseq, which i think i might mostly start doing, now that i've gotten this far developing this site
