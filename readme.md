# homepage

a personal page, to showcase things i'm working on (when i have them), and experiment for fun

## develop

### external dependencies

- unix tools
  - [git](https://git-scm.com), [git-lfs](https://git-lfs.github.com)
  - [bash](https://www.gnu.org/software/bash/), [make](https://www.gnu.org/software/make/), [rsync](https://rsync.samba.org), and possibly others
- [asdf](https://asdf-vm.com)
  - if you're on a mac, you can get this via [macports](https://www.macports.org) or [homebrew](https://brew.sh)
- [pandoc](https://pandoc.org) 3

### setup

```bash
git clone git@gitlab.com:benblazak/homepage.git
git clone git@gitlab.com:benblazak/_homepage_build.git

cd homepage

# might have to do this outside vscode
rm -r .git/hooks
ln -s ../git_hooks .git/hooks

asdf install
pip install pygments
asdf reshim python

npm install
npm run prebuild
```

### run locally

```bash
npm run dev            # run
npm run dev -- --open  # run and open the app in a new browser tab
npm run dev -- --host  # run and make accessible on the local network
```

### preview

```bash
npm run build    # local production build
npm run preview  # preview the last build
```

### deploy

```bash
npm run deploy
```

- deploying while `main` is checked out deploys to production
- deploying to another branch generates a [preview deployment](https://developers.cloudflare.com/pages/platform/preview-deployments)

### workflow

- changes should be implemented on a feature branch, then merged with `--no-ff` and the branch deleted. this way branch history is preserved without cluttering the list of branches.

### other commands

```bash
npx ...  # run in node environment
```

### dashboards

- https://dash.cloudflare.com/
- https://domains.google.com/
- https://search.google.com/search-console/

## references

- web technology references
  - mozilla https://developer.mozilla.org/en-US/
  - google https://developers.google.com/web/fundamentals
  - css-tricks https://css-tricks.com
  - html tag cheat sheet https://learn-the-web.algonquindesign.ca/topics/html-semantics-cheat-sheet/
- cloudflare
  - pages https://developers.cloudflare.com/pages/framework-guides/deploy-a-svelte-site
- svelte https://svelte.dev/docs
- sveltekit https://kit.svelte.dev/docs
- tailwind https://tailwindcss.com/docs
- d3 https://github.com/d3/d3/wiki
- markup languages
  - pug https://pugjs.org/api/getting-started.html
  - pandoc https://pandoc.org
  - asciidoctor https://docs.asciidoctor.org/home/
  - commonmark https://commonmark.org
    - parser with extensions https://github.com/markdown-it/markdown-it

## notes

organized and filtered (somewhat) from my [messy notes](notes.md)

### hosting and such (2021-10-02)

- registrar https://domains.google.com/registrar
- hosting https://pages.cloudflare.com
- i didn't want to use cloudflare dns for the whole domain, so i have a CNAME record in google's dns pointing my www subdomain to the cloudflare pages subdomain. i also have a 301 redirect (set up with the google domains web interface) redirecting my root domain to my www subdomain.

### sass setup (2021-10-04)

- can use `sass` or `node-sass` npm package
  - at the moment, i feel like using `saas` is overall better
- using `node-sass` https://daveceddia.com/svelte-with-sass-in-vscode/ -- with examples, so good to look at either way
- some sites said `node-sass` was faster than `sass`, and the site above talks about setting things up to work with `node-sass`, but i wasn't able to get `import "../app.sass";` in `__layout.svelte` to work without installing `sass` -- and given `sass`, `node-sass` seems to be unnecessary
- as it turns out, i don't really need sass at the moment, and the site runs slightly more smoothly in dev when i'm using plain css -- so that's what i'll do for now

#### update 2021-10-13

- probably need to ignore styles in eslint config
  - https://stackoverflow.com/questions/64644571/how-to-configure-sass-and-eslint-in-svelte-project
  - i was getting errors on a sass style in a .svelte file, and googling didn't turn up any better fix
- sass styles seem to be ignored by prettier, but scss styles are formatted

#### update 2021-10-23

- global sass variables https://www.reddit.com/r/sveltejs/comments/pmham1/sveltekit_how_to_set_up_global_scss_accessible_to/

### favicon (2021-10-05)

- drew by hand on my iphone in notes, then used that as a template for creating the icon in inkscape
- used inkscape to "save as" an optimized svg
- followed this reference to get all my icon renders and links and such set up https://dev.to/masakudamatsu/favicon-nightmare-how-to-maintain-sanity-3al7
  - for generating other formats and such from the svg https://realfavicongenerator.net
  - for making a maskable icon https://maskable.app/editor
- decided to put the code that belongs in the `<head>` section in a `<svelte:head>` element in `__layout.svelte`
- testing
  - macos 10.14: safari 14, chrome 94
    - had to add a link to `favicon.ico` in the `<head>` section, or the favicon wouldn't show in the tab (previously only had the svg and the apple-touch-icon referenced in the html, since the "dev.to" site above says explicitly referencing the ico shoudln't be necessary)
    - could go back and add the png favicons, but the ico should be fine where the svg isn't supported
    - will probably have to test this, along with other things, more in the future

#### update 2021-10-12

- tried to use the svg as an `img` on the site, and it wouldn't display. turns out gradient meshes aren't currently supported by browsers, and the polyfill that inkscape was including was erroring in chrome. i was able to almost duplicate the effect (since it's a simple mesh) in a way that is supported (layerd linear and radial gradients with a clip path), so i updated the icon.

#### update 2021-10-18

- the image was rendering as pixelated in safari when included as an `<img>`
- there are several ways to include svgs https://joshuatz.com/posts/2021/using-svg-files-in-svelte/#svite--vite
  - my favorites are to rename to `.svelte` or include via javascript (`import butterfly from "$lib/icons/butterfly-color.svg?raw"` and `{@html butterfly}`)
    - some modifications to the svg files are needed -- namely, removing the xml decleration, removing the `height` and `width` attributes (otherwise i was having a sizing issue), and adding `aria-hidden="true"`
    - when including as a .svelte, it looks like the content is being minified (at least a little), wheras including with js the content appears to be inserted exactly -- though in testing the non-minified version appears to be ever so slightly smaller, which is interesting
  - so for now, i'm going to include the icons with javascript -- partly because that's more consisten with how i'm using the fontawesome icons right now :)

### pwas and service workers (2021-10-07)

- this page doesn't need to be a pwa, but... for practice :)
- tutorial https://dev.to/100lvlmaster/create-a-pwa-with-sveltekit-svelte-a36
- about service workers
  - by google https://developers.google.com/web/fundamentals/primers/service-workers
  - by mdn https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers
- for now, my code is a combination of the code found in the tutorial and the code found on the mdn page
  - i opted to cache the whole site on install. if the site were to become big in the future, i might want to cache some things on demand (or not at all).
  - i opted not to cache other things that are fetched, since i want to link out to github (in the navbar) and perhaps other things in the future
  - caches are invalidated when the service worker script is updated (which will be on every build). again, if the site were to become big in the future, i'm not sure whether one would want to do that.
- things to possibly look at in the future
  - https://github.com/onderceylan/pwa-asset-generator
    - i don't yet have a splash screen for ios; it's loading with a white window and a black title bar

### nav bar (2021-10-08)

- there's a lot to learn about css, but so far i've been googling specific things. meaning to read (parts of) https://developer.mozilla.org/en-US/docs/Learn/CSS
- centering things in css comes up often https://css-tricks.com/centering-css-complete-guide/
- having a div fill the remaining space https://stackoverflow.com/a/24979148/2360353
- for clickable svg inside links, use an `img` tag http://sanjeevkpandit.com.np/posts/avoid-using-object-tag-inside-anchor-tag/
  - update: including the svg code directly seems to work too. see [update](#update-2021-10-18)
- google fonts https://fonts.google.com
- fontawesome
  - can be set up a few different ways
    - [the recommend way](https://fontawesome.com/v5.15/how-to-use/on-the-web/setup/using-package-managers#installing-free), using `fontawesome-free` and `<span>` tags was making my bundle unnecessarily big
      - they recommend using `<i>` or `<span>` tags. i originally was using `<svg>` so that svelte (vite?) wouldn't prune the "unused" css selector for `svg`, but that led to the icon area being quite big momentarily before the icons loaded (in safari at least). using `:global(svg)` takes care of the css pruning problem, and using `<span>` tags doesn't mess with the block size before they load.
    - doing it [this way](https://fontawesome.com/v5.15/how-to-use/on-the-web/advanced/svg-javascript-core), using `fontawesome-svg-core` and calling `dom.watch()` felt much nicer, but i didn't like that the DOM was being watched (for no reason, since i'm not doing anything dynamic here)
    - turns out you can `onMount(async () => { await dom.i2svg(); });` and only have it scan the DOM once :) -- but still...
    - turns out you can also import the icons in `<script>` and then `<span>{@html icon(faHome).html}</span>` -- yay! -- this way it only uses fontawesome libraries (no third party ones), icons are rendered once, bundle size is small (since it's tree shakable), and the compiler (and vscode) can check for unused icon imports -- so far, this is my favorite way
      - on some later pages, i was having trouble with huge flashing icons no matter what tag i used. ended up using this fix (turning off `autoAddCss` and importing the style sheet manually) https://medium.com/@fabianterh/fixing-flashing-huge-font-awesome-icons-on-a-gatsby-static-site-787e1cfb3a18
        - after doing this, the `<span>` tags aren't necessary around the nav bar fontawesome icons, so i removed them
  - for reference, see the docs for [`dom.i2svg()`](https://fontawesome.com/v5.15/how-to-use/javascript-api/methods/dom-i2svg), [`dom.watch()`](https://fontawesome.com/v5.15/how-to-use/javascript-api/methods/dom-watch), [`icon()`](https://fontawesome.com/v5.15/how-to-use/javascript-api/methods/icon), and [`configuration`](https://fontawesome.com/v5.9/how-to-use/with-the-api/setup/configuration)

### error page (2021-10-13)

- had warnings on the `load` function
  - `Missing return type on function`
  - `Object pattern argument should be typed`
- seems like it wanted explicit types (even when i didn't have the script marked as `lang="ts"`)
- if you look [in the svelte docs](https://kit.svelte.dev/docs#loading) they have an example for a regular (non-error) load function that references the type `Load` -- if you find that type in the source (i used vscode), there's also a type called `ErrorLoad` -- `ErrorLoad` defines a [call signature](https://www.typescriptlang.org/docs/handbook/2/functions.html#call-signatures) `(ErrorLoadInput): Promise<LoadOutput>`, and following this seems to make the compiler happy :)

- i feel like this should be in the docs, so even though it's long

  ```html
  <script context="module" lang="ts">
    import type { ErrorLoadInput, LoadOutput } from "@sveltejs/kit";
    export async function load({
      error,
      status,
    }: ErrorLoadInput): Promise<LoadOutput> {
      return {
        props: {
          title: `${status}: ${error.message}`,
          stack: error.stack,
        },
      };
    }
  </script>

  <script lang="ts">
    import { dev } from "$app/env";

    export let title: string;
    export let stack: string;
  </script>
  ```

#### update 2021-11-22

a much simpler way to write the error function was suggested [here](https://github.com/sveltejs/kit/issues/2712)

```html
<script context="module" lang="ts">
  import type { ErrorLoad } from "@sveltejs/kit";
  export const load: ErrorLoad = ({ error, status }) => ({
    props: {
      title: `${status}: ${error.message}`,
      stack: error.stack,
    },
  });
</script>

<script lang="ts">
  import { dev } from "$app/env";

  export let title: string;
  export let stack: string;
</script>
```

### cloudflare pages (2021-10-18)

- now using a `.node-version` file instead of setting the `NODE_VERSION` environment variable
- using a `Pipfile` for python version
  - `Pipfile` needs to be capitalized
  - default (and highest available) python 3 seems to 3.7
- custom 404 https://developers.cloudflare.com/pages/platform/serving-pages#not-found-behavior
  - had to use `window.location.pathname` for the path
    - https://css-tricks.com/snippets/javascript/get-url-and-url-parts-in-javascript/
    - need to guard references to `window` with `if (browser) ...` https://kit.svelte.dev/docs#modules-$app-env
- wrote a `postbuild` script in python
  - thought about a few different languages, but python is one i'm familiar with :) and i don't want to detour to learn another one right now
  - about npm scripts https://www.twilio.com/blog/npm-scripts
    - `postbuild` is automatically called after `build`

### nav bar (2021-10-23)

- wanted to style the nav bar for smaller screens
  - took much longer than i thought it would -- learned a lot :)
  - wrote even more of it down in my messy notes, so see those if you like. tried to clean it up here, and include only the high points. hopefully didn't miss too much.
- svelte transitions add and remove elements from the dom -- so if you want something to stay in the dom, you can't use them
- css transitions are great! but you can't transition on percents
  - except for (as far as i've found so far) within `transform: translateY()`
- css transitions can occur because of a change of a property set in inline styles (or on the element via javascript, which is equivalent) -- there just needs to be time for it to happen (so, no setting it to one value then immediately another, and hoping for a transition)
- there is a media query for javascript being enabled https://www.w3schools.com/cssref/css3_pr_mediaquery.asp
  - but it's not supported yet https://caniuse.com/?search=scripting
  - the `<noscript>` tag appears to work (though it solves a different problem) https://www.w3schools.com/tags/tag_noscript.asp
- you can tell if you've scrolled to the bottom of the page in javascript with `window.scrollY > document.body.offsetHeight - window.innerHeight;`
- looked a lot at this write up https://navillus.dev/blog/progressive-enhancement/
  - and the REPL it mentions https://svelte.dev/repl/267acb68b79647849f0532774d62c594?version=3.38.2
  - turns out, if you care about things working with javascript disabled, the "button method" is the way to go
    - i considered just not hiding the nav links if javascript was disabled -- didn't pursue that as fully, but i don't think it would have been easier (given that i didn't want the nav bar to flash open on page load when javascript wasn't disabled)
  - the author used a svelte action (`use:...` attribute on the class) to add a `js` class, to help switch in css on whether javascript was enabled -- i found you could also `import { browser } from "$app/env";` and then use `class:js={browser}` on the class to get the same effect (actions are much more powerful, just overkill in this case)
- you can set css vars in js
  - https://css-tricks.com/updating-a-css-variable-with-javascript/
  - https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties
- chain `:not()` selectors https://css-tricks.com/css-not-with-multiple-classes/
- with javascript disabled
  - you can use google fonts, you just can't load them with an `@import` in sass (i imagine this holds true for pure css as well). i ended up loading them inside a `<link>` in `__layout.svelte`. extra care is needed though, if you don't want the font download to block rendering. i ended up using `rel="preload stylesheet"` with `as="style"` (note: i only tested this a little).
  - even though the recommended (i think) way in svelte to include global styles is via javascript in `__layout.svelte`, it doesn't get transformed in any what that makes it work when javascript is disabled. found that (at least with sass), if you `@import "src/app.scss";` in a `<style global ...` it works without javascript -- and even hot reloads, which i thought was the stated reason for recommending loading via js.
    - if you do it this way, any `prependData` set for sass in `svelte.config.js` also applies to the global style
- fun styling
  - blur behind elements https://stackoverflow.com/questions/27583937/how-can-i-make-a-css-glass-blur-effect-work-for-an-overlay
    - safari requires prefix https://caniuse.com/css-backdrop-filter
  - reorder things https://stackoverflow.com/questions/7425665/switching-the-order-of-block-elements-with-css
- references
  - media query logic https://css-tricks.com/logic-in-media-queries/
  - hiding things accessibly https://a11y-guidelines.orange.com/en/web/components-examples/accessible-hiding/
  - accessibility for mobile nav https://a11y-guidelines.orange.com/en/web/components-examples/accessible-hiding/
  - `offsetheight` https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/offsetHeight
  - `clientHeight` https://developer.mozilla.org/en-US/docs/Web/API/Element/clientHeight
  - `scrollHeight` https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight
  - `box-sizing` https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing
- known issues
  - if the nav bar expands and is bigger than the viewport (as with my phone in landscape), you can't view the end of the nav without going to the top of the screen
    - the only solution i could think of so far involved scrolling the `top` value of the nav bar in javascript -- might be okay, if it's fast enough to feel smooth, but it didn't feel like a clean solution
- possible future work
  - it might be fun to have the nav section (the part with the links) behave like a second shade that pulls down over the content below, and slides up when it's hidden
    - i think this would involve a wrapper `<div>` around `<nav>` (given how the code currently is), and making that div `position: absolute`, with appropriate padding and such. would also have to adjust the javascript logic to make sure it's height is included in the header height when appropriate.

### research and cleanup (2021-10-29)

- service worker error when not connected to network
  - debugging was easiest in chrome on the desktop
  - debugging ios safari https://www.browserstack.com/guide/how-to-debug-on-iphone
    - only works when viewing the pwa installed from the internet though, not locally
  - had to add `"/"` to the list of paths for the service worker to cache
- for later
  - maybe change the html layout of my header / nav bar at some point, to possibly improve accessibility https://dev.to/masakudamatsu/don-t-nest-nav-inside-header-do-nest-the-hamburger-menu-button-inside-nav-6cp
- ios spalsh screen / startup image
  - [pwacompat](https://developers.google.com/web/updates/2018/07/pwacompat) is easy, but i think its splash screens are kind of ugly
  - [pwa-asset-generator](https://www.npmjs.com/package/pwa-asset-generator) is nicer :) but i can't get it to work when pointing it towards a locally hosted site, and i don't really want to work around that
    - and i'm hoping apple will [eventually](https://bugs.webkit.org/show_bug.cgi?id=183937) support this part of the webmanifest

### blog research (2021-11-01)

- learned a lot about svelte preprocessors
  - but please poke around in the source to see what i ended up doing
  - or look through the commit history to see the path i took, learning to get there
    - or my messy notes are also good for this, though super long
  - references
    - good example of a svelte preprocessor
      - https://github.com/reed-jones/svelte-markup
    - official
      - https://svelte.dev/docs#svelte_preprocess
      - https://github.com/sveltejs/svelte-preprocess
    - https://nodejs.org/api/child_process.html#child_processexecsynccommand-options
  - summary
    - you can make a preprocessor that takes `.md` (or other) files, and processes them accordingly
      - though doing so is not very well documented
      - and if you want to also be able to say `<template lang='md'>` that would take extra work
    - for pug (at least) you can use the `svelte-preprocess` preprocessor to process `.pug` files, with some extra work, since it looks like it processes the whole file if a `template` tag isn't found
      - though this also isn't really documented i think
- messed with using multiple languages to build in cloudflare
  - can handle python, but for me it ended up being kind of a pain
    - due to supporting only up to python 3.7 at the moment, and because they use pipenv rather than poetry or pdm
  - can handle ruby
    - and i was happy with it in this respect, except that it didn't support `~>` version specifiers for ruby, and doesn't support ruby 3 yet
  - the build environment is hard to debug, and i'm still not entirely sure what it is and isn't capable of
    - and you can't easily directly deploy (like you can with netlify)
    - but for some reason overall i think i'll stick with it, over e.g. netlify
  - overall
    - i'm now a big fan of sticking to one language when possible
    - but oh gosh it's convenient sometimes to pull in the ruby, python, etc., ecosystems -- so in that case i'm a big fan of pulling in just one extra lol
    - ruby seems easier to manage than python, though this is based on limited experience
- looked at various code highlighters
  - each language and stack seems to have their own
  - most good ones (?) are compatible with pygments styles
  - highlight.js is, importantly, not compatible, and i don't like it's highlighting as well, even though it seems to be the only one spoken of when using js
    - github and gitlab seem to use their own stuff, written in ruby, if i recall -- or something like that
- looked at and tested several markup languages
  - asciidoc: processed by asciidoctor
    - well specified and documented
    - extensible, and i really like their templating system
    - don't like the list syntax
    - poor editor (vscode) support relative to markdown (though it's really pretty good)
    - relatively complex, so i'd have a lot of up front work to do, if i wanted to be thorough (since i don't want to use their default html output templates)
  - markdown
    - commonmark
      - widely supported with various implementations
        - i probably would have used markdown-it
      - very simple (extended by almost everything that implements it)
      - seems to have some long running (at this point) trouble getting to a 1.0 version of the spec
    - markdown-it with extensions
      - pure javascript, and the implementation that vscode currently uses
      - extensions make it similar to, but not the same as i think, pandoc
      - many extensions, including some i wanted to use, are third party
    - github or gitlab flavored
      - partially availble via github's fork (cmark-gfm) of the reference implementation in c (cmark)
      - wrappers available (e.g. one for ruby)
      - not fully available, since i think both services wrap it with ruby, and implement some of their extensions in rails as part of their applications
    - kramdown
      - lots of nice features (extensions)
      - seems to treat commonmark as not very important in the docs
      - probably quite extensible, but didn't look as easy as e.g. pandoc
      - seems quite stable
      - but for some reason it feels kind of like an isolated niche at this point
    - pandoc
      - seems stable and nice to use
      - except that it's a haskell library + command line tool
      - not many people seem to be using it for web things
      - markdown-it extensions seem to reference pandoc syntax
      - has many imput and output formats, in addition to extended markdown
      - overall felt kind of like asciidoctor, but for markdown
  - there were others that i looked at too, but in the end these were the main ones i considered
  - summary
    - in the end, each markup language (aside from commonmark) seems to only really have one full implementation
      - so i'm basically choosing a tool, rather than a language, no matter what i do
      - unless i use pure commonmark, and do the rest in html... but that doesn't sound like the most fun option
    - markdown has the best editor support, and i like the syntax better than any others
      - especially since i'm primarily targeting html
    - but markdown is also simple relative to e.g. asciidoc, so i'll probably have to use yaml front matter, and template some things in svelte (or do something equivalent)
- i actually meant, when i started out here, to just do some exploration and implement something, without taking too long, probably asciidoc -- but life had other plans :) (or else, i just wanted to explore, and found much more complexity than i was expecting) and this turned into a research branch rather than an implementation one. but i don't mind :) -- i learned a lot of useful things, and made just a few changes along the way that i'm keeping. also, i'm glad i have this research history (bother here as a summary, in my messy notes as a research log, and in the code as a log of tests and experiments) to look back on later.

### cloudflare (2021-12-14)

- decided to make a new repository just for deploying
  - it's actually a really nice experience, building on my own machine, and deploying by copying the build artifacts over and committing and pushing (via script)
  - if i was working with others on this i would probably want a separate build server -- but on the other hand, one can do that with e.g. gitlab (or github), which might feel nicer -- the cloudflare pages build environment is nice when you're doing simple stuff, but it's very difficult to debug
- added analytics
  - just out of curiosity :)
  - doesn't seem like it works to set up analytics for custom domains on the cloudflare pages settings page, so instead i disabled that and used their "js snippet"
  - their js snippet passes a json object as a string, as the value of one of the attributes, and of course the `{}` get interpreted by svelte, so it's necessary to modify their string a bit (see the code in the root `__layout.svelte`)
- added redirects, from `.pages.dev` to my custom domain
  - references
    - google https://developers.google.com/search/docs/advanced/crawling/301-redirects
    - cloudflare pages `_redirects` file https://developers.google.com/search/docs/advanced/crawling/301-redirects
    - cloudflare pages `_headers` file https://developers.cloudflare.com/pages/platform/headers
  - pretty sure this wasn't possible till recently -- i don't think you can do it with the `_redirects` file, which would be ideal -- but you can set a 'Refresh' field in the header (see the google link, and the `_headers` file docs) which seems to work pretty well :)
  - the other option would be to use javascript to redirect (i don't think `meta refresh` tags in the html would work, because there woulnd't be a way to have them there only when accessing the site via `.pages.dev`), possibly also with a `canonical` link
    - there might be some benefits to doing it this way (see messy notes) but for now i think i'll stick with the header redirect

### about me (2021-12-17)

- learned about css grid layout
  - https://css-tricks.com/snippets/css/complete-guide-grid/
  - https://developer.mozilla.org/en-US/docs/Web/CSS/grid
  - https://www.digitalocean.com/community/tutorials/css-align-justify
    - prefix
      - `justify` : horizontal (inline) axis
      - `align` : vertical (block) axis
    - suffix
      - `items` : items within grid box, set on container
      - `content` : grid tracks within container
      - `self` : item within grid box, set on item
  - safari can show grid lines, but only in tech preview
  - chrome can show grid lines now :)
- realized that ios safari wasn't refreshing the pwa
  - there are (apparently) hacks around this, but i instead just deleted the web worker
    - i did have to hard refresh ios safari and macos chrome (at least) in order to get things to refresh after that, but now everything loads from the web and so is always fresh
  - left some of the other pwa files, like `site.webmanifest`, so that the site saved to my home screen (ios) would still look good
- wrote a license file
  - lol not related to this page, but it seemd like it was time
- git-lfs
  - decided to host my about-me "me" image with the site, rather than fetching from gravatar always -- but it's the first not-trivially-small image i'm using, so i wanted to get that set up well
  - ended up moving all my image files (besides svg ones) to lfs
  - and rewriting my repo history, since i don't know of anyone following it, so why not
- responsive images
  - svelte component
    - wrote a component that takes the output of a vite-imagetools import (using the `meta` output format) and generates a `<picture>` with `<source>`s and an `<img>`.
    - other ideas considered
      - writing macros with svelte-preprocess replace
        - https://github.com/sveltejs/svelte-preprocess/blob/main/docs/preprocessing.md#replace
      - writing something directly with sharp
        - docs https://sharp.pixelplumbing.com
        - tutorial that partially covers it https://rodneylab.com/sveltekit-image-plugin/
        - the thing about this is that the sharp code basically has to go into an "api" route in svelte, since it has to be executed server side only -- and then to get the feature of vite-imagetools (e.g. generated image caching) i'd have to duplicate that functionality myself
      - somewhere, someone also mentioned the idea of generating svelte files for each image, which could then be imported (or something like that)
      - and there is [a library](https://github.com/matyunya/svelte-image) that does preprocessing on `img` tags
    - lots of useful notes and references, but i'm gonna leave them in the component documentation (for `$lib/Image.svelte`) for now :) , rather than copying them here too https://gitlab.com/benblazak/homepage/-/blob/0cb4a432b3b3e9697f1a4a796d203884de59bb7f/src/lib/Image.svelte
  - extending vite-imagetools
    - docs https://github.com/JonasKruckenberg/imagetools/blob/main/docs/guide/extending.md
    - example https://github.com/JonasKruckenberg/imagetools/issues/86#issuecomment-878944821
    - vite plugin options https://github.com/JonasKruckenberg/imagetools/blob/main/packages/vite/src/types.ts
    - default resolve configs https://github.com/JonasKruckenberg/imagetools/blob/main/packages/core/src/lib/resolve-configs.ts#L17
    - entry point for vite imports https://github.com/JonasKruckenberg/imagetools/blob/c231165033105fad9e9af1c0965af081791ce8b0/packages/vite/src/index.ts
      - output format is handled separately from the rest of the config though, so i couldn't do quite what i wanted -- had to settle with `?preset=100&meta` rather than just `?preset=100`, to get the equivalent of `?w=100;200&format=webp;png&meta`
- fun facts
  - you don't set elements on `Map` objects the way you do on regular objects https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map (this was really hard to figure out lol)
  - can't use `===` to test `NaN` https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/isNaN

### cleanup (2022-01-10)

- scoping query selectors https://stackoverflow.com/questions/61598941/how-to-scope-queryselector-to-component-in-svelte
- opacity interacts with layering https://stackoverflow.com/questions/2837057/what-has-bigger-priority-opacity-or-z-index-in-browsers
- global styles
  - loading via js `import`
    - vite feature https://vitejs.dev/guide/features.html#css
    - dev
      - FOUC
      - if in root layout, a layout reset won't reset styles
      - js off, styles won't be loaded
    - preview
      - if in root layout
        - js on, reset doesn't work
        - js off, reset works
      - css appears to be loaded via `<link rel="stylesheet">`
  - loading via a `<style global lang="scss">` at the bottom of the file
    - dev
      - if in root layout
        - js on, reset doesn't work, but does cause FOUC
        - js off, reset works
    - preview
      - if in root layout
        - js on, reset doesn't work
        - js off, reset works
      - css appears to be loaded via `<link rel="stylesheet">`
  - loading via `@use "...";` in a `<style lang="scss">` in `<svelte:head>`
    - dev
      - if in root layout, reset works
    - preview
      - if in root layout, reset works
      - css appears to be inlined
  - notes
    - tested with safari 14.1.2 on macos 10.14.6 on 12 jan 2022
    - i'm using sass and typescript -- didn't test with plain css or javascript
    - if using plain css, another option would be to `<link rel="stylesheet">` to the file in `<svelte:head>`
      - this isn't possible with sass -- it seems to work in dev, but fails to build
  - conclusion
    - for my setup, it looks best to `@use "...";` the styles in a `<style lang="scss">` in `<svelte:head>`
- grid layout
  - does make some things simpler, relative to using flex layout
  - glad i tried both -- need to use them more to get a thorough feel for them
- `lib/splash` page
  - was a bit of a challenge getting the svg to resize correctly
  - small bit of vertical scrolling in mobile safari when in landscape -- leaving alone for now
  - not very useful just now, but was fun :)
    - and also was a good motivation for figuring out layout resets, and a couple other things

### pandoc (2022-01-19)

- references
  - https://pandoc.org
    - https://pandoc.org/custom-writers.html#new-style
      - https://pandoc.org/lua-filters.html
    - https://pandoc.org/MANUAL.html#templates
  - https://prismjs.com
    - use `<pre><code>...`
  - how to markup subtitles http://html5doctor.com/howto-subheadings/
  - table styling https://unused-css.com/blog/css-rounded-table-corners/
  - cool examples https://dev.to/ananyaneogi/html-can-do-that-c0n
  - interesting about css comments https://softwareengineering.stackexchange.com/questions/329053/why-doesnt-css-allow-single-line-comments
- updates
  - pandoc released new style writers around april
  - pandoc upgraded from lua 5.3 to 5.4
    - lua's point releases have breaking changes, but the pandoc folks thought they were't things that would effect most pandoc related lua code. they didn't effect me in any case.
- date format for posts paths
  - decided i like the format `.../blog/posts/2022-01-19--blog`
- using pygments
  - i have to escape the source code for svelte
  - using new style writers, i could use pandoc's default source highlighter, but i'd rather use pygments
- using mathml
  - supposed to be coming (back) to chrome soon enough, and none of the other options are my favorite for one reason or another
- decided to leave pandoc mostly default, except for the things i specifically wanted to change (see code). i'll probably end up changing more as time goes on, and especially as i write posts :)
- when working on templates, i couldn't figure out how to get vite to properly reload on template change
  - ended up adding `include ... "...?raw"` to include the template file
    - first added it to the template file itself (lol)
    - later moved to a script section of the test markdown file
- for the template
  - using js (not ts) for `<script>`
    - tried ts, but errors (?) weren't reported, and bad errors (?) just made the page not render
    - don't expect to be doing much scripting in markdown files anyway
  - using scss (not css) in `<style>`
    - nesting and single line comments are just so useful!
    - error messages for wrong stuff seem to be shown :)

### fixes (2022-07-16)

- portrait mode text size on iphone
  - https://www.saintsatplay.com/blog/2015-04-25-preventing-text-scaling-on-iphone-landscape-orientation-change

### blog (2022-07-17)

- references
  - nested links https://www.sarasoueidan.com/blog/nested-links/
  - proxy and reflect https://javascript.info/proxy
  - header links https://www.leereamsnyder.com/blog/making-headings-with-links-show-up-in-safari-reader
    - great short article, with links to other research
- maybe future
  - animate details elements
    - https://css-tricks.com/how-to-animate-the-details-element-using-waapi/
  - cloudflare
    - direct upload https://developers.cloudflare.com/pages/platform/direct-upload/
      - but for now, uploading via a separate gitlab repo works :)
    - svelte's `adapter-cloudflare` https://github.com/sveltejs/kit/tree/master/packages/adapter-cloudflare
      - though, i might just want a static site for now anyway :)
  - make "buy me tea" button a modal rather than a link
    - using an iframe, the way the "buy me a coffee" widget does
- moved js preprocessor stuff to it's own package in lib
- finished pandoc filters
  - fairly straightforward, if time consuming, once i chose a direction to move in. the main challenge here was choosing the direction.
    - one of the challenges there was that there are several options (and my preferred option was only released sometime after i first started considering all of this)
    - another challenge was looking throught the pandoc docs to find what was possible. i like the pandoc docs, but it doesn't feel like they're geared towards people trying to write a slight variant on an existing writer (which is what i was trying to do). and it doesn't seem like a lot of people do this, which made it hard to google for.
  - also wrote a template
    - the most challenging part of the template was the header
    - to keep things consistent across all the pages in the site, i eventually rewrote that part (lol)
    - my biggest lesson here was non-technical: even though i worked really hard on the svelte template and then ended up doing it a different way, all that work was worthwhile -- and it was quicker, choosing a path and trying it out, than it would have been to try and figure out the best path before putting much effort in.
- wrote a metadata package
  - this was quite challenging
  - my main goal was to be able to easily specify some metadata, and override the data used for particular purposes
  - this probably could have been achieved more simply, but i also wanted to preserve the information of what had been overridden and what hadn't, up until serialization
  - i originally intended to use the code client side, but it ended up being a lot of code, so in the end i decided to serialize the data server side
  - it was still a lot of fun to write though :)
  - i learned way too much about ts and js to summarize it well here -- i suppose the main points are
    - ts is _not_ js with types. i wish it was, but there are important, and sometimes subtle, differences. in the end i ended up switching to js with jsdoc type comments, and i'm glad i did.
      - when you're doing dynamic things, the types are more of a hinderance than a help, as you end up with a lot of implicit `any`s in the places you could most use typing help
      - js problems are also easier to search for :p
      - and js files are much easier to run!
  - for more about how things work, see `$lib/modules/meta/meta.js`
- wrote a js module to keep track of files, paths, and such so that i could do things like
  - list posts on my blog index
  - list pages on my lib index
  - find the file name for the current route, for the header "source" button
  - get the metadata for a route
    - this followed from all my work on the metadata module
    - metadata is combined much like svelte layouts are -- a \_meta.js file sets default data for that route, and all routs under it, and it can be overridden lower in the tree. final metadata for a page (at least for a page mapped one to one with a route) is stored in the page itself
- wrote a small api, using my modules -- and then learned that you can't prerender query parameters, and had to rewrite everything!
  - it wasnt too bad though, since i had function-ized everything
  - in the end i extracted the common code into a util.js, and put what would have been in the api files into .server.js page endpoints
- fixed my error page :)
- made many style changes, and tried out smaller things like filtering posts (might do this more in the future, with keywords, once there's enough posts to make it useful)
- other things, not necessarily in order
  - added a "buy me a coffee" page -- and then stripe introduced payment links, and i switched over to those. they're a bit less nice for the use case... but since buy me a coffee nearly makes you set up a stripe account (to link it, for payments), and since payment links are quite good -- and also because it doesn't seem like buy me a coffee's target audience is technical people setting up their own pages, as i am here -- i recommend those (stripe payment links).
  - on ios, pages extend beneath the adress bar
    - i decided i liked the result well enough
    - if you don't like this, you can use `min-height: -webkit-fill-available;`
  - flex containers (and i think also grid containers... though they seem to behave a bit differently) don't behave as i would have expected with regard to min-width
    - https://weblog.west-wind.com/posts/2016/feb/15/flexbox-containers-pre-tags-and-managing-overflow
    - my `pre` tags, if they had enough content, were extending the width of the page beyond the viewport, even thougi set them to scroll
    - setting `min-width: 0;` on the container (even though it itself wasn't grid or flex) solved it. i'm still not sure exactly what happened, but i'm glad i have some of the concepts in my head now.
  - nested links seem to work in up to date browsers now
    - reading the current posts you'll find by googling this makes it sound like this wasn't always the case
    - it's still against spec, but imho the least bad option in at least some cases
  - made an image for sharing, to link to in my opengraph tags
    - references
      - https://ahrefs.com/blog/open-graph-meta-tags/
      - https://whatabout.dev/formats-for-open-graph-images
        - probably want to make it square
        - and \>= 1200x1200 -- i ended up making it 1200x1200 px exactly
      - https://developers.facebook.com/docs/sharing/webmasters/#images
        - of the allowable image formats, my favorite is png
  - using `path.join` on a url will collapse the `//` after `https:` -- lol
    - this was a fun one to track down, since the resulting `https:/...` worked when it was served from localhost (in both dev and preview), but once i pushed it to cloudflare it would act like `/...` which of course linked to a page that didn't exist
    - solved by... not putting the `https://` part into `path.join` :)
  - svelte error pages also can be a bit tricky with the static adapter, deploying to cloudflare pages
    - so make sure to test not just dev and preview, but also after deploying (perhaps to a test deployment) ... ;)
- lots more i could say -- this was a super eventful month! -- but i think that's good for now :)
  - this clears up my current list of things to do, that aren't waiting on external events
  - it feels _so good_ to be (as much as anything ever is in software) finished :)
    - with the setup part of stuff -- still plan to write ... i hope at least once post :)
    - untill i discover more that needs to be done while writing (lol) (already happened once)
