import { defineConfig } from "vite";
import { sveltekit } from "@sveltejs/kit/vite";
import { imagetools } from "vite-imagetools";
import path from "path";

export default defineConfig({
  resolve: {
    alias: {
      $routes: path.resolve("./src/routes"),
    },
  },
  plugins: [
    sveltekit(),
    imagetools({
      resolveConfigs: (config) => {
        // lets us write `?preset=100&meta`
        // instead of `?width=100;200&format=webp;png&meta`
        const preset = config.find(([key]) => key === "preset");
        if (!preset) return; // default resolve configs

        const [, value] = preset;
        if (value.length !== 1)
          throw new Error("invalid preset: expecting 1 value");
        if (isNaN(value[0]))
          throw new Error("invalid preset: expecting integer");

        const widths = [1, 2].map((e) => e * value[0]);
        const formats = ["webp", "png"];
        const out = widths
          .map((w) => formats.map((f) => ({ width: w, format: f })))
          .flat();
        return out;
      },
    }),
  ],
});
