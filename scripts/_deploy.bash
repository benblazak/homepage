#! /usr/bin/env bash

set -e  # exit if any command fails

build="$1"
deploy="$2"

npm run lint
npm run check
npm run test
npm run build

scripts/deploy.bash "$build" "$deploy"
