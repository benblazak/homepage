#! /usr/bin/env bash

set -e  # exit if any command fails

build="$1"

scripts/rm-ds-store.bash
scripts/build-lib.bash
