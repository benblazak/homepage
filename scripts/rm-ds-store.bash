#! /usr/bin/env bash

set -e  # exit if any command fails

find . -name '.DS_Store' -delete
