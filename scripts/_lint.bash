#! /usr/bin/env bash

count () { (( err++ )); }

trap count ERR

echo --- prettier ---
npx prettier --ignore-path .gitignore --check --plugin-search-dir=. .
echo
echo --- eslint ---
npx eslint --ignore-path .gitignore .

exit $err
