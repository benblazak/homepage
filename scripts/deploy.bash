#! /usr/bin/env bash

set -e  # exit if any command fails

build="$1"
deploy="$2"

branch="$(git symbolic-ref HEAD)"
branch="${branch##refs/heads/}"

(
    cd "$deploy"
    git checkout -B "$branch"
)

rsync -a --delete \
    '.gitattributes' \
    "$build" \
    \
    "$deploy"

(
    cd "$deploy"
    git add .
    git commit -m "deploy $(date)"
    git push -u origin "$branch"
)
