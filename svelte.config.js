import adapter from "@sveltejs/adapter-static";
import preprocess from "svelte-preprocess";

import { fileType, makefile, markdown } from "./src/lib/preprocessors/util.js";

const extensions = {
  svelte: [".svelte"],
  markdown: [".md", ".markdown"],
};

/** @type {import('@sveltejs/kit').Config} */
const config = {
  extensions: Object.values(extensions).flat(),

  // - see https://github.com/sveltejs/svelte-preprocess
  preprocess: [
    makefile(),
    fileType(extensions.markdown, markdown({ standalone: true })),
    preprocess({
      preserve: ["ld+json"],
      scss: {
        prependData: `@use "src/lib/styles/vars" as *;`,
      },
      aliases: [["md", "markdown"]],
      markdown: markdown().markup,
    }),
  ],

  kit: {
    adapter: adapter({
      fallback: "404.html",
    }),
  },
};

export default config;
