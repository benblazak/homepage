import path from "path";
import { Meta } from "../meta/meta.js";

export class LS {
  /**
   * map from file to maybe (unresolved import)
   * - paths are absolute, but based on the routes directory
   */
  file_maybe = {};

  /**
   * map from route to maybe (unresolved import)
   * - trying to match the routes given by svelte
   * - routes from urls are encoded though, and everything here is plain text
   */
  route_maybe = {};

  /**
   * map from file to route
   */
  file_route = {};

  /**
   * map from route to file
   */
  route_file = {};

  constructor({
    root = "/src/routes",
    maybes = import.meta.glob("/src/routes/**/*.(js|md|svelte)"),
  } = {}) {
    for (const rootfile in maybes) {
      const file = rootfile.replace(root, "");
      this.file_maybe[file] = maybes[rootfile];
    }
    for (const file in this.file_maybe) {
      const page = /\/\+page(@[^/.]+)?\.(md|svelte)$/;
      const server = /\/\+server.js/;
      for (const re of [page, server]) {
        if (file.match(re)) {
          const route = file
            .replace(re, "") // strip suffix
            .replace(/(^|\/)\([^/]+\)(\/|$)/g, "/") // strip layout groups
            .replace(/\/$/, "") // no trailing slash
            .replace(/^$/, "/"); // empty is root
          this.route_maybe[route] = this.file_maybe[file];
          this.file_route[file] = route;
          this.route_file[route] = file;
        }
      }
    }
  }

  normalize(route) {
    return path
      .normalize("/" + route)
      .replace(/\/$/, "")
      .replace(/^$/, "/");
  }

  split(route) {
    return this.normalize(route).replace(/^\/$/, "").split(path.sep);
  }

  /**
   * return a list of routes matching the given criteria
   */
  ls({
    route,
    depth = undefined,
    mindepth = undefined,
    maxdepth = undefined,
    extension = undefined,
  }) {
    route = this.normalize(route);
    if (extension !== undefined) {
      if (!Array.isArray(extension)) extension = [extension];
      extension = extension.map((e) => (e.startsWith(".") ? e : "." + e));
    }

    let routes = [];
    for (const r in this.route_file) {
      if (
        r.startsWith(route) &&
        (extension === undefined ||
          extension.includes(path.extname(this.route_file[r])))
      ) {
        const d = this.split(r.replace(route, "")).length - 1; // depth
        if (
          (depth === undefined || d === depth) &&
          (mindepth === undefined || mindepth <= d) &&
          (maxdepth === undefined || d <= maxdepth)
        ) {
          routes.push(r);
        }
      }
    }
    return routes.sort();
  }

  /**
   * get metadata for the given route
   */
  async meta({ route, before = [], after = [] }) {
    if (!Array.isArray(before)) before = [before];
    if (!Array.isArray(after)) after = [after];
    route = this.normalize(route);

    let maybes = [];
    // the route file itself
    maybes.push(this.route_maybe[route]);
    // the meta file in all directories up to root
    while (true) {
      maybes.push(this.file_maybe[path.join(route, "_meta.js")]);
      if (route === "/") break;
      else route = path.dirname(route);
    }
    maybes.filter((e) => e !== undefined);

    // we want to start from root
    maybes = maybes.reverse();

    let metas = [];
    for (const maybe of maybes) {
      try {
        const meta = (await maybe()).meta;
        if (meta) metas.push(meta);
      } catch (e) {}
    }

    let meta = new Meta();
    for (const m of [...before, ...metas, ...after]) {
      if (typeof m === "function") {
        meta = await m(meta);
        if (!(meta instanceof Meta)) meta = new Meta(meta);
      } else meta.update(m);
    }

    return meta;
  }
}

export const ls = new LS();
