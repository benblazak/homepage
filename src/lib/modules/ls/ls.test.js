import { describe, test, expect } from "vitest";
import { LS } from "./ls.js";

const ls = new LS({
  root: "./ls.test",
  maybes: import.meta.glob(["./ls.test/**/*.(js|md|svelte)"]),
});

test("normalize", () => {
  expect(ls.normalize("a")).toBe("/a");
  expect(ls.normalize("/a/")).toBe("/a");
  expect(ls.normalize("")).toBe("/");
});

test("split", () => {
  expect(ls.split("/a/b")).toEqual(["", "a", "b"]);
  expect(ls.split("/a")).toEqual(["", "a"]);
  expect(ls.split("/")).toEqual([""]);
});

test("ls", () => {
  expect(ls.ls({ route: "/six" })).toEqual([
    "/six/6",
    "/six/60",
    "/six/one/1",
    "/six/one/10",
    "/six/one/two/2",
    "/six/one/two/20",
  ]);
  expect(ls.ls({ route: "/six", depth: 1 })).toEqual(["/six/6", "/six/60"]);
  expect(ls.ls({ route: "/six", mindepth: 2 })).toEqual([
    "/six/one/1",
    "/six/one/10",
    "/six/one/two/2",
    "/six/one/two/20",
  ]);
  expect(ls.ls({ route: "/six", maxdepth: 2 })).toEqual([
    "/six/6",
    "/six/60",
    "/six/one/1",
    "/six/one/10",
  ]);
  expect(ls.ls({ route: "/six", depth: 1, mindepth: 2, maxdepth: 3 })).toEqual(
    []
  );
  expect(ls.ls({ route: "/six", extension: "js" })).toEqual([
    "/six/6",
    "/six/one/1",
    "/six/one/two/2",
  ]);
  expect(ls.ls({ route: "/six", extension: ".js" })).toEqual([
    "/six/6",
    "/six/one/1",
    "/six/one/two/2",
  ]);

  // layout groups
  // https://kit.svelte.dev/docs/advanced-routing#advanced-layouts-group
  expect(ls.ls({ route: "/seven" })).toEqual(["/seven/two", "/seven/two/four"]);
});

describe("meta", () => {
  const get = async (obj) => (await ls.meta(obj)).toJSONCopy();
  const getroute = async (route) => get({ route });

  test("before, after", async () => {
    expect(
      await get({
        route: "/two/leaf",
        before: { subtitle: "before" },
        after: { description: "after" },
      })
    ).toEqual({
      title: "leaf",
      subtitle: "before",
      description: "after",
    });

    expect(
      await get({
        route: "/two/leaf",
        before: { title: "before" },
      })
    ).toEqual({
      title: "leaf",
    });

    expect(
      await get({
        route: "/two/leaf",
        before: { title: "before" },
        after: { title: "after" },
      })
    ).toEqual({
      title: "after",
    });
  });

  test("all metas are found", async () => {
    expect(await getroute("/one/two/leaf")).toEqual({
      title: "root",
      description: "one",
      subtitle: "two",
      abstract: "leaf",
    });
  });

  test("it's okay if one isn't found", async () => {
    // also that lower metas override higher ones
    expect(await getroute("/two/leaf")).toEqual({
      title: "leaf",
    });
  });

  test("route can end with a slash", async () => {
    // and have various slashes
    expect(await getroute("/two")).toEqual({
      title: "root",
    });
    expect(await getroute("/two/")).toEqual({
      title: "root",
    });
    expect(await getroute("two")).toEqual({
      title: "root",
    });
    expect(await getroute("two/")).toEqual({
      title: "root",
    });
  });

  test("functions", async () => {
    // can be async
    expect(await getroute("/three/leaf")).toEqual({
      title: "leaf",
      description: "three",
    });
    // can return non-meta
    expect(await getroute("/four/leaf")).toEqual({
      title: "not a meta",
    });
  });
});
