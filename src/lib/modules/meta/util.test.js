import { test, expect } from "vitest";
import { search, searchfnit } from "./util.js";

test("search", () => {
  expect(search("a", { a: 1, b: 2 })).toBe(1);
  expect(
    search("a", [
      { a: undefined, b: 2 },
      [
        { b: 2 },
        [
          { a: 1, b: 2 },
          { a: 2, b: 2 },
        ],
      ],
    ])
  ).toBe(1);
  expect(search("a", { a: undefined, b: 2 })).toBe(undefined);
});

test("search all", () => {
  expect([
    ...searchfnit(
      (obj) => obj["a"],
      [
        { a: 1, b: 1 },
        [
          { a: 2, b: 2 },
          [
            { a: 3, b: 3 },
            { a: 4, b: 4 },
          ],
        ],
      ]
    ),
  ]).toEqual([1, 2, 3, 4]);
});
