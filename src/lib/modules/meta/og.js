import { Base } from "./base.js";
import * as JSONLD from "./jsonld.js";

/**
 * OpenGraph data
 *
 * references
 * - about
 *   - https://ahrefs.com/blog/open-graph-meta-tags/
 * - test
 *   - https://socialsharepreview.com
 *   - https://cards-dev.twitter.com/validator
 */
export class OG extends Base {
  /** @type {string} */
  title;
  /** @type {string} */
  description;

  /** @type {URL | string} */
  url;
  /**
   * absolute URL
   * @type {URL | string}
   */
  image;

  constructor(copy) {
    super();

    Object.defineProperties(this, {
      type: {
        /** @type {string} */
        get: () =>
          this._type ??
          (this._outer?.jsonld instanceof JSONLD.Article
            ? "article"
            : "website"),
        set: (value) => {
          this._type = value;
        },
        enumerable: true,
      },
    });

    this.update(copy);
  }

  toJSONCopy() {
    let out = super.toJSONCopy();
    out.type = this._type;
    if (out.type === undefined) delete out.type;
    return out;
  }

  _asHTML() {
    const obj = this.toJSON();
    let lines = [];
    for (const key in obj) {
      if (obj[key])
        lines.push(
          `<meta property="og:${key}" content=${JSON.stringify(obj[key])} />`
        );
    }
    return lines.join("\n");
  }
}
