import { Data } from "./data.js";
import { Tag } from "./tag.js";
import { OG } from "./og.js";
import * as JSONLD from "./jsonld.js";

export class MetaBase extends Data {
  /** properties defined with defineProperty */
  _inner = [];

  init(copy) {
    this.update(copy);
    for (const key of this._inner) if (this[key] === undefined) this[key] = {};
  }

  /**
   * @param {string} prop
   * @param {Base} type
   * @param {Object} opt - options
   * @param {boolean} opt.html
   * @param {any} descriptor - property descriptor
   */
  defineProperty(prop, type, opt = {}, descriptor = {}) {
    opt = { html: false, ...opt };

    Object.defineProperty(this, prop, {
      get: () => this["_" + prop],
      set: (value) => {
        this["_" + prop] = value
          ? new type(value).setOuter(this).addSearch(this)
          : value;
      },
      enumerable: true,
      ...descriptor,
    });
    this._inner.push(prop);

    if (opt.html)
      Object.defineProperty(this, prop + "HTML", {
        get: () => this[prop]?._asHTML?.(),
        enumerable: true,
        ...descriptor,
      });
  }

  toJSONCopy() {
    let out = super.toJSONCopy();
    for (const key of this._inner)
      if (out[key] && Object.keys(out[key]).length === 0) delete out[key];
    return out;
  }
}

export class MetaDisplay extends MetaBase {
  constructor(copy) {
    super();

    this.defineProperty("page", Data);
    this.defineProperty("card", Data);

    this.init(copy);
  }
}

export class MetaMeta extends MetaBase {
  /** whether to update jsonld when `type` changes */
  _jsonld_auto = true;

  _type_set(info) {
    if (
      this._jsonld_auto &&
      (this.jsonld === undefined ||
        this.jsonld?.["@type"] !== this.jsonld?.constructor.name)
    ) {
      this.jsonld = this.jsonld?.toJSONCopy() ?? {};
    }
    super._type_set(info);
  }

  constructor(copy) {
    super();

    this.defineProperty("tag", Tag, { html: true });
    this.defineProperty("og", OG, { html: true });
    this.defineProperty(
      "jsonld",
      null,
      { html: true },
      {
        set: (value) => {
          if (!value) {
            this._jsonld = value;
            this._jsonld_auto = false;
            return;
          }
          this._jsonld_auto = true;
          const type = value?.["@type"] ?? this.type;
          if (!type) {
            this._jsonld = undefined;
            return;
          }
          if (!(type in JSONLD))
            throw new Error(`unknown json-ld type: ${type}`);
          const Type = JSONLD[type];
          this._jsonld = new Type(value).setOuter(this).addSearchMeta(this);
        },
      }
    );

    this.init(copy);
  }
}

/**
 * collection of metadata
 *
 * the flow of defaults is
 * ```text
 * this
 * |- display
 *    |- page
 * |  '- card
 * '- meta
 *    |- tag
 *    |- og
 *    '- jsonld
 * ```
 * so, for example, a property not set in `tag` will be searched for in `meta`
 * and then `this`
 */
export class Meta extends MetaBase {
  _type_set(info) {
    if (info.on === this) this.meta?._type_set(info);
    super._type_set(info);
  }

  constructor(copy) {
    super();

    this.defineProperty("display", MetaDisplay);
    this.defineProperty("meta", MetaMeta);

    this.init(copy);
  }
}
