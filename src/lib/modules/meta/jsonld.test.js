import { test, expect } from "vitest";
import * as JSONLD from "./jsonld.js";
import { Meta } from "./meta.js";

test("metas", () => {
  let ld = new JSONLD.CreativeWork().addSearchMeta(
    new Meta({
      type: "CreativeWork",
      title: "title",
      abstract: "abstract",
      keywords: ["one", "two"],
      date: "2022-01-01",
      updated: "2022-01-02",
      license: "license",
      author: [{ name: "name" }],
    })
  );
  expect(JSON.parse(JSON.stringify(ld.withContext()))).toEqual({
    "@context": "https://schema.org",
    "@type": "CreativeWork",
    headline: "title",
    abstract: "abstract",
    keywords: "one, two",
    datePublished: "2022-01-01",
    dateModified: "2022-01-02",
    author: {
      "@type": "Person",
      name: "name",
    },
    copyrightNotice: "Copyright © 2022 name",
    license: "license",
  });
});

test("author", () => {
  let ld;

  ld = new JSONLD.CreativeWork().addSearchMeta({ author: [] });
  expect(JSON.parse(JSON.stringify(ld.withContext()))).toEqual({
    "@context": "https://schema.org",
    "@type": "CreativeWork",
  });

  ld = new JSONLD.CreativeWork().addSearchMeta({ author: [{ name: "bob" }] });
  expect(JSON.parse(JSON.stringify(ld.withContext()))).toEqual({
    "@context": "https://schema.org",
    "@type": "CreativeWork",
    author: {
      "@type": "Person",
      name: "bob",
    },
  });

  ld = new JSONLD.CreativeWork().addSearchMeta({
    author: [
      { name: "bob" },
      { name: "alice", url: "example.com", email: "alice@example.com" },
    ],
  });
  expect(JSON.parse(JSON.stringify(ld.withContext()))).toEqual({
    "@context": "https://schema.org",
    "@type": "CreativeWork",
    author: [
      {
        "@type": "Person",
        name: "bob",
      },
      {
        "@type": "Person",
        name: "alice",
        url: "example.com",
        email: "alice@example.com",
      },
    ],
  });
});

test("html", () => {
  let ld = new JSONLD.Thing({ name: "name" });
  expect(ld._asHTML()).toBe(
    [
      `<script type="application/ld+json">`,
      `{`,
      `  "@type": "Thing",`,
      `  "@context": "https://schema.org",`,
      `  "name": "name"`,
      `}`,
      `</script>`,
    ].join("\n")
  );
});
