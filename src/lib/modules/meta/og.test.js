import { test, expect } from "vitest";
import { OG } from "./og.js";
import { Meta } from "./meta.js";

test("type", () => {
  let og = new OG({});
  expect(og.type).toBe("website");
  og.type = "extratype";
  expect(og.type).toBe("extratype");
  og.type = undefined;
  expect(og.type).toBe("website");

  // make sure we're not triggering a call to _type_set
  // (or at least that it's not doing anything it shouldn't)
  let meta = new Meta();
  meta.meta.og.type = "extratype";
  expect(meta.meta.og.type).toBe("extratype");
  expect(meta.meta.jsonld).toBeUndefined();
  meta.meta.og.type = undefined;
  expect(meta.meta.og.type).toBe("website");
  meta.type = "BlogPosting";
  expect(meta.meta.og.type).toBe("article");
  meta.meta.jsonld = undefined;
  expect(meta.meta.og.type).toBe("website");
});

test("to json copy", () => {
  let og = new OG();
  expect(og.toJSONCopy()).toEqual({});
  og.type = "og type";
  expect(og.toJSONCopy()).toEqual({ type: "og type" });
});

test("html", () => {
  let og = new OG({ title: "title" });
  let lines = og._asHTML().split("\n");
  expect(lines.length).toBe(2);
  expect(new Set(lines)).toEqual(
    new Set([
      `<meta property="og:type" content="website" />`,
      `<meta property="og:title" content="title" />`,
    ])
  );
});
