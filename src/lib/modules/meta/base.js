import { isWritable, searchif } from "./util";

/**
 * Base class for all data classes
 */
export class Base {
  _outer;
  _search = [];

  _type_set(info) {
    this._outer?._type_set?.(info);
  }

  constructor(copy) {
    this.update(copy);
    return new Proxy(this, {
      set: (target, prop, value) => {
        target[prop] = value;
        if (["type", "@type"].includes(prop))
          target._type_set({ value, on: target });
        return true;
      },
      get: (target, prop) => {
        const value = searchif(prop, target, target._search);
        if (value !== undefined) return value;
        if (prop === "_target") return target;
      },
    });
  }

  update(copy) {
    if (copy instanceof Base) copy = copy.toJSONCopy();
    for (const key in copy) {
      if (!(key in this)) continue;
      if (key.startsWith("_")) continue;
      if (copy[key] === undefined) continue;
      if (!isWritable(key, this)) continue;
      if (typeof this[key]?.update === "function") this[key].update(copy[key]);
      else this[key] = copy[key];
    }
    return this;
  }

  setOuter(outer) {
    this._outer = outer;
    return this;
  }

  addSearch(_search) {
    this._search.push(_search);
    return this;
  }

  toJSON(opt = {}) {
    const { ignore = [] } = opt;
    let out = {};
    for (const key in this) {
      if (key.startsWith("_")) continue;
      if (ignore.includes(key)) continue;
      if (this[key] === null) continue;

      const value = this[key];
      if (value?.toJSON) out[key] = value.toJSON();
      else out[key] = this[key];
    }
    return out;
  }

  /**
   * only the properties we might want to copy
   */
  toJSONCopy(opt = {}) {
    const { ignore = [] } = opt;
    const target = this._target;

    let out = {};
    for (const key in target) {
      if (key.startsWith("_")) continue;
      if (ignore.includes(key)) continue;
      if (!isWritable(key, target)) continue;
      if (target[key] === null) continue;
      if (target[key] === undefined) continue;

      const value = target[key];
      if (value?.toJSONCopy) out[key] = value.toJSONCopy();
      else if (value?.toJSON) out[key] = value.toJSON();
      else out[key] = value;
    }
    return out;
  }
}
