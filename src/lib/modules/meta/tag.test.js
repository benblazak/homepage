import { test, expect } from "vitest";
import { Tag } from "./tag.js";

test("html", () => {
  let lines = [];

  let tag = new Tag({ title: "title" });
  lines.push(`<title>title</title>`);

  expect(tag._asHTML()).toBe(lines.join("\n"));

  tag.description = "description";
  lines.push(`<meta name="description" content="description" />`);

  tag.url = "https://www.google.com";
  lines.push(`<link rel="canonical" href="https://www.google.com" />`);

  expect(tag._asHTML()).toBe(lines.join("\n"));
});
