import { Base } from "./base.js";

/**
 * HTML tag data
 */
export class Tag extends Base {
  /** @type {string} */
  title;
  /** @type {string} */
  description;

  /**
   * canonical URL
   * - https://developers.google.com/search/docs/advanced/crawling/consolidate-duplicate-urls
   * @type {URL | string}
   */
  url;

  constructor(copy) {
    super().update(copy);
  }

  _asHTML() {
    let lines = [];

    if (this.title) lines.push(`<title>${this.title}</title>`);
    if (this.description)
      lines.push(
        `<meta name="description" content=${JSON.stringify(
          this.description
        )} />`
      );

    if (this.url)
      lines.push(`<link rel="canonical" href=${JSON.stringify(this.url)} />`);

    return lines.join("\n");
  }
}
