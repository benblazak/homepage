import { describe, test, expect } from "vitest";
import { Data } from "./data.js";

const json = (e) => JSON.parse(JSON.stringify(e));

test("ignore non-writable", () => {
  const d = new Data();
  d.update({ authorText: "text" });
  expect(json(d)).toEqual({});
});

describe("author", () => {
  let d = new Data();

  test("update", () => {
    d.author = undefined;
    d.update({ author: "alice" });
    d.update({ author: ["alice", "bob"] });
    d.update({ author: { name: "eve", url: "https://www.example.com/eve" } });
    d.update({
      author: [
        { name: "bob", email: "bob@example.com" },
        { name: "eve", email: "eve@example.com" },
      ],
    });
    expect(json(d.author)).toEqual([
      { name: "alice" },
      { name: "bob", email: "bob@example.com" },
      {
        name: "eve",
        email: "eve@example.com",
        url: "https://www.example.com/eve",
      },
    ]);
  });

  test("set, get, clear", () => {
    d.author = "alice";
    expect(json(d.author)).toEqual([{ name: "alice" }]);
    d.author = ["alice", "bob"];
    expect(json(d.author)).toEqual([{ name: "alice" }, { name: "bob" }]);
    d.author = { name: "alice" };
    expect(json(d.author)).toEqual([{ name: "alice" }]);
    d.author = [{ name: "alice" }, { name: "bob" }];
    expect(json(d.author)).toEqual([{ name: "alice" }, { name: "bob" }]);
    d.author = undefined;
    expect(d.author).toBeUndefined();
  });
});

describe("history", () => {
  let d = new Data();

  test("update", () => {
    d.history = [{ date: "2020-01-01", description: "one" }];
    d.update({ history: [{ date: "2020-01-02", description: "two" }] });
    expect(json(d.history)).toEqual([
      { date: "2020-01-01", description: "one" },
      { date: "2020-01-02", description: "two" },
    ]);
  });

  test("set, get, clear", () => {
    d.history = { date: "2020-01-01", description: "one" };
    expect(json(d.history)).toEqual([
      { date: "2020-01-01", description: "one" },
    ]);
    d.history = [
      { date: "2020-01-01", description: "one" },
      { date: "2020-01-02", description: "two" },
    ];
    expect(json(d.history)).toEqual([
      { date: "2020-01-01", description: "one" },
      { date: "2020-01-02", description: "two" },
    ]);
    d.history = undefined;
    expect(d.history).toBeUndefined();
  });
});

describe("keywords", () => {
  let d = new Data();

  test("update", () => {
    d.keywords = "one, two";
    expect(d.keywords).toEqual(["one", "two"]);
    d.keywords.update("two, three");
    expect(d.keywords).toEqual(["one", "two", "three"]);
  });

  test("set, get, clear", () => {
    d.keywords = "one  ,  two,three";
    expect(d.keywords).toEqual(["one", "two", "three"]);
    d.keywords = ["four", "five"];
    expect(d.keywords).toEqual(["four", "five"]);
    d.keywords = new Set(["six", "seven"]);
    expect(d.keywords).toEqual(["six", "seven"]);
    d.keywords = undefined;
    expect(d.keywords).toBeUndefined();
  });
});
