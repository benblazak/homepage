/**
 * references
 * - json-ld
 *   - https://schema.org
 *   - https://schema.org/BlogPosting
 * - github
 *   - interfaces https://github.com/google/schema-dts
 * - google
 *   - overview https://developers.google.com/search/docs/advanced/structured-data/intro-structured-data
 *   - example https://developers.google.com/search/docs/advanced/appearance/publication-dates
 *   - test https://search.google.com/test/rich-results
 */

import { searchfn } from "./util.js";
import { Base } from "./base.js";

export class Thing extends Base {
  "@type";
  "@context";

  withType() {
    return new Proxy(this, {
      get: (target, prop) => {
        const value = target[prop];
        if (value !== undefined) return value;
        if (prop === "@type") return target.constructor.name;
      },
    });
  }

  withContext() {
    return new Proxy(this, {
      get: (target, prop) => {
        const value = target[prop];
        if (value !== undefined) return value;
        if (prop === "@context") return "https://schema.org";
      },
    });
  }

  /** @type {string} */
  name;
  /** @type {URL | string} */
  url;
  /** @type {URL | string} */
  image;
  /** @type {string} */
  description;

  constructor(copy) {
    super().update(copy);
  }

  getMeta(prop, meta) {
    if (prop === "@type") return meta.type;
    if (prop in this) return meta[prop];
  }

  addSearchMeta(_search_meta) {
    return this.addSearch(
      new Proxy(this, {
        get: (target, prop) => {
          if (prop in target)
            return searchfn((obj) => target.getMeta(prop, obj), _search_meta);
        },
      })
    );
  }

  toJSON(opt) {
    return super.toJSON.bind(this.withType())(opt);
  }

  _asHTML() {
    let lines = [];
    lines.push(
      `<script type="application/ld+json">`,
      JSON.stringify(this.withContext(), null, 2),
      `</script>`
    );
    return lines.join("\n");
  }
}

export class CreativeWork extends Thing {
  /** @type {string} */
  headline;
  /** @type {string} */
  abstract;
  /** @type {string} */
  keywords;

  /** @type {string} */
  datePublished;
  /** @type {string} */
  dateModified;

  /** @type {Person | Person[]} */
  author;

  /** @type {string} */
  copyrightNotice;
  /** @type {URL | string} */
  license;

  constructor(copy) {
    super().update(copy);
  }

  getMeta(prop, meta) {
    switch (prop) {
      case "headline":
        return meta.title;
      case "abstract":
        return meta.abstract;
      case "keywords":
        return meta.keywords?.join(", ");
      case "datePublished":
        return meta.date;
      case "dateModified":
        return meta.updated;

      case "author": {
        const value = meta.author?.map((a) => new Person(a));
        if (value === undefined || value.length === 0) return;
        else if (value.length === 1) return value[0];
        return value;
      }

      case "copyrightNotice":
        return meta.copyrightText;
      case "license":
        return meta.license;
      default:
        return super.getMeta(prop, meta);
    }
  }
}

export class Article extends CreativeWork {}
export class SocialMediaPosting extends Article {}
export class BlogPosting extends SocialMediaPosting {}

export class Blog extends CreativeWork {}

export class WebPage extends CreativeWork {}

export class WebSite extends CreativeWork {}

export class Person extends Thing {
  /** @type {string} */
  email;

  constructor(copy) {
    super().update(copy);
  }
}
