/**
 * utility functions
 *
 * references
 * - https://javascript.info/proxy
 */

/**
 * Test whether `obj` is iterable
 */
export const isIterable = (obj) => typeof obj?.[Symbol.iterator] === "function";

/**
 * Test whether `prop` is writable
 */
export function isWritable(prop, obj) {
  while (obj) {
    const descriptor = Object.getOwnPropertyDescriptor(obj, prop);
    if (descriptor !== undefined)
      return descriptor.writable ?? descriptor.set !== undefined;
    obj = Object.getPrototypeOf(obj);
  }
  return false;
}

/**
 * search function iterator: yield `fn(obj)` if the value is not-undefined,
 * searching recursively if `obj` is iterable
 *
 * - any value is valid for `obj`, including `undefined`
 * - if `obj` is a string, we treat it as non-iterable (otherwise we get a stack
 *   overflow)
 */
export function* searchfnit(fn, obj) {
  if (isIterable(obj) && typeof obj !== "string")
    for (const o of obj) yield* searchfnit(fn, o);
  else {
    const value = fn(obj);
    if (value !== undefined) yield value;
  }
}

/**
 * Search for a not-undefined value of `fn(obj)` in `objs`
 */
export function searchfn(fn, obj) {
  const { value, done } = searchfnit(fn, obj).next();
  if (!done) return value;
}

/**
 * Search for `key` in each of `objs`
 */
export function search(key, obj) {
  return searchfn((obj) => obj?.[key], obj);
}

/**
 * Search for `key` in each of `objs`, only if `key in obj`
 */
export function searchif(key, obj, objs) {
  if (key in obj) {
    return search(key, [obj, objs]);
  }
}
