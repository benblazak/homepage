import { Base } from "./base.js";

export class DataAuthorElement extends Base {
  /** @type {string} */
  name;
  /** @type {string} */
  email;
  /** @type {URL | string} */
  url;

  constructor(copy) {
    super().update(copy);
  }
}

export class DataAuthor extends Array {
  updatePush(copy) {
    const name = copy?.name;
    for (const a of this)
      if (a.name === name) {
        a.update(copy);
        return this.length;
      }
    return this.push(copy);
  }
  update(copy) {
    if (typeof copy === "string") this.update({ name: copy });
    else if (Array.isArray(copy)) for (const c of copy) this.update(c);
    else if (typeof copy === "object")
      this.updatePush(new DataAuthorElement(copy));
    return this;
  }
}

export class DataHistoryElement extends Base {
  /** @type {string} */
  date;
  /** @type {string} */
  description;

  constructor(copy) {
    super().update(copy);
  }
}

export class DataHistory extends Array {
  update(copy) {
    if (Array.isArray(copy)) for (const c of copy) this.update(c);
    else if (typeof copy === "object") this.push(new DataHistoryElement(copy));
    return this;
  }
}

export class DataKeywords extends Array {
  add(c) {
    if (!this.includes(c)) this.push(c);
  }
  update(copy) {
    if (typeof copy === "string") copy = copy.split(/\s*,\s*/);
    for (const c of copy) this.add(c);
    return this;
  }
}

/**
 * display data
 */
export class Data extends Base {
  /** @type {string} */
  type;

  /** @type {string} */
  title;
  /** @type {string} */
  description;

  /** @type {string} */
  subtitle;
  /** @type {string} */
  abstract;

  /** @type {string} */
  date;
  /** @type {string} */
  updated;

  /** @type {URL | string} */
  url;
  /** @type {URL | string} */
  image;
  /** @type {URL | string} */
  license;

  /** @type {boolean} */
  debug;
  /** @type {boolean} */
  monetize;

  get authorText() {
    return this.author?.map((a) => a.name).join(", ") || undefined;
  }
  get copyrightYear() {
    const date = this.date?.slice(0, 4);
    const updated = this.updated?.slice(0, 4);
    return date
      ? date + (updated && updated !== date ? "-" + updated : "")
      : undefined;
  }
  get copyrightText() {
    const year = this.copyrightYear;
    const author = this.authorText;
    return year && author ? `Copyright © ${year} ${author}` : undefined;
  }

  constructor(copy) {
    super();

    Object.defineProperties(this, {
      author: {
        /** @type {DataAuthor} */
        get: () => this._author,
        set: (value) => {
          if (!value) this._author = value;
          else this._author = new DataAuthor().update(value);
        },
        enumerable: true,
      },

      history: {
        /** @type {DataHistory} */
        get: () => this._history,
        set: (value) => {
          if (!value) this._history = value;
          else this._history = new DataHistory().update(value);
        },
        enumerable: true,
      },

      keywords: {
        /** @type {DataKeywords} */
        get: () => this._keywords,
        set: (value) => {
          if (!value) this._keywords = value;
          else this._keywords = new DataKeywords().update(value);
        },
        enumerable: true,
      },
    });

    this.update(copy);
    return new Proxy(this, {
      get(target, prop, receiver) {
        const value = target[prop];
        if (value !== undefined) return value;
        if (prop === "date") return receiver.history?.at(0)?.date;
        if (prop === "updated") {
          const value = receiver.history?.at(-1)?.date;
          if (value?.localeCompare?.(receiver.date) > 0) return value;
        }
      },
    });
  }
}
