import { describe, test, expect } from "vitest";
import { Meta } from "./meta.js";
import { Data } from "./data.js";
import { Tag } from "./tag.js";
import { OG } from "./og.js";

test("errors", () => {
  let meta = new Meta();
  expect(() => (meta.type = "extratype")).toThrow(
    "unknown json-ld type: extratype"
  );
});

describe("base", () => {
  test("copy", () => {
    // sub things should be updated (not overwritten)
    let m = new Meta({ meta: { tag: { title: "one" }, og: { title: "two" } } });
    expect(m.meta.tag.title).toBe("one");
    expect(m.meta.og.title).toBe("two");

    // defaults should not be copied
    m = new Meta({ meta: { title: "title" } });
    expect(m.meta.title).toBe("title");
    expect(m.meta.tag.title).toBe("title");
    expect(m.meta.og.title).toBe("title");
    let n = new Meta(m);
    n.meta.title = undefined;
    expect(n.meta.title).toBeUndefined();
    expect(n.meta.tag.title).toBeUndefined();
    expect(n.meta.og.title).toBeUndefined();

    // we don't want to copy undefined values
    m = new Meta({ title: "one" });
    n = new Meta({ title: "two", description: "three" }).update(m);
    expect(n.title).toBe("one");
    expect(n.description).toBe("three");
  });

  test("set", () => {
    let m = new Meta().update({ meta: { title: "one" } });
    expect(m.meta.title).toBe("one");
    expect(m.meta.og.title).toBe("one");
    m.meta = { title: "two" };
    expect(m.meta.title).toBe("two");
    expect(m.meta.og.title).toBe("two");
    m.meta = { title: "three" };
    expect(m.meta.title).toBe("three");
    expect(m.meta.og.title).toBe("three");
  });

  test("update", () => {
    let meta = new Meta({ meta: { description: "description one" } }).update({
      meta: { title: "title", description: "description" },
    });
    expect(meta.meta.tag.title).toBe("title");
    expect(meta.meta.tag.description).toBe("description");
    expect(meta.meta.og.title).toBe("title");
    expect(meta.meta.og.description).toBe("description");

    meta.type = "CreativeWork";
    expect(meta.meta.jsonld.headline).toBe("title");
    expect(meta.meta.jsonld.description).toBe("description");
  });
});

describe("meta base", () => {
  test("clear", () => {
    let meta = new Meta();

    // initially set
    expect(meta.meta).toBeInstanceOf(Data);
    expect(meta.meta.tag).toBeInstanceOf(Tag);
    expect(meta.meta.og).toBeInstanceOf(OG);

    // and data flows down
    meta.meta.title = "title";
    expect(meta.meta.tag.title).toBe("title");
    expect(meta.meta.og.title).toBe("title");

    // but can be cleared
    meta.meta = undefined;
    expect(meta.meta).toBeUndefined();

    // can be set again
    meta.meta = {};
    expect(meta.meta).toBeInstanceOf(Data);

    // and these can be cleared and set again too
    meta.meta.tag = undefined;
    expect(meta.meta.tag).toBeUndefined();
    meta.meta.tag = {};
    expect(meta.meta.tag).toBeInstanceOf(Tag);
    //
    meta.meta.og = undefined;
    expect(meta.meta.og).toBeUndefined();
    meta.meta.og = {};
    expect(meta.meta.og).toBeInstanceOf(OG);
  });
});

test("date", () => {
  let meta = new Meta({ type: "CreativeWork" });

  meta.history = [];
  expect(meta.updated).toBe(undefined);

  meta.history = [{ date: "2022-01-01", description: "test 1" }];
  expect(meta.date).toBe("2022-01-01");
  expect(meta.updated).toBe(undefined);

  meta.history = [
    { date: "2022-01-01", description: "test 1" },
    { date: "2022-01-02", description: "test 2" },
  ];
  expect(meta.date).toBe("2022-01-01");
  expect(meta.updated).toBe("2022-01-02");
});

test("jsonld", () => {
  let meta = new Meta();
  const type = (ld) => ld?.withType()["@type"];

  // undefined till there's a type
  expect(meta.meta.jsonld).toBeUndefined();
  meta.meta.jsonld = {};
  expect(meta.meta.jsonld).toBeUndefined();

  // updating with the type
  meta.type = "CreativeWork";
  expect(type(meta.meta.jsonld)).toBe("CreativeWork");
  meta.meta.type = "Article";
  expect(type(meta.meta.jsonld)).toBe("Article");
  meta.meta.jsonld = {};
  expect(type(meta.meta.jsonld)).toBe("Article");

  // undefined if set that way
  meta.meta.jsonld = undefined;
  expect(meta.meta.jsonld).toBeUndefined();
  meta.meta.type = "BlogPosting";
  expect(meta.meta.jsonld).toBeUndefined();

  // till it's set again
  meta.meta.jsonld = {};
  expect(type(meta.meta.jsonld)).toBe("BlogPosting");
  meta.meta.type = undefined;
  expect(type(meta.meta.jsonld)).toBe("CreativeWork");

  // and then undefined again if there is no type
  meta.type = undefined;
  expect(meta.meta.jsonld).toBeUndefined();

  // values should be copied on update
  meta.type = "CreativeWork";
  expect(type(meta.meta.jsonld)).toBe("CreativeWork");
  meta.meta.jsonld.headline = "headline";
  expect(meta.meta.jsonld.headline).toBe("headline");
  meta.meta.jsonld["@type"] = "Article";
  expect(type(meta.meta.jsonld)).toBe("Article");
  expect(meta.meta.jsonld.headline).toBe("headline");
});

test("json", () => {
  let meta = new Meta();
  meta.meta.title = "title";
  meta.meta.date = "2022-01-01";
  meta.meta.tag.title = "tag title";

  let obj = JSON.parse(JSON.stringify(meta.meta));
  expect(obj).toHaveProperty("tagHTML");
  expect(obj).toHaveProperty("ogHTML");
  delete obj.tagHTML;
  delete obj.ogHTML;
  expect(obj).toEqual({
    title: "title",
    date: "2022-01-01",
    tag: {
      title: "tag title",
    },
    og: {
      title: "title",
      type: "website",
    },
  });

  expect(meta.meta.toJSONCopy()).toEqual({
    title: "title",
    date: "2022-01-01",
    tag: {
      title: "tag title",
    },
  });
});
