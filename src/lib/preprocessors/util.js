import fs from "fs";
import path from "path";
import { fileURLToPath } from "url";
import { execSync } from "child_process";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

/**
 * preprocessor wrappers
 */

export function fileType(extensions, preprocessor) {
  let wrapped = {};
  for (const key in preprocessor) {
    wrapped[key] = (obj) =>
      extensions.includes(path.extname(obj.filename))
        ? preprocessor[key](obj)
        : undefined;
  }
  return wrapped;
}

/**
 * preprocessors
 *
 * references
 * - good example of a svelte preprocessor
 *   - https://github.com/reed-jones/svelte-markup
 * - official
 *   - https://svelte.dev/docs#compile-time-svelte-preprocess
 *   - https://github.com/sveltejs/svelte-preprocess
 * - possible changes
 *   - https://github.com/sveltejs/rfcs/pull/56
 * - javascript
 *   - https://nodejs.org/api/child_process.html#child_processexecsynccommand-options
 */

export function makefile() {
  return {
    markup({ filename }) {
      const dir = path.dirname(filename);

      const vars = [
        ["PATH_ROOT", path.resolve("./")],
        ["PATH_LIB", path.resolve("./src/lib")],
        ["PATH_ROUTES", path.resolve("./src/routes")],
      ]
        .map(([key, value]) => `${key}="${value}"`)
        .join(" ");

      // run make if makefile is found
      if (fs.existsSync(path.join(dir, "makefile")))
        console.log(execSync(`cd '${dir}'; make ${vars}`).toString());
    },
  };
}

export function markdown({ standalone = false, json = false } = {}) {
  if (standalone && json)
    throw new Error("can only have one of standalone or json");

  return {
    markup({ content, filename }) {
      const dir = path.dirname(filename);

      // search for metadata file
      let meta;
      for (const p of [
        path.join(dir, "_meta.yaml"),
        path.join(dir, "..", "_meta.yaml"),
      ]) {
        if (fs.existsSync(p)) {
          meta = p;
          break;
        }
      }

      // run pandoc
      // - math is handled in the lua writer
      return {
        code: execSync(
          `pandoc \
          --toc \
          --wrap none \
          --metadata private_path_root="${path.resolve("./")}" \
          --metadata private_path_lib="${path.resolve("./src/lib")}" \
          --metadata private_path_routes="${path.resolve("./src/routes")}" \
          --metadata private_filename="${filename}" \
          --metadata private_dirname="${path.dirname(filename)}" \
          ${meta ? `--metadata-file '${meta}'` : ""} \
          --to '${__dirname}/pandoc/svelte.lua' \
          ${
            standalone ? `--template '${__dirname}/pandoc/svelte.template'` : ""
          } \
          ${json ? `--template '${__dirname}/pandoc/json.template'` : ""} \
          `,
          { input: content }
        ).toString(),
      };
    },
  };
}
