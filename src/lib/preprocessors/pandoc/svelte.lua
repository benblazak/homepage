-- pandoc writer for svelte
-- - usage: pandoc -t svelte.lua
--
-- - see `pandoc --version` for the versions of things (including lua) that
--   pandoc is using
--
-- notes
-- - the prefix `p_` is used for variables in the input document that pass data
--   to this writer
--   - see `p_temp` fields and the Raw filters
--   - see `p_src', `p_tag`, `p_label` in the Code filters
--   - see `p_width` in the Image filter
-- - the prefix `_p_` is used for things added to the output javascript by this
--   writer
--   - image imports are prefixed with `_p_image_`.  see below for how the name
--     is generated from the path.
--   - things added by the template may not use this convention
-- - `p_temp` is added to the metadata table to hold things meant specifically
--   for the template. other fields may also be added by pandoc.
--
-- references
-- - https://pandoc.org/custom-writers.html#new-style
--   - https://www.lua.org/manual/5.3/
--   - https://pandoc.org/lua-filters.html
-- - https://pandoc.org
--   - https://pandoc.org/MANUAL.html
--   - https://pandoc.org/MANUAL.html#templates

-- ----------------------------------------------------------------------------

local pipe = pandoc.pipe

local script_dir = debug.getinfo(1).source:match('@?(.*/)')
package.path = script_dir .. '?.lua;' .. package.path
local util = require("util")
local escape = util.escape
local attributes = util.attributes

local function _test()
  util._test()
end

-- ----------------------------------------------------------------------------

-- set of images we've already imported, to avoid generating duplicate imports
local images = {}

-- data for the template
--
-- this table will be added to the document metadata, with objects set to nil if
-- empty
--
-- notes
-- - https://pandoc.org/MANUAL.html#extension-yaml_metadata_block
-- - strings in metadata blocks are parsed as markdown, even if they're
--   generated here
local p_temp = {
  image_imports = {},

  -- will be added to the corresponding toplevel svelte blocks
  script = {},
  head = {},
  style = {},
}

-- private data
local private = {
  path = {},
}

-- ----------------------------------------------------------------------------

-- resolve path
--
-- working directory is project root, but we want to be able to specify paths
-- relative to the md file being processed. we also want to be able to specify
-- routes relative to various other things.
--
-- depends on `private` values
local function path_resolve(path)
  if path:match('^/') then
    return private.path.routes .. path
  elseif path:match('^%$') then
    local key = path:match('^%$(.-)/')
    path = path:gsub('^%$(.-)/', '')
    return private.path[key] .. '/' .. path
  else
    return private.dirname .. '/' .. path
  end
end

-- helper for Code filters
local function code(e)
  local opt = {}
  opt.lang = table.remove(e.classes, 1)
  opt.label = e.attributes.p_label or opt.lang
  opt.tag = e.attributes.p_tag or 'code' -- 'samp' or 'kbd' are other options
  e.attributes.p_label = nil
  e.attributes.p_tag = nil

  local file = e.attributes.p_src
  e.attributes.p_src = nil
  if file ~= nil then
    e.text = io.open(path_resolve(file)):read("a")
  end

  local body = ''
  if opt.lang then
    table.insert(e.classes, 1, 'highlight')
    table.insert(e.attributes, { 'data-lang', opt.lang })
    body = escape(pipe('pygmentize', { '-f', 'html', '-O', 'nowrap', '-l', opt.lang }, e.text), 's'):gsub('%s+$', '')
  else
    body = escape(e.text, 'sh')
  end
  if opt.label then
    table.insert(e.attributes, { 'data-label', opt.label })
  end
  -- for some reason, spaces are getting eaten in highlighted inline code.
  -- pygments puts spaces into their own span, and escapes html, so this should
  -- be safe.  it appears to keep following spaces from getting eaten as well.
  body = body:gsub('> ', '>&nbsp;')

  return '<' .. opt.tag .. attributes(e) .. '>' .. body .. '</' .. opt.tag .. '>', opt
end

-- ----------------------------------------------------------------------------

function Writer(doc, opts)
  doc = doc:walk({

    Meta = function(e)
      -- private data
      for k, v in pairs(e) do
        if k:match('^private_path_') and e[k] ~= nil then
          local pk = k:gsub('^private_path_', '')
          private.path[pk] = v
          e[k] = nil
        end
      end
      for k, v in pairs(e) do
        if k:match('^private_') and e[k] ~= nil then
          local pk = k:gsub('^private_', '')
          private[pk] = v
          e[k] = nil
        end
      end

      if e.debug == true then
        _test()
      end

      return e
    end,

  }):walk({

    traverse = 'topdown',

    Blocks = function(e)
      -- helps implement https://pandoc.org/MANUAL.html#extension-implicit_figures
      for i = 1, #e do
        if e[i].tag == 'Para'
            and #e[i].content == 1
            and e[i].content[1].tag == 'Image'
            and #e[i].content[1].caption ~= 0
        then
          e[i] = pandoc.Plain(e[i].content)
        end
      end
      return e
    end,

    RawInline = function(e)
      if e.format:match('^p_') then
        table.insert(p_temp[e.format:gsub('^p_', '')], pandoc.RawInline('html', e.text))
      end
    end,
    RawBlock = function(e)
      if e.format:match('^p_') then
        table.insert(p_temp[e.format:gsub('^p_', '')], pandoc.RawBlock('html', e.text))
      end
    end,

  }):walk({

    Math = function(e)
      local s = pandoc.write(pandoc.Pandoc(e), 'html', { html_math_method = 'mathml' })
      return pandoc.RawInline('html', escape(s, 's'))
    end,

    Code = function(e)
      local html = code(e)
      return pandoc.RawInline('html', html)
    end,
    CodeBlock = function(e)
      local html, opt = code(e)
      local attr = attributes({ attributes = { class = opt.tag, ['data-lang'] = opt.lang, ['data-label'] = opt.label } })
      return pandoc.RawBlock('html', '<div' .. attr .. '><pre' .. attr .. '>' .. html .. '</pre></div>')
    end,

    RawInline = function(e)
      if e.format == 'svelte' then
        return pandoc.RawInline('html', e.text)
      elseif e.format == 'html' then
        return pandoc.RawInline('html', escape(e.text, 's'))
      end
    end,
    RawBlock = function(e)
      if e.format == 'svelte' then
        return pandoc.RawBlock('html', e.text)
      elseif e.format == 'html' then
        return pandoc.RawBlock('html', escape(e.text, 's'))
      end
    end,

    -- https://www.leereamsnyder.com/blog/making-headings-with-links-show-up-in-safari-reader
    Header = function(e)
      e.content = pandoc.Link(pandoc.Span(e.content), '#' .. e.identifier, '', { class = 'heading' })
      return e
    end,

    Link = function(e)
      if e.target:match('://') then
        e.attributes.target = e.attributes.target or '_blank'
      end
      if e.attributes.target == '_blank' then
        e.attributes.rel = e.attributes.rel or 'noreferrer'
      end
      return e
    end,

    Image = function(e)
      -- the reader prefixes the title of figures with `fig:`.  i don't see this
      -- documented, but it is convenient.
      -- https://pandoc.org/MANUAL.html#extension-implicit_figures
      local figure = e.title:match('^fig:')
      e.title = e.title:gsub('^fig:', '')

      if not e.attributes.alt then e.attributes.alt = pandoc.utils.stringify(e.caption) end
      if not e.attributes.title then e.attributes.title = e.title end

      -- use image src to generate the import name
      -- - strip leading `./`
      -- - strip trailing extension
      -- - change `/` to `_`
      -- - strip any remaining special characters
      -- - prefix with `_p_image_`
      local src = '_p_image_' .. e.src:gsub('^%./', ''):gsub('%.[^/]*$', ''):gsub('/', '_'):gsub('[^%w_]', '')
      local image = '<Image meta={' .. src .. '}' .. attributes(e) .. '/>'

      if not images[src] then
        images[src] = true -- we don't want duplicate imports
        local width = e.attributes.p_width and 'preset=' .. e.attributes.p_width .. '&' or ''
        e.attributes.p_width = nil
        local import = 'import ' .. src .. ' from "' .. e.src .. '?' .. width .. 'meta&imagetools";'
        table.insert(p_temp.image_imports, pandoc.RawBlock('html', import))
      end

      if figure then
        return pandoc.List({ pandoc.RawInline('html', '<figure>' .. image .. '<figcaption aria-hidden="true">') }) ..
            e.caption ..
            pandoc.List({ pandoc.RawInline('html', '</figcaption></figure>') })
      end
      return pandoc.RawInline('html', image)
    end,

    Meta = function(e)
      -- template data
      if e.p_temp == nil then e.p_temp = {} end
      --
      for _, k in ipairs({ 'image_imports', 'script', 'head', 'style' }) do
        if #p_temp[k] == 0 then p_temp[k] = nil end
      end
      --
      for k in pairs(p_temp) do
        if e.p_temp[k] == nil then e.p_temp[k] = p_temp[k] end
      end

      return e
    end,

  }) -- end walk

  return pandoc.write(doc, 'html', opts)
end
