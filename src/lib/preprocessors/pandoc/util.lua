-- utility functions
local util = {}

-- ----------------------------------------------------------------------------

-- escape characters
--
-- types is a string controlling what will be escaped
-- - h : html
-- - q : quote
-- - s : svelte
--
-- more than one character may be present. for example, `escape(s,'hq')` to
-- escape html and quote characters in `s`.
function util.escape(s, types)
  local html = types:find('h')
  local quote = types:find('q')
  local svelte = types:find('s')

  -- single character replacements https://dev.w3.org/html5/html-author/charref
  local pat = ''
  local repl = {}
  if html then
    pat = pat .. '<>&'
    repl['<'] = '&lt;'
    repl['>'] = '&gt;'
    repl['&'] = '&amp;'
  end
  if quote then
    pat = pat .. '"\''
    repl['"'] = '&quot;'
    repl["'"] = '&apos;'
  end
  if svelte then
    pat = pat .. '{}'
    repl['{'] = '&lbrace;'
    repl['}'] = '&rbrace;'
  end
  pat = '[' .. pat .. ']'

  return s:gsub(pat, repl)
end

function util._escape_test()
  local s = '<>& "\' {}'
  assert(util.escape(s, 'h') == '&lt;&gt;&amp; "\' {}')
  assert(util.escape(s, 'q') == '<>& &quot;&apos; {}')
  assert(util.escape(s, 's') == '<>& "\' &lbrace;&rbrace;')
  assert(util.escape(s, 'hqs') == '&lt;&gt;&amp; &quot;&apos; &lbrace;&rbrace;')
end

-- take an object and return an html attribute string
--
-- included if present
-- - identifier
-- - classes
-- - attributes
--
-- preceded by a space if non-empty
function util.attributes(attr)
  local a = ''
  if attr.attributes then
    for k, v in pairs(attr.attributes) do
      a = a .. ' ' .. k .. '="' .. util.escape(v, 'q') .. '"'
    end
  end
  local c = attr.classes and util.escape(table.concat(attr.classes, ' '), 'q') or ''
  local i = attr.identifier or ''

  return ((i ~= '') and ' id="' .. i .. '"' or '') ..
      ((c ~= '') and ' class="' .. c .. '"' or '') ..
      ((a ~= '') and a or '')
end

function util._attributes_test()
  assert(util.attributes({}) == '')
  assert(util.attributes({
    identifier = 'one',
    classes = { 'one', 'two' },
    attributes = { one = 'two' },
  }) == ' id="one" class="one two" one="two"')
end

-- ----------------------------------------------------------------------------

-- run all tests
function util._test()
  for key, item in pairs(util) do
    if type(item) == 'function' and key:match('_.*_test') then
      item()
    end
  end
end

-- ----------------------------------------------------------------------------

return util
