/**
 * An object mapping from (description) to (URL string) for each external
 * resource that we want to cache.
 */
export const external = {
  google_fonts:
    "https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap",
};
