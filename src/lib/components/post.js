// - can't use `class="comment"` for `label` because safari strips those in
//   reader view
export const label = (s) => `<span class="label">${s}</span>`;
export const draft = (s) => `<span class="draft">${s}</span>`;
export const date = (s) => `<span class="date">${s}</span>`;

export const keywords = (meta, { div = false } = {}) =>
  meta.keywords
    ? (div ? '<div class="keywords">' : "") +
      meta.keywords
        .map((k) => label("#") + (k === "draft" ? draft(k) : k))
        .join(" ") +
      (div ? "</div>" : "")
    : "";

export const authors = (
  meta,
  { div = false, label: by = false, url = true, email = true } = {}
) =>
  meta.author
    ? (div ? '<div class="authors">' : "") +
      (by ? label("By: ") : "") +
      meta.author
        .map((a) =>
          url && a.url
            ? `<a href="${a.url}">${a.name}</a>`
            : a.name +
              (email && a.email
                ? label(` (<a href="mailto:${a.email}">email</a>)`)
                : "")
        )
        .join(label(", ")) +
      (div ? "</div>" : "")
    : "";

export const dates = (meta, { div = false, span = false } = {}) => {
  const dates = [
    meta.date ? label("Published: ") + date(meta.date) : "",
    meta.updated ? label("Updated: ") + date(meta.updated) : "",
  ]
    .filter((d) => d)
    .join('<span class="sep"> </span>');
  return dates
    ? (div ? '<div class="dates">' : "") +
        (span ? '<span class="dates">' : "") +
        dates +
        (span ? "</span>" : "") +
        (div ? "</div>" : "")
    : "";
};

export const years = (meta) => {
  const date = meta.date?.slice(0, 4);
  const updated = meta.updated?.slice(0, 4);
  return date ? date + (updated && updated !== date ? "-" + updated : "") : "";
};

export const copyright = (meta) => {
  const year = years(meta);
  const author = authors(meta);
  return year && author ? `Copyright © ${year} ${author}` : "";
};
