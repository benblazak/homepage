import { ls } from "$lib/modules/ls/ls.js";

/** @type {import('./$types').PageServerLoad} */
export async function load() {
  const routes = ls.ls({ route: "/lib/", depth: 1 });

  return {
    routes,
  };
}
