## printing

- chrome
  - adds margins consistently
  - does weird things with text and fonts
- safari
  - prints text and fonts correctly
  - adds 0.25in to margins, and set non-first page top margin to 0.25in
- to measure margins
  - can use the select tool in Skim
    - ruler is on the bottom right
    - click to toggle between point and inch units
