---
debug: true
monetize: true
title: test blog post
subtitle: yup yup yup
keywords: [test, blog]
author:
  - name: Ben Blazak
    url: https://www.benblazak.dev
  - name: Author Two
    email: two@example.com
  - name: Author Three
    url: http://www.example.com
    email: three@example.com
  - name: Author Four
  - name: Author Five
date: 2022-07-13
history:
  - date: 2022-07-14
    description: changed for reasons unknown
  - date: 2022-07-15
    description: because nothing is ever complete
  - date: 2022-07-16
    description: |
      in this edition i have made many modifications

      - there is a new something
      - and something has changed as well
      - let me not forget the third something

      also, i must speak at length about the lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
description: informal test of markdown conversion
abstract: |
  this is indeed a blog post which is a test
...

```{=p_script}
// reload the page if the template changes
import _template from "$lib/preprocessors/pandoc/svelte.template?raw";

const a = 5;
```

```{=p_head}
<!-- header :) -->
```

```{=p_style}
// style
```

`const b = 7;`{=p_script}

`<!-- header again -->`{=p_head}

`// style inline`{=p_style}

`const c = 11;`{=p_script}

this is an introduction before the first section heading. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

## math

one $x^2$ two

$$
  x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}
$$

## code

```{#code1 .js one=two .three}
const a = { a: 1, b:2 }
if (a.a === 1 && a.b === 2 && 1 === 1 && 2 === 2) console.log('a is 1 and b is 2')
```

```
const a = { a: 1, b:2 }
if (a.a === 1 && a.b === 2 && 1 === 1 && 2 === 2) console.log('a is 1 and b is 2')
```

```{p_src='./small.js'}

```

one `const a = { a: 1, b:2 }`{#code2 .js one=two .three} two

one `const a = { a: 1, b:2 }` two

## admonitions

::: note
note 🙂
[lorem ipsum dollor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.]{.note} one
:::

::: tip
tip 😀
[lorem ipsum dollor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.]{.tip} one
:::

::: important
important 🤔
[lorem ipsum dollor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.]{.important} one
:::

::: caution
caution 🧐
[lorem ipsum dollor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.]{.caution} one
:::

::: warning
warning 😬
[lorem ipsum dollor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.]{.warning} one
:::

## raw

normal {5+2}

svelte `{5+2}`{=svelte}

html `{5+2}`{=html}

```{=svelte}
svelte block {5+2}
```

```{=html}
html block {5+2}
```

## links

### links in [headers](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements) too

here is a link to [google](https://www.google.com)

here is a link to [my splash page](splash)

## image

![](./butterfly.png "title"){#id .class one=two p_width=50 hidden=true} <!-- to use an image more than once, make a hidden image with p_width set to the largest width, then use the width attribute (or css) for the others -->

![alt *caption*](./butterfly.png "title"){#id .class one=two width=25}

one ![alt *caption*](./butterfly.png "title"){#id .class one=two} two

## footnotes

this has a footnote.^[and it is using pandoc's very nice inline footnote feature :)]

this had a footnote, but prettier doesn't get along well with empty footnote definitions. which isn't too bad, since i suppose they don't make much sense in real life.

empty inline footnote.^[]

long footnote[^l-foot]

[^l-foot]: this is

    a long

    fooooooottttttnoooooootttteeeeeee
