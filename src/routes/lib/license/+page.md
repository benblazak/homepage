---
title: License
description: License file for the content and source code of this website.
...

All work that I am allowed to copyright is Copyright © Ben Blazak as of the dates of publishing and update (see git history). It is distributed under these terms unless otherwise noted.

## Source Code

Source code and documentation are distributed under the [MIT License](#mit-license).

I do politely ask that you not duplicate my site's appearance too closely. There are lots of free templates around, if that's what you're looking for.

## Content

I reserve all rights to content.

If you'd like to use more than fair use with attribution (including a link 🙂) allows, please contact me.

If the content is more than 14 years old and you cannot contact me after making a reasonable effort, you may consider it licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

### Disclaimer and Limitation of Liability

Content is provided as-is, without warranty of any kind. I make no representations concerning it. If you choose to use it in any way, you do so at your own risk.

## Other

I expect everything to be covered above, so if there's anything that's not please let me know.

## Notes

- Source code includes source code listed in blog posts.
- Documentation includes comments in source code listings in blog posts, but does not include the rest of the post (which is content).
- SVGs are images (and not source code).
- Content includes images, videos, most anything in a natural language that is not documentation, and generally most anything that is not source code or documentation.

---

## [MIT License](https://opensource.org/licenses/MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
