import { test, expect } from "vitest";
import { filter } from "./util.js";

test("filter", () => {
  expect(
    filter(
      {
        type: "book",
        name: "alice in wonderland",
        date: 1865,
        author: {
          name: "lewis carroll",
          birthday: "1832-01-27",
        },
      },
      {
        name: null,
        author: {
          birthday: null,
        },
      }
    )
  ).toEqual({
    name: "alice in wonderland",
    author: { birthday: "1832-01-27" },
  });
});
