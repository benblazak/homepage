import path from "path";
import { ls } from "$lib/modules/ls/ls.js";
import { url as baseurl } from "$routes/_meta.js";

/**
 * Fill in null entries of `query` with data from `data`
 */
export function filter(data, query) {
  for (const key in query)
    if (query[key] === null) query[key] = data[key];
    else filter(data[key], query[key]);
  return query;
}

export function get_source_url({ route }) {
  let source_url = "https://gitlab.com/benblazak/homepage";

  let file = ls.route_file[route];
  if (file !== undefined)
    source_url += path.join("/-/blob/main/src/routes", encodeURI(file));

  return source_url;
}

export async function get_meta(opt) {
  const { route } = opt;

  return JSON.parse(
    JSON.stringify(
      await ls.meta({
        before: { url: new URL(route, baseurl) },
        ...opt,
      })
    )
  );
}
