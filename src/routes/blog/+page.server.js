import { ls } from "$lib/modules/ls/ls.js";
import { get_meta } from "$routes/api/util.js";

/** @type {import('./$types').PageServerLoad} */
export async function load() {
  let posts = ls.ls({ route: "/blog/posts", depth: 1 });
  posts = posts.map(async (route) => ({
    route,
    meta: (await get_meta({ route }))?.display?.card ?? {},
  }));
  posts = await Promise.all(posts);
  posts.sort((a, b) => (a.meta.date > b.meta.date ? -1 : 1));

  return {
    posts,
  };
}
