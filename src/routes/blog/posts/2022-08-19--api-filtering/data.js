/**
 * Modified from the Apollo GraphQL Star Wars server example data
 * - retrieved 2022-08-23
 * - from https://github.com/apollographql/starwars-server/blob/main/data/swapiSchema.js
 * - under the MIT license https://github.com/apollographql/starwars-server/blob/main/LICENSE
 */

class Human {
  id;
  name;
  appearsIn;
  homePlanet;
  height;
  mass;

  constructor(data) {
    Object.defineProperties(this, {
      friends: {
        set: (value) => {
          this._friends = value;
        },
        get: () => this._friends.map((e) => idMap[e]),
        enumerable: true,
      },
      starships: {
        set: (value) => {
          this._starships = value;
        },
        get: () => this._starships.map((e) => idMap[e]),
        enumerable: true,
      },
    });

    for (const key in this) this[key] = data[key];
  }
}

class Droid {
  id;
  name;
  appearsIn;
  primaryFunction;

  constructor(data) {
    Object.defineProperties(this, {
      friends: {
        set: (value) => {
          this._friends = value;
        },
        get: () => this._friends.map((e) => idMap[e]),
        enumerable: true,
      },
    });

    for (const key in this) this[key] = data[key];
  }
}

class Starship {
  id;
  name;
  length;

  constructor(data) {
    for (const key in this) this[key] = data[key];
  }
}

// ----------------------------------------------------------------------------

export const humans = [
  new Human({
    id: "1000",
    name: "Luke Skywalker",
    friends: ["1002", "1003", "2000", "2001"],
    appearsIn: ["NEWHOPE", "EMPIRE", "JEDI"],
    homePlanet: "Tatooine",
    height: 1.72,
    mass: 77,
    starships: ["3001", "3003"],
  }),
  new Human({
    id: "1001",
    name: "Darth Vader",
    friends: ["1004"],
    appearsIn: ["NEWHOPE", "EMPIRE", "JEDI"],
    homePlanet: "Tatooine",
    height: 2.02,
    mass: 136,
    starships: ["3002"],
  }),
  new Human({
    id: "1002",
    name: "Han Solo",
    friends: ["1000", "1003", "2001"],
    appearsIn: ["NEWHOPE", "EMPIRE", "JEDI"],
    height: 1.8,
    mass: 80,
    starships: ["3000", "3003"],
  }),
  new Human({
    id: "1003",
    name: "Leia Organa",
    friends: ["1000", "1002", "2000", "2001"],
    appearsIn: ["NEWHOPE", "EMPIRE", "JEDI"],
    homePlanet: "Alderaan",
    height: 1.5,
    mass: 49,
    starships: [],
  }),
  new Human({
    id: "1004",
    name: "Wilhuff Tarkin",
    friends: ["1001"],
    appearsIn: ["NEWHOPE"],
    height: 1.8,
    mass: null,
    starships: [],
  }),
];

export const droids = [
  new Droid({
    id: "2000",
    name: "C-3PO",
    friends: ["1000", "1002", "1003", "2001"],
    appearsIn: ["NEWHOPE", "EMPIRE", "JEDI"],
    primaryFunction: "Protocol",
  }),
  new Droid({
    id: "2001",
    name: "R2-D2",
    friends: ["1000", "1002", "1003"],
    appearsIn: ["NEWHOPE", "EMPIRE", "JEDI"],
    primaryFunction: "Astromech",
  }),
];

export const starships = [
  new Starship({
    id: "3000",
    name: "Millennium Falcon",
    length: 34.37,
  }),
  new Starship({
    id: "3001",
    name: "X-Wing",
    length: 12.5,
  }),
  new Starship({
    id: "3002",
    name: "TIE Advanced x1",
    length: 9.2,
  }),
  new Starship({
    id: "3003",
    name: "Imperial shuttle",
    length: 20,
  }),
];

// ----------------------------------------------------------------------------

const humanData = {};
humans.forEach((human) => {
  humanData[human.id] = human;
});

const droidData = {};
droids.forEach((droid) => {
  droidData[droid.id] = droid;
});

const starshipData = {};
starships.forEach((ship) => {
  starshipData[ship.id] = ship;
});

// ----------------------------------------------------------------------------

const idMap = { ...humanData, ...droidData, ...starshipData };
