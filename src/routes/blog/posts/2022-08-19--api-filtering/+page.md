---
title: Filtering API Data with Plain JavaScript
subtitle: GraphQL is Cool, but...
keywords: software
date: 2022-09-17
history:
  - date: 2022-08-19
    description: Idea
  - date: 2022-08-26
    description: Draft
  - date: 2022-09-17
    description: Published
...

```{=p_style}
.color-comment {
  color: var(--color-comment);
}
```

## Background

<details>
<summary>
For my non-programming friends, some background is in order as to why this is a problem in the first place. [(click to expand)]{.color-comment}
</summary>

In the beginning, there was the web. It wasn't quite formless and void, but it was very basic. Just HTML. At some point JavaScript was invented, and then CSS. People started to use it for more and more things, and eventually applications -- not just pages -- started to be written. Sometimes the applications needed to talk to each other. And thus APIs -- Application Program Interfaces -- were born.

It took a while for APIs to look like they do today, and there are still a few ways to make them, but the general principle is that there's a URL that serves data, rather than a web page. For example, if you wanted to see the metadata for my homepage, you could go to

[`{$page.url.origin + "/__data.json"}`{=svelte}](/__data.json){target=\_blank}

<details>
<summary>
or expand this for a prettier version.
</summary>
```{=svelte}
{#await fetch("/__data.json").then((r) => r.json()) then data}
<br>
<pre class="samp">
<samp>{JSON.stringify(data, null, 2)}</samp>
</pre>
{/await}
```
</details>

This particular data is in a format called JSON, one of the most common formats for such data today. If it makes you say 😵‍💫 I am sorry. But computers find it quite easy to read!

So we have a URL serving data, rather than a web page, but... what is this about filtering? Well, dear reader, say we have several applications, and they all want slightly different pieces of this data. What should we do then?

One solution would be to say no, we are going to send everyone all the data, and each application can ignore what it doesn't want. This is simple, but a giant waste of data.

Another solution would be to say okay, we'll make a special URL for each application, and then everyone can have exactly what they want. This turns out to be not only a lot of work, but very easy to break. What happens when an application changes, and wants slightly different data? Or an application wants different pieces of data at different times? Or an application stops using one URL, and lets you know so you can delete it, but secretly another application started using the old URL, and now deleting it breaks everything? No fun at all.

A third solution, and the one I like best is to set up a single URL (or a small collection of them), and have each application ask for what it wants when it wants it. Good communication: a bit more work, but improves the quality of relationships, even for programs 😉.

Of course, just like there are many different ways for people to communicate what they need, there are many different ways for programs to do it too. It would be helpful to agree on a common language at least. And fiiiiinally, this is where [GraphQL](https://graphql.org) comes in: it's a common language that applications can use to say what they want, and servers can use to respond.

So what am I talking about here? Making a new language, just to confuse everybody?

Sort of, but not quite. I think it's fun to play with languages. So this is an experiment along those lines :-)

We'll get more technical below.

</details>

## Introduction

I was recently implementing a small API, and found myself wanting to filter out some of the data before sending the response. All of the data was computed during build time, and it really wasn't that much... so using GraphQL would have been a bit silly. But doing it manually... nah, might as well let it send a few duplicated fields.

GraphQL felt like a cool idea though, and I wondered how close I could get with just a little bit of JavaScript. I wrote a simple filter function

```{.javascript p_src='./1/filter.js'}

```

<details>
<summary>
and tried it out
</summary>
```{.javascript p_src='./1/test.js'}
```
```{.json p_label='json output' p_tag=samp p_src='./1/test.out'}
```
</details>

and wow. That actually does everything I need it to do.

All done!

Just kidding. This got me thinking: how much of GraphQL could we implement before it started to get complicated? Let's try. We'll stop when we get tired :-)

## Setup

<details>
<summary>
To keep tests shorter, we'll set up some test data here.
</summary>
```{.javascript p_src='./data.js'}
```
</details>

## Basic Filtering

The function above does everything I need it to, but it doesn't begin to approach what a GraphQL query can do. To start with, it can't filter fields of object in arrays. Let's add that,

```{.javascript p_src='./2/filter.js'}

```

<details>
<summary>
and give it a try.
</summary>
```{.javascript p_src='./2/test.js'}
```
```{.json p_label='json output' p_tag=samp p_src='./2/test.out'}
```
</details>

This took more effort, since there were more cases to consider, but it seems to work. We have our base case (`query === null`), we handle the case that `data` is an array, and for all other cases we assume that `query` and `data` are both objects, go through the values in `query` one by one, and filter recursively.

### Error Handling

Before we move on, this might also be a good time to add some error handling and remove empty arrays and objects. It makes our code a bit more complicated (and much longer) but it will be important later.

```{.javascript p_src='./3/filter.js'}

```

<details>
<summary>
We'll give this a try too.
</summary>
```{.javascript p_src='./3/test.js'}
```
```{.json p_label='json output' p_tag=samp p_src='./3/test.out'}
```
</details>

## Matching

What if we want to only retrieve fields for objects that match some criteria -- say, objects with a given ID? Let's see what we can do.

We'll remove the restrictions on what kind of values `query` can have.

- If it's `null` or a non-array object, we'll do the same thing as before.
- If it's a different type of value (but not an array), let's only return objects with the same value.
- If it's an array of values, let's only return objects that match one of them.

If an object doesn't match, we'll return an error saying so, and filter those out of the result.

```{.javascript p_src='./4/filter.js'}

```

Because of all the work we did above, that wasn't too bad!

<details>
<summary>
We'll give this a quick test
</summary>
```{.javascript p_src='./4/test.js'}
```
```{.json p_label='json output' p_tag=samp p_src='./4/test.out'}
```
</details>

and then move on to our final section.

## Advanced Matching

If you give a programmer the ability to match fields exactly, they're going to want... the ability to match fields inexactly! Of course. So let's see what we can do.

The way `filter` is written right now, it doesn't make much sense to have `_query` be something like `[{...}]`. We could certainly change that (perhaps by expanding our matching code), but instead let's use it: If `_query` is an array, and its first value is an object, we'll use the fields of that object for special functions.

I'll only implement a few for now -- enough to see how you could implement more if you wanted to.

```{.javascript p_src='./5/filter.js'}

```

The first thing to notice is that this is getting not only long, but complicated. There were several times while trying things out that I had to step through the code because I wasn't sure what was going on, and many of those times I found a mistake.

I've also started to notice little idiosyncrasies that would take some work to smooth out. For instance, if you want to match a field without including it in the output, you say `[{ query: false, ... }]`. Why `false`? Because `null` was already taken and `undefined` won't make it through `JSON.stringify`. Made sense to me at the time, still makes sense to me now, but I really don't know if it was the best choice. This is starting to become a language design issue, and those take a bit more care than I can spend on this right now.

And if you read through carefully, I'm sure you'll discover many things that I've missed. But I think I am officially tired ;-)

### Tests

Before we stop, I promised you tests! In real life, these would be automated in a testing framework (I actually used Vitest to help with coverage data while writing these out), but for illustration purposes I think this is more helpful.

It's the last version of our `filter` function, and it's gotten quite complicated, so we'll do a more thorough job testing this time around.

<details>
<summary>
fill in null entries
</summary>
```{.javascript p_src='./5/test-1.js'}
```
```{.json p_label='json output' p_tag=samp p_src='./5/test-1.out'}
```
</details>

<details>
<summary>
matching
</summary>
```{.javascript p_src='./5/test-2.js'}
```
```{.json p_label='json output' p_tag=samp p_src='./5/test-2.out'}
```
</details>

<details>
<summary>
pagination
</summary>
```{.javascript p_src='./5/test-3.js'}
```
```{.json p_label='json output' p_tag=samp p_src='./5/test-3.out'}
```
</details>

<details>
<summary>
special queries
</summary>
```{.javascript p_src='./5/test-4.js'}
```
```{.json p_label='json output' p_tag=samp p_src='./5/test-4.out'}
```
</details>

<details>
<summary>
errors
</summary>
```{.javascript p_src='./5/test-5.js'}
```
```{.json p_label='json output' p_tag=samp p_src='./5/test-5.out'}
```
</details>

## Conclusion

I'm pretty happy with what we accomplished here. We took our little filter function and grew it into something that can do some fairly sophisticated things. Most importantly, we had fun thinking about how all these things could be done.
