/**
 * Return an object with the same shape as `query`, with data from `data`
 */
export function filter(_data, _query) {
  // fill in null entries
  if (_query === null) return { data: _data };

  // filter arrays of objects
  if (Array.isArray(_data)) {
    let out = [];
    for (const obj of _data) {
      let { data, error } = filter(obj, _query);
      // handle errors
      if (error === "match failed") continue;
      if (error !== undefined) return { error };
      // only keep non-empty results
      if (
        !(Array.isArray(data) && data.length === 0) &&
        !(typeof data === "object" && Object.keys(data).length === 0)
      ) {
        out.push(data);
      }
    }
    return { data: out };
  }

  // match
  if (typeof _query !== "object")
    if (_data === _query) return { data: _data };
    else return { error: "match failed" };
  if (Array.isArray(_query))
    if (_query.includes(_data)) return { data: _data };
    else return { error: "match failed" };

  // filter recursively
  let out = {};
  for (const key in _query) {
    let { data, error } = filter(_data[key], _query[key]);
    // handle errors
    if (error === "match failed") return { data: {} };
    if (error !== undefined) return { error };
    // only keep non-empty results
    if (data !== undefined) out[key] = data;
  }
  return { data: out };
}
