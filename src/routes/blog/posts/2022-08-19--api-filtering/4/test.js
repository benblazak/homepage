import { log_json } from "../util.js";
import { humans, droids } from "../data.js";
import { filter } from "./filter.js";

console.log("// match non-array in object");
log_json(
  filter(droids, {
    id: "2000",
    name: null,
    friends: {
      name: null,
    },
  })
);

console.log("\n// match array in object");
log_json(
  filter(humans, {
    id: ["1000", "1001"],
    name: null,
  })
);
