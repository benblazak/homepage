/**
 * Functions for special queries.
 *
 * - return `undefined` to do nothing
 * - if `true` is returned, `filter` should return `{ data }`
 *   - unless `query` is `false` in which case it should return
 *     `{ data: undefined }`. this allows us to filter based on a field without
 *     including that field in the output.
 * - if `false` is returned, `filter` should return `{ error: "match failed" }`
 * - if an object is returned, `filter` should return that object
 */
const specialFunctions = {
  /** Do nothing. This field is used elsewhere. */
  query: () => {},

  /**
   * Paginate, by returning a slice of `data`.
   */
  page: ({ data, opt }) => {
    if (!Array.isArray(data))
      return { error: { opt, message: "page: data must be an array" } };
    return { data: data.slice(opt.start, opt.end) };
  },

  /**
   * Return `true` if `data` is a string, and starts with `opt`.
   */
  startsWith: ({ data, opt }) => {
    if (typeof data !== "string")
      return { error: { opt, message: "startsWith: data must be a string" } };

    return data.startsWith(opt);
  },

  /**
   * Return `true` if `data` includes `opt`.
   */
  includes: ({ data, opt }) => {
    if (!Array.isArray(data))
      return {
        error: { opt, message: "includes: data must be an array" },
      };
    return data.includes(opt);
  },
};

/**
 * Return an object with the same shape as `query`, with data from `data`
 */
export function filter(_data, _query) {
  // special query
  // - a query of the form `[{...}, ...]`
  // - anything in the array besides the first object is ignored for now
  let specialQuery;
  if (Array.isArray(_query) && typeof _query[0] === "object")
    specialQuery = _query[0];
  // special queries can have regular queries inside
  let query = _query;
  if (specialQuery) query = specialQuery.query;

  // fill in null entries
  // - including when a special query's query is `null`
  if (query === null) return { data: _data };

  // filter arrays of objects
  if (Array.isArray(_data) && typeof _data[0] === "object") {
    let out = [];
    for (const obj of _data) {
      let { data, error } = filter(obj, query);
      // handle errors
      if (error === "match failed") continue;
      if (error !== undefined) return { error };
      // only keep non-empty results
      if (
        !(data === undefined) &&
        !(Array.isArray(data) && data.length === 0) &&
        !(typeof data === "object" && Object.keys(data).length === 0)
      ) {
        out.push(data);
      }
    }
    // special query: pagination
    if (specialQuery?.page)
      return specialFunctions.page({ data: out, opt: specialQuery.page });
    return { data: out };
  }

  // special queries
  for (const key in specialQuery) {
    if (!(key in specialFunctions))
      return { error: { field: key, message: "unknown special query field" } };
    const value = specialFunctions[key]({
      data: _data,
      opt: specialQuery[key],
    });
    if (value === undefined) continue;
    if (value === true)
      if (query === false) return { data: undefined };
      else return { data: _data };
    if (value === false) return { error: "match failed" };
    if (typeof value === "object") return value;
    return { value, message: "unknown special function return value" };
  }

  // match
  if (typeof query !== "object")
    if (_data === query) return { data: _data };
    else return { error: "match failed" };
  if (Array.isArray(query))
    if (query.includes(_data)) return { data: _data };
    else return { error: "match failed" };

  // filter recursively
  let out = {};
  for (const key in query) {
    let { data, error } = filter(_data[key], query[key]);
    // handle errors
    if (error === "match failed") return { data: {} };
    if (error !== undefined) return { error };
    // only keep non-empty results
    if (data !== undefined) out[key] = data;
  }
  return { data: out };
}
