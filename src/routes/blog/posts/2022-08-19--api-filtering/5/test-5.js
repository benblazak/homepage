import { log_json } from "../util.js";
import { humans, starships } from "../data.js";
import { filter } from "./filter.js";

console.log("// errors");

console.log("\n// paginate non-array");
console.log("\n// --- data");
log_json(filter(starships[0], [{ query: { name: null } }]));
console.log("\n// --- error");
log_json(filter(starships[0], [{ page: { start: 0 }, query: { name: null } }]));

console.log("\n// starts with non-string");
console.log("\n// --- data");
log_json(filter(humans, { name: [{ startsWith: "L" }] }));
console.log("\n// --- error");
log_json(filter(humans, { mass: [{ startsWith: "L" }] }));

console.log("\n// includes non-array");
console.log("\n// --- data");
log_json(filter(humans, { name: [{ startsWith: "L" }] }));
console.log("\n// --- error");
log_json(filter(humans, { name: [{ includes: "L" }] }));

console.log("\n// unknown special query field");
console.log("\n// --- data");
log_json(filter(humans, { name: [{ startsWith: "Le" }] }));
console.log("\n// --- error");
log_json(filter(humans, { name: [{ fake_field_name: "L" }] }));
