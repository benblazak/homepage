import { log_json } from "../util.js";
import { droids } from "../data.js";
import { filter } from "./filter.js";

console.log("// fill in null entries");

log_json(
  filter(droids, {
    name: null,
  })
);
