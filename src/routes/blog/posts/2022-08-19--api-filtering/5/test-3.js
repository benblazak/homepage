import { log_json } from "../util.js";
import { humans } from "../data.js";
import { filter } from "./filter.js";

console.log("// pagination");

console.log("\n// without pagination");
log_json(filter(humans, [{ query: { name: null } }]));

console.log("\n// entries 0..2");
log_json(
  filter(humans, [
    {
      page: { start: 0, end: 2 },
      query: { name: null },
    },
  ])
);
console.log("\n// entries 2..4");
log_json(
  filter(humans, [
    {
      page: { start: 2, end: 4 },
      query: { name: null },
    },
  ])
);
console.log("\n// entries 4..end");
log_json(
  filter(humans, [
    {
      page: { start: 4 },
      query: { name: null },
    },
  ])
);
