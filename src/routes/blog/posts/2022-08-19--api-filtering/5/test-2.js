import { log_json } from "../util.js";
import { humans } from "../data.js";
import { filter } from "./filter.js";

console.log("// match");

console.log("\n// value");
log_json(
  filter(humans, {
    id: "1000",
    name: null,
  })
);

console.log("\n// array");
log_json(
  filter(humans, {
    id: ["1000", "1001"],
    name: null,
  })
);

console.log("\n// subfield value");
log_json(
  filter(humans, {
    id: "1000",
    name: null,
    friends: {
      id: "1002",
      name: null,
    },
  })
);

console.log("\n// subfield array");
log_json(
  filter(humans, {
    id: "1000",
    name: null,
    friends: {
      id: ["1002", "1003"],
      name: null,
    },
  })
);
