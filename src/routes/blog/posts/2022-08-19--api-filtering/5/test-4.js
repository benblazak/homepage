import { log_json } from "../util.js";
import { humans } from "../data.js";
import { filter } from "./filter.js";

console.log("// special queries");

console.log("\n// starts with");

log_json(
  filter(humans, {
    name: [{ startsWith: "L" }],
  })
);

console.log("\n// includes");

console.log("\n// --- with field");
log_json(
  filter(humans, {
    name: null,
    appearsIn: [{ includes: "JEDI" }],
  })
);

console.log("\n// --- without field");
log_json(
  filter(humans, {
    name: null,
    appearsIn: [{ query: false, includes: "JEDI" }],
  })
);
