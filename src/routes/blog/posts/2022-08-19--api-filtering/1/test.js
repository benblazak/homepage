import { log_json } from "../util.js";
import { filter } from "./filter.js";

console.log("// one");
log_json(
  filter(
    {
      one: 1,
      two: 2,
      three: 3,
    },
    {
      two: null,
    }
  )
);

console.log("\n// two");
log_json(
  filter(
    {
      one: { one: 11, two: 12 },
      two: { one: 21, two: 22 },
      three: { one: 31, two: 32 },
    },
    {
      one: { one: null },
      two: null,
    }
  )
);
