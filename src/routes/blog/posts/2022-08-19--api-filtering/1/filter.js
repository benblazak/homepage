/**
 * Fill in null entries of `query` with data from `data`
 */
export function filter(data, query) {
  for (const key in query)
    if (query[key] === null) query[key] = data[key];
    else filter(data[key], query[key]);
  return query;
}
