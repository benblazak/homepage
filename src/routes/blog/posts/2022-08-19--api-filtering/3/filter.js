/**
 * Return an object with the same shape as `query`, with data from `data`
 */
export function filter(_data, _query) {
  // check arguments
  // - `query` should be `null` or a non-array object
  if (
    !(_query === null) &&
    !(typeof _query === "object" && !Array.isArray(_query))
  ) {
    return { error: "invalid arguments" };
  }

  // fill in null entries
  if (_query === null) return { data: _data };

  // filter arrays of objects
  if (Array.isArray(_data)) {
    let out = [];
    for (const obj of _data) {
      let { data, error } = filter(obj, _query);
      // handle errors
      if (error !== undefined) return { error };
      // only keep non-empty results
      if (
        !(Array.isArray(data) && data.length === 0) &&
        !(typeof data === "object" && Object.keys(data).length === 0)
      ) {
        out.push(data);
      }
    }
    return { data: out };
  }

  // filter recursively
  let out = {};
  for (const key in _query) {
    let { data, error } = filter(_data[key], _query[key]);
    // handle errors
    if (error !== undefined) return { error };
    // only keep non-empty results
    if (data !== undefined) out[key] = data;
  }
  return { data: out };
}
