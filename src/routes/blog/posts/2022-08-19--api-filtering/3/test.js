import { log_json } from "../util.js";
import { droids } from "../data.js";
import { filter } from "./filter.js";

console.log("// no error");
log_json(
  filter(droids, {
    name: null,
    friends: {
      homePlanet: null,
    },
  })
);

console.log("\n// error");
log_json(filter(droids, []));
