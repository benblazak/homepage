import { log_json } from "../util.js";
import { droids } from "../data.js";
import { filter } from "./filter.js";

log_json(
  filter(droids, {
    name: null,
    friends: {
      name: null,
      homePlanet: null,
    },
  })
);
