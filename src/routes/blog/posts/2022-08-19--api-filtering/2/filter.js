/**
 * Return an object with the same shape as `query`, with data from `data`
 *
 * - `data` can be an array or a non-array object
 * - `query` should be a non-array object or null
 */
export function filter(data, query) {
  // fill in null entries
  if (query === null) return data;

  // filter arrays of objects
  if (Array.isArray(data)) return data.map((obj) => filter(obj, query));

  // filter recursively
  // - put results in `out` instead of modifying the original query, otherwise
  //   we can't use `query` multiple times, like we need to when filtering
  //   arrays of objects above
  let out = {};
  for (const key in query) out[key] = filter(data[key], query[key]);
  return out;
}
