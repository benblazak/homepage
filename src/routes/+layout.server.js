export const prerender = true;

import { filter, get_source_url, get_meta } from "$routes/api/util.js";

/** @type {import('./$types').LayoutServerLoad} */
export async function load({ url }) {
  const route = decodeURI(url.pathname);

  const source_url = get_source_url({ route });
  const meta = filter(await get_meta({ route }), {
    display: {
      page: null,
    },
    meta: {
      tagHTML: null,
      ogHTML: null,
      jsonldHTML: null,
    },
  });

  return {
    source_url,
    meta,
  };
}
