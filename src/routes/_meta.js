export const url = new URL("https://www.benblazak.dev");

export const meta = {
  type: "WebPage",
  author: "Ben Blazak",

  image: new URL("/images/share--2022-08-19.png", url),
  license: new URL("/lib/license", url),
};
